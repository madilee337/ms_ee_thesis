%% SC Function
% Single Constraint Algorithm 
%   - The algorithm with only one extra constraint
%   Inputs:
%       - hprev: column vector containing all the transfer functions 
%               estimates size: M(L + 1)x1
%       - x: Toeplitz data matrix size: (N - L)xM(L + 1)
%       - eta: stepsize for update (scalar)
%       - epsilon: the offset for the denominator to be nonzero (small)
%       - alpha: the weight for the sparsity constraint
%       - gamma: the weight for the shift-suppression constraint
%       - L: the length of a transfer function estimate
%       - N: the amount of data in the Toeplitz data matrix
%       - M: the number of channels
%       - maxBlockReset: the data block size used for processing
%       - maxIter: the maximum amount of passes through the data
%       - d: the index for the elementary vector
%   Outputs:
%       - EstChannels: vector containing new updated estimate for transfer
%       functions
function EstChannels = SC(hprev, x, eta, alpha, gamma, epsilon, L, N, M,...
                           maxBlockReset, maxIter, d)
%% Pre-Processing and Initialization
    % Determine iterations through data
    iters = N - L;
    % Set elementary vector
    ed = zeros(M*(L + 1), 1);
    ed(d) = 1;
    % Set up identity matrix (or rather vector)
%     I = ones(M*(L + 1)); % eye(M*(L + 1)
%             inverse = (I + gamma*ed*ed')\I;
%             inverse_diag = (I + gamma*ed*ed'); %\I;
%             inverse = 1./diag(inverse_diag);
    inverse = ones(M*(L + 1), 1);
    inverse(d) = inverse(d) + gamma; 
    inverse = 1./inverse;
%% Processing
    % Make multiple passes through data (if data chunks are sufficiently
    % small)
    for iter = 1:maxIter
        % Reset the block size
        block = maxBlockReset;
        % Iterate through data
        for row = 1:block:iters
            % Verify that the indices are valid
            if row > N - block
                % Set maxBlock such that the indices are not violated
                block = N - row - (L + 1);
            end
            % Get "row" of data
            xr = x(row:row + block - 1, :).';
            % Compute place holder variable for repeated variables
            hest = (hprev - alpha*exp(1j*phase(hprev)) + gamma*ed);
            den = zeros(block, 1); xrt = xr';
            for cols = 1:block
                temp = inverse.*xr(:, cols); 
                den(cols) = xrt(cols, :)*temp;
            end
            % Compute Lagrange Multiplier
            tilde_lambda = ((hest'.*inverse.')*xr)./(den + epsilon).';
            % Compute Update
            hnew = inverse.*(hest - eta*(xr*tilde_lambda'));
            % Save new update for next iteration
            hprev = hnew;
        end
    end
%% Output
   % Format output
   EstChannels = reshape(hnew, L + 1, M); 
end