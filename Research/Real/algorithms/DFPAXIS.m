%% DFPAXIS Function
% Dual Fixed-Peak Adaptive Cross-Channel Identification with Sparse Shift-
% Suppression Algorithm 
%   - The modified update algorithm to combat asymmetric treatement of the
%     channels for channel estimation with sparsity,
%     shift-suppression and flexibility
%   Inputs:
%       - hprev: column vector containing all the transfer functions 
%               estimates size: M(L + 1)x1
%       - x: Toeplitz data matrix size: (N - L)xM(L + 1)
%       - eta: stepsize for update (scalar)
%       - epsilon: the offset for the denominator to be nonzero (small)
%       - alpha: the weight for the sparsity constraint
%       - L: the length of a transfer function estimate
%       - N: the amount of data in the Toeplitz data matrix
%       - M: the number of channels
%       - maxBlockReset: the data block size used for processing
%       - maxIter: the maximum amount of passes through the data
%       - d: the index for the elementary vector
%   Outputs:
%       - EstChannels: vector containing new updated estimate for transfer
%       functions
function EstChannels = DFPAXIS(hprev, x, eta, alpha, epsilon, L, N, M,...
                           maxBlockReset, maxIter, d)
%% Pre-Processing and Initialization
    % Determine iterations through data
    iters = N - (L + 1);
    % Set elementary vector
    ed = zeros(M*(L + 1), 1);
    ed(d) = 1;
    % initialize flexible variable
    z = 1;
%% Processing
    % Make multiple passes through data (if data chunks are sufficiently
    % small)
    for iter = 1:maxIter
        % Reset the block size
        block = maxBlockReset;
        % Iterate through data
        for row = 1:block:iters
            % Verify that the indices are valid
            if row > N - block
                % Set maxBlock such that the indices are not violated
                block = N - row - (L + 1);
            end
            % Get "row" of data
            xr = x(row:row + block - 1, :).';
            % set ed block matrix for ease of computation
            edMat = zeros(size(xr));
            edMat(d,:) = ones(1, block); 
            % Check if first iteration
            if row == 1
                % Call normal AXIS algorithm to start algorithm
                EstChannels = AXIS(hprev, xr.', eta, alpha, epsilon, L,...
                                  (L + 1) + block, M, block, 1, d);
                % Reformat output
                hprev = reshape(EstChannels, M*(L + 1), 1);
                % Determine which indice for the max peak
                [~, c] = max(hprev(L + 1:end));
                % Make corresponding elementary vector
                ec = zeros(M*(L + 1), 1);
                ec(c) = 1;
            % If not the first iteration
            else
                % set ec block matrix for ease of computation
                ecMat = zeros(size(xr));
                ecMat(c,:) = ones(1, block); 
                % Compute place holder variable for repeated variables
                hest = (hprev - alpha*exp(1j*phase(hprev)));
                % Compute Lagrange Multipliers
                den = (vecnorm(xr).' - (xr'*ed).*(ed'*xr).' - ...
                                         (xr'*ec).*(ec'*xr).' + epsilon).';
                tilde_lambda = (hest'*xr - (hest'*ec - y)*ec'*xr ...
                                             - (hest'*ed - z)*ed'*xr)./den;
                tilde_mu = ((hest'*ed - z)*(vecnorm(xr).' - ...
                            (xr'*ec).*(ec'*xr).') - ...
                            (xr'*ed).*(hest'*xr - ...
                                          (hest'*ec - y)*ec'*xr).').'./den;
                tilde_beta = ((hest'*ec - y)*(vecnorm(xr).' - ...
                               (xr'*ed).*(ed'*xr).') - ...
                               (xr'*ec).*(hest'*xr - ...
                                  ((ed'*xr).'*(hest'*ed - z)).').').'./den;
                % Compute Update
                hnew = hest - eta*(xr*tilde_lambda' + ...
                                      edMat*tilde_mu' + ecMat*tilde_beta');
                % Save new update for next iteration
                hprev = hnew;
            end
            % Determine new value for z and y
            z = hprev(d);
            y = hprev(c);
        end
    end
%% Output
   % Format output
   EstChannels = reshape(hnew, L + 1, M); 
end