%% vBF Function
% Varied "Brute" Force Algorithm 
%   - The update algorithm to that "forces" the results, with some
%   flexibility
%   Inputs:
%       - hprev: column vector containing all the transfer functions 
%               estimates size: M(L + 1)x1
%       - x: Toeplitz data matrix size: (N - L)xM(L + 1)
%       - eta: stepsize for update (scalar)
%       - epsilon: the offset for the denominator to be nonzero (small)
%       - alpha: the weight for the sparsity constraint
%       - L: the length of a transfer function estimate
%       - N: the amount of data in the Toeplitz data matrix
%       - M: the number of channels
%       - maxBlockReset: the data block size used for processing
%       - maxIter: the maximum amount of passes through the data
%       - d: the index for the elementary vector
%   Outputs:
%       - EstChannels: vector containing new updated estimate for transfer
%       functions
function EstChannels = vBF(hprev, x, eta, epsilon, alpha, L, N, M, ...
                          maxBlockReset, maxIter, d)
%% Pre-Processing and Initialization
    % Determine iterations through data
    iters = N - (L + 1);
    % Set elementary vector
    ed = zeros(M*(L + 1), 1);
    ed(d) = 1;
    % Set up identity matrix
    I = eye(M*(L + 1));
    % Iinitialize flexible variable
    z = 1;
%% Processing
    % Make multiple passes through data (if data chunks are sufficiently
    % small)
    for iter = 1:maxIter
        % Reset the block size
        block = maxBlockReset;
        % Iterate through data
        for row = 1:block:iters
            % Verify that the indices are valid
            if row > N - block
                % Set maxBlock such that the indices are not violated
                 block = N - row - (L + 1);
            end
            % Get "row" of data
            xr = x(row:row + block - 1, :);
            % Compute place holder variable for repeating value
            Xinv = (xr'*xr)\I;
            % Compute Lagrange Multiplier
            tilde_lambda = (-z - (Xinv*alpha*exp(1j*phase(hprev)))'*ed)...
                                                  /(ed'*Xinv*ed + epsilon);
            % Compute Update
            hnew = -Xinv*(alpha*exp(1j*phase(hprev)) + ...
                                                eta*conj(tilde_lambda)*ed);
            % Save new update for next iteration
            hprev = hnew;
        end
    end
%% Output
   % Format output
   EstChannels = reshape(hnew, L + 1, M); 
end