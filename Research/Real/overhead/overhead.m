%% Overhead - Real
% This version incorporates multiple functions for ease of processing for
% real observed data collected using the SDRs 
clear all; 
% close all; 
clc;
% Add all files needed to working directory
dirs{1} = '/home/madi/Research_2018-2019/thesis/ms_ee_thesis/Research/Real/algorithms/';
dirs{2} = '/home/madi/Research_2018-2019/thesis/ms_ee_thesis/Research/Real/data/';
% Cycle through and add paths
for fold = 1:length(dirs)
    % Add folders
    addpath(dirs{fold});
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialization and Pre-Processing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initializations and pre-processing for algorithm comparison
    % Sampling Rate (FIX) 
    Fs = 16e6;
    % Convert sampling rate to MHz
    Fs_MHz = Fs/1e6;
    % Label sampling rate for output
    sampleRate = [num2str(Fs_MHz), ' MHz'];
    % Compute L 
%     L = Fs_MHz;
    L = 32;
    % Speed of light
    c = 3e8;
    % Distance conversion
    dist = (c/Fs).'; 
    % Cyle through data (Remove for later iterations)
    for dId = [6 8]
        if dId == 1
            % Determines data set to read from and labels data set for
            % output
            dateDat = '002_2020-03-27_18-46'; dataSet = 'GaussDist_Lab';
        elseif dId == 2
            % Determines data set to read from and labels data set for
            % output
            dateDat = '002_2020-03-27_18-47-29'; dataSet = 'GaussDist_Hall';
        elseif dId ==3
            % Determines data set to read from and labels data set for
            % output
            dateDat = '002_2020-03-27_18-47-54'; dataSet = 'GaussDist_Corner';
        elseif dId == 4
            % Determines data set to read from and labels data set for
            % output
            dateDat = '002_2020-03-27_18-53-49'; dataSet = 'GaussDist_Sync1';
        elseif dId == 5
            % Determines data set to read from and labels data set for
            % output
            dateDat = '002_2020-03-27_18-53-56'; dataSet = 'GaussDist_Sync2';
        elseif dId == 6
            % Determines data set to read from and labels data set for
            % output
            dateDat = '002_2020-02-26_14-51'; dataSet = 'HT_Lab';
        elseif dId == 7
            % Determines data set to read from and labels data set for
            % output
            dateDat = '002_2020-02-26_14-52'; dataSet = 'HT_Hall';
        elseif dId == 8
            % Determines data set to read from and labels data set for
            % output
            dateDat = '002_2020-02-26_14-54'; dataSet = 'HT_Corner';
        end
%     dateDat = '002_2020-04-03_16-34'; dataSet = 'Ham_Lab';
%     dateDat = '002_2020-04-03_16-47-28'; dataSet = 'Ham_Hall';
%     dateDat = '002_2020-04-03_16-47-58'; dataSet = 'Ham_Corner';
%     dateDat = 'gnuRadioData'; msg = 'hall'; dataSet = ['GaussDist_' msg];
%     dateDat = '002_2020-03-27_18-46'; dataSet = 'GaussDist_Lab';
%     dateDat = '002_2020-03-27_18-47-29'; dataSet = 'GaussDist_Hall';
%     dateDat = '002_2020-03-27_18-47-54'; dataSet = 'GaussDist_Corner';
%     dateDat = '002_2020-03-27_18-53-49'; dataSet = 'GaussDist_Sync1';
%     dateDat = '002_2020-03-27_18-53-56'; dataSet = 'GaussDist_Sync2';
%     dateDat = '002_2020-03-23_17-18'; dataSet = 'Sinusoide_Fed';
%     dateDat = '002_2020-03-23_17-16'; dataSet = 'WhiteNoise_Fed';
%     dateDat = '002_2020-03-17_14-47-22'; dataSet = 'UniformDist_Hallway';
%     dateDat = '002_2020-03-17_14-47-53'; dataSet = 'UniformDist_Under';
%     dateDat = '002_2020-03-17_14-48-28'; dataSet = 'UniformDist_Indside';
%     dateDat = '002_2020-02-26_14-51'; dataSet = 'WalkieTalkie_Inside';
%     dateDat = '002_2020-02-26_14-52'; dataSet = 'WalkieTalkie_Hallway';
%     dateDat = '002_2020-02-26_14-54'; dataSet = 'WalkieTalkie_Corner';
    % Read in data 
    if exist('msg', 'var')
        data0 = readDataFile(dirs{2}, dateDat, msg);
    else 
        data0 = readDataFile(dirs{2}, dateDat);
    end
    % Initialize file to output results to
    fileName = ['Method_Comparisons_Real_' date '_v1.csv'];
     % Check if file already exits to prevent overwrite
    while exist(fileName, 'file')
        % Determine version
        if fileName(end - 5) == 'v'
            % If single digits
            ver = str2double(fileName(end-4));
        elseif fileName(end - 6) == 'v'
            % If double digits
            ver = str2double(fileName(end-5:end-4));
        elseif fileName(end - 7) == 'v'
            % If triple digits
            str2double(fileName(end-6:end-4));
        else
            % If more
            msgl1 = 'HELP! More than triple digits method comparison\n';
            msgl2 = 'files, add more to if statement';
            msg = [msgl1 msgl2];
            error(msg);
        end
        % Update version
        new_ver = ver + 1;
        % Determine new filename
        fileName = ['Method_Comparisons_Real_' date '_v' ...
                                                  num2str(new_ver) '.csv'];
    end
    % Set first row of data table array
    header = {'Data_Set', '&', 'Channels', '&', 'Algorithm',  '&', ...
                                   'Sample_Rate', '&', 'Estimate_m', '\\'};
    % Initialize number of columns for output table;
    dataTabCol = 5;
    % Set algorithms (1:19 selects which algorithm to run, NOTE multiple
    % algorithms can be selected)
    alg_list = {'CC', 'SVD', ... % [1 2]
            'AED', 'ModAED', 'ModAEDS', ... %[3 4 5]
            'AXIS', 'VAXIS', 'DFPAXIS', 'NSAXIS',... %[6 7 8 9]
            'NBF', 'ABF', 'VBF', 'DFPBF', 'NSBF',... %[10 11 12 13 14]
            'SC', 'VSC', 'DFPSC', 'NSSC'}; %[15 16 17 18]
    % Select the adaptive algorithms and the CC algorithm for processing
    % real data
    algs = alg_list([1 3 4 5 6 7 8 9 15 16 17 18]);
%     algs = alg_list([1, 3, 4, 5, 6, 7, 8, 9 15, 16, 17, 18]);
    % Determine number of algorithms
    numAlgs = length(algs);
    % Create dataTable
    dataTable =  cell(numAlgs, 2*dataTabCol);
    % Determine if data needs to be synchronized
    delayFlag = 0;
    % Sepecify the delay
    delayMat = [0     -79   1203 3;
                79    0     1280 80;
                -1203 -1280 0    -1200;
                -3    -80   1200 0]; % Delay for Gaussian Distributed Signals
    % Determine data size
    N = length(data0);
    % Set index for output table
    index = 0;
    % Cycle through all channel pairs
    for chan1 = 2
        for chan2 = 3:4
            % Determine which channels to run through processing (1 2 3 4)
            channels = [chan1, chan2];
            % Extract channels
            data = data0(channels, :);
            % Modify to processable size And synchronize if needed
            if delayFlag
                % Determine delay flag
                delay = delayMat(channels(1), channels(2));
                % Verify Nleft amount is a practical value to account for delay
                maxN = floor(N - abs(delay));
                Nleft = maxN - 1; 
                % Determine delay direction
                if delay > 0
                    % Grab 2nd Channel
                    dataTmp2 = data(1, :);
                    % Grab 1st Channel
                    dataTmp1 = data(2, 1:(Nleft + 1));
                else
                    % Grab 2nd Channel
                    dataTmp2 = data(2, :);
                    % Grab 1st Channel
                    dataTmp1 = data(1, 1:(Nleft + 1));
                end
                % Interpolate for fractional delay amount
                dataTmp2 = interp(dataTmp2, 100);
                % Shift
                try
                    dataTmp2 = dataTmp2(abs(delay)*100 - 1:(abs(delay) + Nleft)*100);
                catch
                    dataTmp2 = dataTmp2;
                end
                % Decimate to normal amount
                dataTmp2 = decimate(dataTmp2,100); 
                % Create new data matrix
                data = [dataTmp1; dataTmp2];
            end
        %     figure();
%             channel1 = data(1, :);
%             channel2 = data(2, :);
%             channel3 = data(3, :);
%             channel4 = data(4, :);
        %     plot(abs(channel1)); hold on;
        %     plot(abs(channel2)); hold off;
        %     plot(abs(channel3));
        %     plot(abs(channel4));
            % Process all data through algorithms
            for a = 1:numAlgs
                % Process algorithms
                delayEst = algSelect(algs{a}, data, L, channels, dataSet);
                % Compute distance estimate 
                distEst = delayEst*dist;
                % Add to table
                dataTable(index + a, :) = {dataSet, '&', [num2str(channels(1)) '\&' ...
                                num2str(channels(2))], '&', algs{a}, '&', ...
                                                   sampleRate, '&', distEst, '\\'};
            end
            % increment index
            index = index + numAlgs;
        end
    end
    % Output to file
    T = cell2table(vertcat(header, dataTable));
    writetable(T, fileName, 'Delimiter', ' ');   
    end
    
    
    
    
    
