%% Read In GNU Radio Data
% GNU Radio data is collected in subfolder dat files, this code cycles
% through the subfolders and reads in the dat files
%   Input:
%       - directory: This is the location of the data files
%       - dateDat: This denotes which folders to cycle through via the date
%       - msg: specifies where data file was found
%   Output:
%       - data: This holds all of the channels data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function data = readDataFile(directory, dateDat, msg)
    % Determine length of date
    lenD = length(dateDat);
    % Get files in directory
    files = dir(directory);
    directoryNames = {files([files.isdir]).name};
     % Cycle through directory to add data subfolders
    for fold = 1:length(directoryNames)
        % Check if folder matches date 
        if length(directoryNames{fold}) >= lenD
            if strcmp(directoryNames{fold}(1:lenD), dateDat)
                % Save directory
                subDir = directoryNames{fold};
                % break out of loop
                break
            end
        elseif fold == length(directoryNames)
            % Add Warning and Break
            disp('Directory does not exist in specified folder, pausing.')
            pause(1);
        end
    end
    % Add subDir to path
    subDirPath = [directory subDir];
    addpath(subDirPath);
    % Grab dat files in folder
    dataFiles = dir([subDirPath '/*.dat']);
    % If there are three arguments, then check which variation is desired
    if nargin >2
        % Specify temporary storage function
        dataFilestmp = dataFiles([]);
        % Initialize index
        ind = 1;
        % Specify the desired argument
        for datFile = 1:length(dataFiles)
            % Check if file matches
            if strcmp(dataFiles(datFile, 1).name(5:end-7), msg)
                % Assign correct file name to temp storage
                dataFilestmp(ind) = dataFiles(datFile, 1);
                % increment index
                ind = ind + 1;
            end
        end
        % Reassign dataFiles
        dataFiles = dataFilestmp;
    end
    dataFilesName = strings(length(dataFiles), 1);
    for datFile = 1:length(dataFiles)
        dataFilesName(datFile, 1) = dataFiles(datFile, 1).name;
    end
%     data = zeros(length(dataFiles), 1);
    % Load in data files
    for d = 1:length(dataFilesName)
        % Load in data files
        fid = fopen(dataFilesName(d));
        dat = fread(fid, 'int16');
        fclose(fid);
        % Format
        dat = reshape(dat, [length(dat)/2 2]);
        dat = dat(:, 1) + 1j*dat(:, 2);
        % Put in data array
        data(d,:) = dat;
    end
end