%% Algorithm Selection Function
% This function determines which algorithm is needed to process the data
%   Input:
%       - dataTable: where the results are output
%       - dataindex: the index which corresponds to output
%       - LMat: the matrix which stores the model orders
%       - actualDist: the actual distance between the channels
%       - actChans: the 
%       - sampleRate: the sample rate to output
%       - dist: the distance conversion vector
%       - inst: the message corresponding to the data
%       - xMats: matrix which contains all the data to process the
%                algorithms with
%       - doPlot: specify if plotting is desired
%       - algs: denotes the algorithms to use
%   Output:
%       - dataTable: the table which outputs the estimates
%       - dataIndex: the index which coresponds to which line of the table
%                   can be used
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [dataTable, dataIndex] = algSelect(dataTable, dataIndex, LMat, ...
                           actualDist, actChans, sampleRate, dist, inst,...
                           xMats, doPlot, algs)
    % Set amount for interpolation
    interpolationAmt = 1;
    % Number of channels 
    M = 2;
    % Set nonzero offset for denominator
    epsilon = 0.1e-9;
    % Set step size for adaptive algorithms
    eta = 0.5e-3;
    % Set weighting variables for objective constraints
    alpha = 0.1e-4;
    gamma = 0.1e-4;
    phi = 0.1e-4;
    % Set default blocksize for faster algorithm processing
    maxBlockReset = 500;
    % Set default maximum iterations for algorithm processing
    maxIter = 1;
    % number of sample Rates
    numIts = size(xMats, 1);
    % number of seeds
    numSeeds = size(xMats, 2); 
    %  number of algorithms
    numAlgs = length(algs);
    % Cycle through algorithms
    for a = 1:numAlgs
        % Determine algorithm
        alg = algs{a};
        % Create figure
        if doPlot
            figure();
        end
        % Cycle through impulse responses first by sample rate
        for ir = 1:numIts
            % Save estimates to determine median, max and min estimate to
            % output to table
            algEst = zeros(numSeeds, 1);
            % Grab model order
            L = LMat(ir);
            % determine peak index for unitary vector
            peak_index = floor((L + 1)/2);
            % Start unit vector initial estimate
            hest = zeros(M*(L + 1), 1);
            hest(peak_index) = 1;
            % Grab distance conversion
            d = dist(ir);
            % Initialize estimation accumulation
            est = 0;
            % Then by seed
            for s = 1:numSeeds
                % Grab filtered data
                data = xMats{ir, s};
                % Determine length of data
                N = length(data);
                % Determine if algorithm matches CC
                if strcmp('CC', alg)
                    % just input the xmat straight into the CC algorithm
                    EstChannel = CC(data);
                    % Normalize
                    EstChannel = EstChannel/norm(EstChannel);
                    % Estimate Delay
                    [~, pe] = max(interp(abs(EstChannel), ...
                                                        interpolationAmt));
                    indexes = ...
                          interp(-(length(xmat) - 1):(length(xmat) - 1),...
                                                         interpolationAmt);
                    peak_correlation = indexes(pe)*d; 
                    diff_est_m = abs(peak_correlation); 
                    % Accumulate
                    est = est + diff_est_m;
                    % Store to compute median, max and min ests
                    algEst(s) = est;
                % Else, normal imput methods are available
                else
                    % Determine which model order to use
                    L = LMat(ir);
                    % Split into 2 channels
                    channel1 = (data(1, :)).';
                    channel2 = (data(2, :)).';
                    % Create toeplitz matrix
                    cols = channel1(L + 1:end); 
                    rows = flipud(channel1(1:L + 1));
                    X1 = toeplitz(cols, rows);
                    cols = channel2(L + 1:end); 
                    rows = flipud(channel2(1:L + 1));
                    X2 = toeplitz(cols, rows);
                    X = [X1 -X2];
                    switch alg
                        % If Xu et al.'s LS algorithm
                        case 'SVD'
                            EstChannel = LS(X, L, M);
                        % If Adaptive Eigenvalue Decomposition algorithm
                        case 'AED'
                            EstChannel = AED(hest, X, eta, epsilon, L, ...
                                                   N, M, maxBlockReset, ...
                                                                  maxIter);                                       
                        % If Modified Adaptive Eigenvalue Decompostion 
                        % algorithm
                        case 'ModAED'
                            EstChannel = modAED(hest, X, eta, epsilon, ...
                                                     L, N, M, ...
                                                   maxBlockReset, maxIter);                            
                        % If Modified Sparse Adaptive Eigenvalue 
                        % Decomposition algorithm
                        case 'ModAEDS'
                            EstChannel = AEDS(hest, X, eta, alpha, ...
                                                epsilon, L, N, M, ...
                                                   maxBlockReset, maxIter);                            
                        % If Non Sparse Adaptive Cross-Channel 
                        % Identification with Shift-Suppression algorithm
                        case 'NonSAXIS'
                            EstChannel = NS_AXIS(hest, X, eta, alpha, ...
                                              epsilon, L, N, M, ...
                                                maxBlockReset, maxIter, ...
                                                               peak_index);                                         
                        % If Adaptive Cross-Channel Identification with 
                        % Sparse Shift-Suppression algorithm
                        case 'AXIS'
                            EstChannel = AXIS(hest, X, eta, alpha, ...
                                             epsilon, L, N, M, ...
                                                maxBlockReset, maxIter, ...
                                                               peak_index);                                           
                        % If Varied Adaptive Cross-Channel Identification 
                        % with Sparse Shift-Suppression
                        case 'VAXIS'
                            EstChannel = vAXIS(hest, X, eta, alpha, ...
                                             epsilon, L, N, M, ...
                                                maxBlockReset, maxIter, ... 
                                                               peak_index);                                        
                        % If Dual Fixed-Peak Adaptive Cross-Channel 
                        % Identification with Spares Shift-Suppression 
                        % algorithm
                        case 'DFPAXIS'
                            EstChannel = DFPAXIS(hest, X, eta, alpha, ...
                                                 epsilon, L, N, M, ...
                                               maxBlockReset, maxIter, ... 
                                                               peak_index);
                        % If Non Sparse "Brute Force" algorithm
                        case 'NSBF'
                            EstChannel = BF_NS(hest, X, eta, alpha, ...
                                                    epsilon, L, N, M, ...
                                                        N - L, maxIter, ...
                                                               peak_index);
                        % If NonAdaptive "Brute Force" algorithm
                        case 'NBF'
                            EstChannel = BF(hest, X, eta, alpha, ...
                                                    epsilon, L, N, M, ...
                                                        N - L, maxIter, ...
                                                               peak_index);
                        % If Adaptive "Brute Force" algorithm
                        case 'ABF'
                            EstChannel = BF(hest, X, eta, alpha, ...
                                            epsilon, L, N, M, ...
                                                maxBlockReset, maxIter, ...
                                                               peak_index);
                        % If Varied "Brute Force" algorithm
                        case 'VBF'
                            EstChannel = vBF(hest, X, eta, alpha, ...
                                              epsilon, L, N, M, ...
                                                maxBlockReset, maxIter, ...
                                                               peak_index);
                        % If Dual Fixed-Peak "Brute Force" algorithm
                        case 'DFPBF'
                            EstChannel = DFPBF(hest, X, eta, alpha, ...
                                              epsilon, L, N, M, ...
                                                maxBlockReset, maxIter, ...
                                                               peak_index);
                        % If Non-Sparse Single Constraint algorithm
                        case 'NSSC'
                            EstChannel = NS_SC(hest, X, eta, alpha, ...
                                            gamma, epsilon, L, N, M, ...
                                               maxBlockReset,  maxIter, ...
                                                               peak_index);                                       
                        % If Single Constraint algorithm
                        case 'SC'
                            EstChannel = SC(hest, X, eta, alpha, gamma, ...
                                           epsilon, L, N, M, ...
                                              maxBlockReset,  maxIter, ...
                                                               peak_index);                                          
                        % If Varied Single Constraint algorithm
                        case 'VSC'
                            EstChannel = vSC(hest, X, eta, alpha, gamma,...
                                              epsilon, L, N, M, ...
                                                maxBlockReset, maxIter, ...
                                                               peak_index);                                          
                        % If Dual Fixed-Peak Single Constraint algorithm
                        case 'DFPSC'
                            EstChannel = DFPSC(hest, X, eta, alpha, ...
                                                 gamma, phi, epsilon,L, ...
                                                   N, M, maxBlockReset, ...
                                                      maxIter, peak_index);                                 
                    end
                    % Try to compute average
                    try
                        % If the numbers work out
                        [~, ind1] = max(abs((EstChannel(:, 1))));
                        [~, ind2] = max(abs((EstChannel(:, 2))));
                        diff_est_m = abs((ind1 - ind2)*d*...
                                         interpolationAmt);
                    catch
                       diff_est_m = NaN;
                    end
                    % Accumulate
                    est = est + diff_est_m;
                    % Store to compute median, max and min ests
                    algEst(s) = diff_est_m;
                    % Check if plot is desired
                    if doPlot == 1
                        subplot(121); 
                        plot(abs(actChans{s}(1, :))); hold on;
                        plot(abs(actChans{s}(2, :))); hold off;
                        % Normalize Estimates for plotting
                        estChannel1 = (EstChannel(:, 1))/...
                                                    norm(EstChannel(:, 1));
                        estChannel2 = (EstChannel(:, 2))/...
                                                    norm(EstChannel(:, 2));
                        subplot(122);
                        plot(abs(estChannel2)); hold on;
                        plot(abs(estChannel1)); hold off;
                        sR = (sampleRate{1});
                        suptitle(sprintf('%s-%i, %s: Act Channel vs Est Channel', ...
                                    sR, s-1, alg));
                        pause(1);
                    end
                end
            end
            % Average
            avgEst = est/numSeeds;
            % Compute Min, Max and Median Estimates
            minEst = min(algEst);
            medEst = median(algEst);
            maxEst = max(algEst);
            % Compute error
            err = abs(abs(actualDist) - avgEst);
            % Place in Table
            dataTable(dataIndex, :) = {inst, '&', sampleRate, '&', alg, ...
                '&', actualDist, '&', avgEst, '&' minEst, '&', medEst, ...
                '&', maxEst, '&', err, '\\'};
            % Increment dataIndex
            dataIndex = dataIndex + 1;
        end
    end
    
    
    