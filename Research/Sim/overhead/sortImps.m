%% Sort impulse responses
% Simulated Impulse responses are .mat files, which need to be sorted into
% groupings by either or both seed and sample rate
%   Input:
%       - directory: the directory where the impulse responses are stored
%       - smplespec: flag which denotes whether the sample rate is
%               specified
%       - seedspec: flag which denotes whether the seed is specified
%       - smplRate: specifies which sample rate(s) is(are) desired, only
%               required if the smplespec is flagged
%       - se: specifies which seed(s) is(are) desired, only required if the
%               seedspec is flagged
%   Output:
%       - namImps: lists the names of the impulse responses meeting the
%               requirements specified
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function namImps = sortImps(directory, smpleSpec, seedSpec, smplRate, se)
    % Check if nargins match flags
    if nargin < 4
        % Check if smpleSpec
        if smpleSpec == 1
            % Error
            msg = 'Error, not enough arguments, sample rate must be specified\n';
            error(msg); 
        % Check if speedSpec
        elseif seedSpec == 1
            % Error
            msg = ('Error, not enough arguments, seed must be specified\n');
            error(msg);
        end
    end
    % Determine names of files in directory
    impulseFiles = dir([directory '*.mat']);
    namesImpulseResponse = strings(length(impulseFiles), 1);
    for files = 1:length(impulseFiles)
        namesImpulseResponse(files) = impulseFiles(files, 1).name;
    end
    % Create variable(s) to store names
    L_imp = strings; L_imp_row = 1;
    N_imp = strings; N_imp_row = 1;
    % Determine is smpleSpec is required
    if smpleSpec
        % Assign a converted smplerate array
        smple = zeros(length(smplRate), 1);
        % Cycle though sample rate array
        for r = 1:length(smplRate)
            % grab number from string
            smple(r) = str2double(smplRate{r}(1:end-4));
            smple(r) = smple*1e6;
        end
    end
    % Cycle through names
    for row = 1:length(namesImpulseResponse)
        % Assert smpleSpec flag has not been met
        smpleRateMatch = 0;
        % get name
        n = convertStringsToChars(namesImpulseResponse(row));
        % Determine sample rate
        num = str2double(n(end - 17:end - 10));
        % Check if valid sample rate
        if isnan(num)
            % shift where sample rate is grabbed (may need to be modified
            % for numbers containing more than 2 digits)
            num = str2double(n(end - 18:end - 11));
        end
        % If smpleSpec
        if smpleSpec
            % Cycle though sample rate array
            for r = 1:length(smple) 
                % Check if sample rates match
                if num == smple(r)
                    % Flag that a match has been found
                    smpleRateMatch = 1;
                end
            end
            % Check if no sample rate has been found
            if ~smpleRateMatch
                % Move to next iteration
                continue;
            end
        end
        % Get first letter
        letter = n(1);
        % Impulse sorting
        sort_tf = 1;
        index = 1;
         % While sorting is on, 
        while(sort_tf)
            % If it is large impulse response
            if letter == 'L'
                % Determine which row
                if L_imp_row == 1
                    % If the first row of L, replace first variable
                    L_imp(1) = n;
                    % set L row to false
                    L_imp_row = 0;
                    % Get out of loop
                    sort_tf = 0;
                % If not, continue processing
                else
                    % Get sample rate index 
                    tmpStr = convertStringsToChars(L_imp(index));
                    % Check if the sample rate is less than the current 
                    % index sample rate
                    if num < str2double(tmpStr(end - 17:end - 10))
                        % Place new name in 
                        L_imp = [L_imp(1:index - 1), n, L_imp(index:end)];
                        % Get out if loop
                        sort_tf = 0;
                    % Check if index is last for L_imp
                    elseif index == length(L_imp)
                        % Place new name in 
                        L_imp = [L_imp(1:index), n];
                        % Get out of loop
                        sort_tf = 0;
                    % If greater, increase index
                    else
                        % Increment index
                        index = index + 1;
                    end
                end
            % Else if normal impulse response
            else
                % Determine which row of sorted array
                if N_imp_row == 1
                    % If the first row of L, replace first variable
                    N_imp(1) = n;
                    % set L row to false
                    N_imp_row = 0;
                    % Get out of loop
                    sort_tf = 0;
                % If not, continue processing
                else
                    % Get sample rate index 
                    tmpStr = convertStringsToChars(N_imp(index));
                    % Check if the sample rate is less than the current 
                    % index sample rate
                    if num < str2double(tmpStr(end - 17:end - 10))
                        % Place new name in 
                        N_imp = [N_imp(1:index - 1), n, N_imp(index:end)];
                        sort_tf = 0;
                    % Check if index is last for L_imp
                    elseif index == length(N_imp)
                        % Place new name in 
                        N_imp = [N_imp(1:index), n];
                        % Get out of loop
                        sort_tf = 0;
                    % If greater, increase index
                    else
                        % Increment index
                        index = index + 1;
                    end
                end
            end
        end
    end
    % Collect all impulse responses
    sorted_array = [L_imp; N_imp];
    % Specify total possible seed values
    totalSeeds = [0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20];
    % Check if sample Rate and seed was specified
    if smpleSpec && seedSpec
        % If seed is specified, 
        aSeed = se;
        % If specified only contain samples and seeds specified
        seed = strings(2, length(aSeed), length(smple)); 
    % Else if only sample Rate was specified  
    elseif smpleSpec
        % Else if seed is not specified
         aSeed = totalSeeds;
        % If specified, only contain the samples specified
        seed = strings(2, length(aSeed), length(smple)); 
    % Else if only seed was specified
    elseif seedSpec
         % If seed is specified, 
        aSeed = se;
        % If specified only contain the seeds specified
        seed = strings(2, length(aSeed), ...
            length(sorted_array)/length(totalSeeds)*length(se));
    % Else no specification
    else
        % Else if seed is not specified
         aSeed = totalSeeds;
        % Else use all sample rates
         seed = strings(2, length(aSeed), ...
                           length(namesImpulseResponse)/(2*length(aSeed)));
    end
    seed_row = ones(2, length(aSeed));
    % Cycle through newly sorted arrays
    for arr = 1:2
        % Cycle through arrays
        for idx = 1:length(sorted_array)
            % Assert seedSpec flag has not been met
            seedMatch = 0;
            % Get name of tranfer function
            n = convertStringsToChars(sorted_array(arr, idx));
            % Determine seed
            s = str2double(n(end-5:end-4));
            % Check if valid number
            if isnan(s)
                % Set seed to smaller amount (note may need to be
                % modified for numbers larger than 2 digits)
                s = str2double(n(end-4));
            end
            % Check if seed is specified
            if seedSpec
                % Cycle through seed array
                for seds = 1:length(se)
                    % Check if sample rates match
                    if s == se(seds)
                       % Flag that a match has been found
                        seedMatch = 1;
                    end
                end
                % Check if no seed has been found
                if ~seedMatch
                    % Move to next iteration
                    continue;
                end 
            end
            % Determine which of the seeds match the seed from the
            % impulse response
            id = find(aSeed == s);
            % Place IR into index, acknowledging that the impulse
            % responses should already be in numerical order based on
            % previous sort
            seed(arr, id, seed_row(arr, id)) = ...
                                              convertCharsToStrings(n);
            % Increment row
            seed_row(arr, id) = seed_row(arr, id) + 1;                  
        end
    end
    namImps = seed;
end