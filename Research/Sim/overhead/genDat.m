%% Simulate data
% Simulated data processed accordingly for 2 channels--for more channels,
% more edits may be needed
%   Input:
%       - M: number of channels (must be 2)
%       - N: size of data
%       - Li: model order selection
%       - ir: impulse response set
%       - sigma: noise added
%       - bandwidth: narrow or wideband signal bandwidth
%       - channels: specify which simulated channels are desired
%       - numImps: the number of impulse responses
%       - numSeeds: the number of seed sets
%
%   Output:
%       - xMats: the cell array containing all of the data variations
%       - L: vector contains model order for each sampling rate
%       - dist: vector containing the distance estimate for each sampling
%               rate
%       - actDist: the actual distance between receivers 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [xMats, L, dist, actDist, actChans] = genDat(M, N, Li, ir, ...
                             sigma, bandwidth, channels, numImps, numSeeds)
    % Make Lvec function for over or underestimating L
    Lvec = @(L) [L, L/2, L-10, 2*L, L+10];
    % Create storage for model orders
    L = zeros(numImps, numSeeds);
    % Create storage for distance conversions
    dist = zeros(numImps, numSeeds);
    % Initialize filtered signal and prep for data Matrix
    xMats = cell(numImps, numSeeds);
    % Initialize channel storage for comparison
    actChans = cell(numImps, numSeeds);
    % Generate white data
    xsig = 0.1*randn(1, N) + 1j*0.1*randn(1, N);
    % Determine size of impulse response vector
    sIR = size(ir);
    % Set speed of light variable in M(ega)m/s (3e8 -> 300e6 m/s)
    c = 300;
    % Cycle through different impulse sets
    for i = 1:numImps
        % Cycle through different seeds
        for s = 1:numSeeds
            % Check size of impulse response vector
            if length(sIR) > 2
                % Get impulse response (with three dimensions)
                load(ir(1, s, i));
            elseif length(sIR) > 1
                % Get impulse response (with two dimensions)
                load(ir(i, s));
            end
            % Grab data matrix from storage
            x = zeros(M, N); 
            % Calculate samplingRate to MHz
            Fs_MHz = Fs/1e6;
            % Compute distance conversion
            dist(i, s) = c/Fs_MHz;
            % Compute L
            Ltest = Fs_MHz; Ltest= Lvec(Ltest); 
            L(i, s) = floor(Ltest(Li)) - 1;
            % Extract channels for impulse response filtering
            h = hs(channels, :);
            % Set interpolation amount and decimation amount (for band 
            % limited noise, 8 MHz)
            if strcmp(bandwidth, 'n')
                decimation = Fs_MHz/8;
            else
                decimation = 1;
            end
            % Filter signal
            for m = 1:M
                % Normalize impulse response to remove power 
                % discrepancies
                h(m, :) = h(m, :)/norm(h(m, :));
                % Check which bandwidth before further filtering (if
                % needed)
                if strcmp(bandwidth, 'w')
                    % If wideband, merely transfer variables
                    x(m, :) = xsig;
                elseif strcmp(bandwidth, 'n')
                    % If narrowband, filter signal to bandlimited
                    [b, a] = cheby1(10, 0.5, (4/Fs_MHz*2));
                    x(m, :) = filter(b, a, xsig);
                end
                % Then filter signal with transfer function
                x(m, :) = filter(h(m , :), 1, x(m, :));
                % Add some noise for realism
                x(m, :) = (x(m, :) + sigma*(randn(1, N) + 1j*randn(1, N)));
            end
            % Check which bandwidth before decimation (if needed)
            if strcmp(bandwidth, 'wideband')
                % merely do a straight transfer over
                xmat = x;
            else
                for m = 1:M
                    % Decimate if bandlimited
                    xmat(m, :) = decimate(x(m, :), decimation);
                end
            end
            % Save Channels
            actChans{i, s} = h;
            % Save filtered data
            xMats{i, s} = xmat;
        end
    end
    % Compute the actual delay amount
    actDist = (min(recs.d(channels(1), :)) - min(recs.d(channels(2), :)));
    % Only one column of these matrices are needed as they repeat
    dist = dist(:, 1);
    L = L(:, 1);
end
    