%% Overhead - Sim
% This version incorporates mutiple variations of the functions for ease of
% processing for simulation data
clear all; close all; clc;
% Add all files needed to working directory
dirs{1} = '/home/madi/Research_2018-2019/thesis/ms_ee_thesis/Research/Sim/algorithms/';
dirs{2} = '/home/madi/Research_2018-2019/thesis/ms_ee_thesis/Research/Sim/impulseResponses/';
% Cycle through and add paths
for fold = 1:length(dirs)
    % Add folders
    addpath(dirs{fold});
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialization and Pre-Processing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initializations and pre-processing for algorithm comparison
    % Initialize file to output results to
    fileName = ['Method_Comparisons_' date '_v1.csv'];
    % Check if file already exits to prevent overwrite
    while exist(fileName, 'file')
        % Determine version
        if fileName(end - 5) == 'v'
            % If single digits
            ver = str2double(fileName(end-4));
        elseif fileName(end - 6) == 'v'
            % If double digits
            ver = str2double(fileName(end-5:end-4));
        elseif fileName(end - 7) == 'v'
            % If triple digits
            str2double(fileName(end-6:end-4));
        else
            % If more
            msgl1 = 'HELP! More than triple digits method comparison\n';
            msgl2 = 'files, add more to if statement';
            msg = [msgl1 msgl2];
            error(msg);
        end
        % Update version
        new_ver = ver + 1;
        % Determine new filename
        fileName = ['Method_Comparisons_' date '_v' ...
                                                  num2str(new_ver) '.csv'];
    end
    % Set first row of data table array
    header = {'Special_Message', '&', 'Sample_Rate', '&', 'Algorithm', '&'...
               'Actual_Dist_m', '&', 'Estimate_m', '&','Min_Est_m', '&'...
               'Median_Est_m', '&', 'Max_Est_m', '&', 'Error_m', '\\'};
           
    % Set index for writing to file
    dataIndex = 1;
    % Specify if plotting is desired
    doPlot = 1;
    % Set algorithms (1:19 selects which algorithm to run, NOTE multiple
    % algorithms can be selected)
    alg_list = {'CC', 'SVD', ... % [1 2]
            'A_AED', 'AED', 'ModAED', 'ModAEDS', ... %[3 4 5 6]
            'AXIS', 'VAXIS', 'DFPAXIS', 'NonSAXIS',... %[7 8 9 10]
            'SC', 'VSC', 'DFPSC', 'NonSSC'...%[11 12 13 14]
            'NBF', 'ABF', 'VBF', 'DFPBF', 'NonSBF'};%[15 16 17 18 19]
    algs = alg_list([6 10]);
    % Specify which processing situations are desired:
    %   1: Ideal situation
    %   2: Narrowband
    %   3: Noisy
    %   4: Fewer Samples
    %   5: Wrong Model Order: L/2
    %   6: Wrong Model Order: L-10
    %   7: Wrong Model Order: 2L
    %   8: Wrong Model Order: L+10
    %   9: Normalized Random Start
%     sit = [1 2 3 4 5 6 7 8 9];
    sit = [1];
    % Specify which sample rates are desired for processing: 
    %   16 MHz, 20 MHz, 40 MHz, 60 MHz, 80 MHz, 100 MHz, 120 MHz, 140 MHz, 
    %   150 MHz, 160 MHz, 180 MHz, 200 MHz
    smplRate = {'40 MHz'};
    % Specify which seed is desired for processing Must be in ascending
    % order, and include all seeds between 0 to 20
    aSeed = [2];
    % Flag if specific seed is desired 
    seedSpec = 1;
    % Flag if specified sample rate is desired, if not, then all sample
    % rates will be processed
    smpleSpec = 1;
    % Sort impulse responses
    namImps = sortImps(dirs{2}, smpleSpec, seedSpec, smplRate, aSeed);
    % Make arrays for processing:
    %   bandwidth: corresponds to  wide or narrow signal bandwidth
    %   Lind: corresponds to different L lengths
    %   sigma: corresponds to SNR values
    %   dataSize: corresponds to different data sizes
    %   IR: corresponds to different impulse response sets
    bandwidth = {'w', 'n'};
    Lind = 1:5;
    sigma = [0.1e-2 0.1e-6];
    dataSize = [30e1 30e3];%[250e1 250e3];
    IR = [2]; % [1, 2]
    % If SVD is used, dataSizes must be changed to avoid overflowing the
    % memory
    if ~isempty(find(strcmp(algs, 'SVD'), 1))
         % Then dues to memory issues, the amount of data must be
            % minimized 
            dataSize= [30e1 30e3];
    end
    % Specify channels (1:4096)
    channels = [30 3000];
    % Specify number of columns for data table
    datTabCols = length(header);
    % Specify number of algorithms for table
    numAlgs = length(algs);
    % Specify number of samples rates for table
    numSmpl = size(namImps, 3);
    % Specify number of situations for table
    numSits = length(sit);
    % Specify number which seeds were used
    numSeeds = size(namImps, 2);
    % Specify which impulse response sets
    numIR = length(IR);
    % Create dataTable
    dataTable =  cell(numIR*numSits*numAlgs*numSmpl*numSits, datTabCols);
    % Cyle through impulse response sets
    for i = 1:numIR
        % Set impulse response set
        ir = IR(i);
        % For special instructions, specify which IR set
        set = sprintf('IR-%i', ir);
        % Cycle through situations
        for s = 1:numSits 
            % Determine which sitution
            switch sit(s)
                % Ideal sitution
                case 1
                    % Special instructions
                    inst = [set, ': Ideal'];
                    % Generate data for situation
                    [xMats, L, dist, actDistance, actChans] = genDat(2, ...
                                dataSize(2), Lind(1), ...
                                namImps(ir, :, :), sigma(2), ...
                                bandwidth(1), channels, numSmpl, numSeeds);
                % Narrowband situation
                case 2
                    % Special instructions
                    inst = [set, ': Narrowband'];
                    % Generate data for situation
                    [xMats, L, dist, actDistance, actChans] = genDat(2, ...
                                dataSize(2), Lind(1), ...
                                namImps(ir, :, :), sigma(2), ...
                                bandwidth(2), channels, numSmpl, numSeeds);
                % Noisy Situation
                case 3
                    % Special instructions
                    inst = [set, ': Noisy'];
                    % Generate data for situation
                    [xMats, L, dist, actDistance, actChans] = genDat(2, ...
                                dataSize(2), Lind(1), ...
                                namImps(ir, :, :), sigma(1), ...
                                bandwidth(1), channels, numSmpl, numSeeds);
                % Fewer Samples Situation
                case 4
                    % Special instructions
                    inst = [set, ': Fewer Samples'];
                    % Generate data for situation
                    [xMats, L, dist, actDistance, actChans] = genDat(2,...
                                dataSize(1), Lind(1), ...
                                namImps(ir, :, :), sigma(1), ...
                                bandwidth(1), channels, numSmpl, numSeeds);
                % Wrong Model order, L/2, situation
                case 5
                    % Special instructions
                    inst = [set ': L/2']; 
                    % Generate data for situation
                    [xMats, L, dist, actDistance, actChans] = genDat(2, ...
                                dataSize(2), Lind(2), ...
                                namImps(ir, :, :), sigma(1),...
                                bandwidth(1), channels, numSmpl, numSeeds);
                % Wrong Model order, L-10, situation
                case 6
                    % Special instructions
                    inst = [set ': L-10 '];
                    % Generate data for situation
                    [xMats, L, dist, actDistance, actChans] = genDat(2, ...
                                dataSize(2), Lind(3), ...
                                namImps(ir, :, :), sigma(1), ...
                                bandwidth(1), channels, numSmpl, numSeeds);
                % Wrong Model order, 2L, situation
                case 7
                    % Special instructions
                    inst = [set ': 2L'];
                    % Generate data for situation
                    [xMats, L, dist, actDistance, actChans] = genDat(2, ...
                                dataSize(2), Lind(4), ...
                                namImps(ir, :, :), sigma(1), ...
                                bandwidth(1), channels, numSmpl, numSeeds);
                % Wrong Modle order, L+10, 
                case 8
                    % Special instructions
                    inst = [set ': L+10']; 
                    % Generate data for situation
                    [xMats, L, dist, actDistance, actChans] = genDat(2,...
                                dataSize(2), Lind(5), ...
                                namImps(ir, :, :), sigma(1), ...
                                bandwidth(1), channels, numSmpl, numSeeds);
                % Normalized Random Startcase 9
                case 9
                    % Special instructions
                    inst = [set ': Random Start'];
                     % Generate data for situation
                    [xMats, L, dist, actDistance, actChans] = genDat(2, ...
                                dataSize(2), Lind(1), ...
                                namImps(ir, :, :), sigma(2), ...
                                bandwidth(1), channels, numSmpl, numSeeds);
            end
            [dataTable, dataIndex] = algSelect(dataTable, dataIndex, L, ...
                            actDistance, actChans, smplRate, dist, inst,...
                                                      xMats, doPlot, algs);
        end
    end
    % Outpute to File
    T = cell2table(vertcat(header, dataTable));
    writetable(T, fileName, 'Delimiter', ' ');
    
           
          