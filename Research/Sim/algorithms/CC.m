%% CC Function
% Cross Correlation Delay Estimation Algorithm 
%   - The channel estimation algorithm that utilizes the SVD
%   Inputs:
%       - x: data vector size: MxN
%   Outputs:
%       - correlation: vector containing cross correlation values
function correlation = CC(x)
%% Pre-processing
    % Separate matrices
    x1 = x(1, :);
    x2 = x(2, :);
%% Processing/Output
    % Get correlation
    correlation = xcorr(x1, x2);
end