\section{Modified Algorithms} \label{sec:modAlgs}
Using the AED algorithm as a basis, two similar complex algorithms were derived during the course of this research. These two algorithms are then used as a basis for the algorithms developed in Chapter \ref{ch:5}.

\subsection{Modified System Model}
The system model explained here is similar to the model used for the AED and SVD algorithms, shown in Figure \ref{fig:transmission_figure}.
It should be noted that the AED algorithm was originally created for acoustic signals, whereas the data transmitted in this research are assumed to be complex RF signals. The SVD algorithm generalizes naturally for complex signals and needs no modification, but for ease of computation, a slightly different complex model is used for all the other algorithms in this thesis. This affects the cross relation equations on which these algorithms are based on, and so is explained in detail here.

As before, consider a source, at some unknown location, transmitting an unknown signal, $s(n)$, in a multipath environment. The transmission is detected by multiple receivers. The system is modeled as
\begin{equation}
\label{eq:system_conj}
\begin{cases}
x_{1}(n)=s(n)*h^*_{1}(n)+w_{1}(n) \\ 
\ \vdots \\ 
x_{M}(n)=s(n)*h^*_{M}(n)+w_{M}(n), 
\end{cases}
\end{equation}
where $*$ denotes convolution, $h^*_{i}(n)$ denotes the complex conjugate of each individual channel mapping from the source to the $i^\mathrm{th}$ receiver, $x_{i}(n)$ the value received from the transmitted signal at the $i^{th}$ receiver, and $\mathrm{w}(n) = [w_{1}(n)\hspace{0.1cm}w_{2}(n)...w_{M}(n)]$ is additive white noise, and $n$ denotes the $n^\mathrm{th}$ sample. $M$ denotes the number of receivers. This model also assumes a finite impulse response, and is a SIMO system.
\subsection{Complex Cross Relation}
As done previously, neglecting noise, the signal model, (\ref{eq:system_conj}), can be expressed individually as $x_i(n) = s(n)*h^*_i(n)$. Convolving $x_i(n)$ with $h^*_j(n)$, where $i \neq j$ produces a complex CR, 
\begin{equation}
\label{eq:cr-derivation_conj}
\begin{split}
h^*_j(n)*x_i(n) = h^*_j(n)*[s(n)*h^*_i(n)] \\
\boxed{h^*_j(n)*x_i(n) - h^*_i(n)*x_j(n) = 0.}
\end{split}
\end{equation}
Equation (\ref{eq:cr-derivation_conj}) can also be expanded for two receiver measurements as the matrix equation\footnote{For multiple receivers ($>2$), Equation (\ref{eq:cr-matrix_conj}) can be modified as seen in the appendix, \ref{app:notation}.}

\begin{equation} \label{eq:cr-matrix_conj}
\begin{bmatrix} \mathrm{X}_i(L) & \vdots & -\mathrm{X}_j(L)
\end{bmatrix}
\begin{bmatrix} \mathbf{h}^*_j \\ \mathbf{h}^*_i \end{bmatrix} = 0,
\end{equation}
where $\mathrm{X}_m(L)$ is a Toeplitz matrix of the same form described in Section \ref{sec:ch_4_LS_conj_cr}, and $\mathbf{h}^*_m$ is the impulse response vector 
\begin{equation} \notag
\mathbf{h}^*_m \triangleq [h_m(0), \cdots, h_m(L)]^{\mathrm{H}}.
\end{equation}
For brevity, (\ref{eq:cr-matrix_conj}) can be rewritten as
\begin{equation} \label{eq:cr-brief_conj}
\mathbf{X}\mathbf{h}^* = \mathbf{0}.
\end{equation}
Equation (\ref{eq:cr-brief_conj}) is the basis for the approaches derived and explored in this research. As explained in Section \ref{sec:ch_4_LS_conj_cr},  (\ref{eq:cr-brief_conj}) implies that $\mathbf{h}^*$ is in the null space of $\mathbf{X}$.

\subsection{Modified Adaptive Eigenvalue Decomposition}
The Modified Adaptive Eigenvalue Decomposition (ModAED) also takes an adaptive least-squares approach like, and produces a similar update to the AED algorithm. This algorithm uses a different objective function, using (\ref{eq:cr-brief_conj}) as a constraint. The ModAED algorithm looks at minimizes the change between updates, resembling an error minimization 
\begin{equation} \label{eq:modAED_objective}
\begin{aligned}
& \min_{{\mathbf{h}^{(n + 1)}}^{\mathrm{*}}}&  &||\mathbf{h}^{(n + 1)} - \mathbf{h}^{(n)}||_2^2& \\
& \text{subject to}&  &{\mathbf{h}^{(n + 1)}}^\mathrm{H}\mathrm{x}_{r} = 0,&
\end{aligned}
\end{equation}
where $\mathrm{x}_{r}$ is a row of the data matrix $\mathbf{X}$,  transposed, as all vectors are assumed to be column formatted.

The update equation which solves this objective function is derived using typical minimization techniques.
Initally setting up the Lagrangian based on (\ref{eq:modAED_objective}) produces
\begin{equation} \label{eq:modAED_lagrangian}
    \begin{aligned}
        \mathcal{L} = {\mathbf{h}^{(n + 1)}}^\mathrm{H}\mathbf{h}^{(n + 1)}
        - {\mathbf{h}^{(n + 1)}}^\mathrm{H}\mathbf{h}^{(n)}
        - {\mathbf{h}^{(n)}}^\mathrm{H}\mathbf{h}^{(n + 1)}
        + {\mathbf{h}^{(n)}}^\mathrm{H}\mathbf{h}^{(n)}
        + \lambda{\mathbf{h}^{(n + 1)}}^\mathrm{H}\mathrm{x}_{r}.
    \end{aligned}
  \end{equation}

 The update is then obtained by taking the gradient derivative of (\ref{eq:modAED_lagrangian}), with respect to the updated transfer function estimate, $\mathbf{h}^{(n + 1)}$.
 Then, setting the derivative equal to 0 and solving for the updated transfer function estimate  forms
\begin{equation} \label{eq:AED_update_lambda}
    \begin{aligned}
       &\frac{d}{d\mathbf{h}^{(n + 1)}}\mathcal{L}& &=& &2\mathbf{h}^{(n + 1)} - 2\mathbf{h}^{(n)} + \lambda\mathrm{x}_r& \\
       &\mathbf{h}^{(n + 1)}& &=& &\mathbf{h}^{(n)} - \lambda\mathrm{x}_r.&
    \end{aligned}
\end{equation}

The value for $\lambda$ is found by substituting the results of (\ref{eq:AED_update_lambda}) into the constraint listed in (\ref{eq:AED_objective}) as
\begin{equation} \label{eq:AED_constraint}
    \begin{aligned}
        \tilde{\lambda} = \frac{{\mathbf{h}^{(n)}}^\mathrm{H}\mathrm{x}_r}
        {||\mathrm{x}_r||_2^2}.
    \end{aligned}
\end{equation}
Note that $\tilde{\lambda} = \lambda^\mathrm{H}$. Replacing the $\lambda$ in (\ref{eq:AED_update_lambda}) with the conjugate of (\ref{eq:AED_constraint}) provides the update used in the ModAED algorithm,
\begin{equation} \label{eq:modAED_final_update}
    \begin{aligned}
        \mathbf{h}^{(n + 1)} = \mathbf{h}^{(n)} - \eta\frac{\mathrm{x}_r^\mathrm{H}{\mathbf{h}^{(n)}}}
        {||\mathrm{x}_r||_2^2}\mathrm{x}_r.
    \end{aligned}
\end{equation}
Note, $\eta$ in (\ref{eq:modAED_final_update}) was added (as a step size) to make the equation more adaptive, limiting the change between steps. 

The complete derivation and the code listing for the ModAED algorithm are found in Appendix \ref{app:AED}, and \ref{code:modAED} respectively.

\subsection{Modified AED with Sparsity} 
Some sources claim that a sparsity constraint is needed in order to accurately estimate the channels \cite{art:AdaptBESparse, art:IDinBDeconv, art:SparseBDeConv}. The second algorithm derived during the course of this research implemented the sparsity constraint with Modified AED algorithm, changing 
 the objective function only slightly to
\begin{equation} \label{eq:modAEDS_objective}
    \begin{aligned}
        & \min_{{\mathbf{h}^{(n + 1)}}^\mathrm{H}}&  &||\mathbf{h}^{(n + 1)} - \mathbf{h}^{(n)}||_2^2 + \alpha||{\mathbf{h}^{(n + 1)}}^\mathrm{*}||_1& \\
        & \text{subject to}&  &{\mathbf{h}^{(n + 1)}}^\mathrm{H}\mathrm{x}_{r} = 0.&
    \end{aligned}
\end{equation}
Sparsity is typically enforced by utilizing an $L_1$-norm \cite{art:IDinBDeconv,art:AdaptiveTDEUsingFilterConstraints,thesis:SLvTDOA, art:SparseBDeConv, conf:ETDOARoomRefl}.

The update equation is derived, as before, by first setting up the Lagrangian
\begin{equation} \label{eq:lagrangian_modAEDS}
    \mathcal{L} = {\mathbf{h}^{(n + 1)}}^\mathrm{H}{\mathbf{h}^{(n + 1)}} -
                  {\mathbf{h}^{(n + 1)}}^\mathrm{H}{\mathbf{h}^{(n)}} -
                  {\mathbf{h}^{(n)}}^\mathrm{H}{\mathbf{h}^{(n + 1)}} + 
                  {\mathbf{h}^{(n)}}^\mathrm{H}{\mathbf{h}^{(n)}} +
                  \alpha||{\mathbf{h}^{(n + 1)}}^\mathrm{H}||_1 +
                  \lambda{\mathbf{h}^{(n + 1)}}^\mathrm{H}\mathrm{x}_r.
\end{equation}
Taking the derivative of (\ref{eq:lagrangian_modAEDS}) is more complicated due to the $L_{\mathrm{1}}$ norm, as the $L_{\mathrm{1}}$ norm contains a discontinuity at 0.
It is common practice to use a subgradient approximation \cite{web:subgradient} in place of the true derivative for this reason.
The subgradient for the $L_{\mathrm{1}}$ norm in (\ref{eq:lagrangian_modAEDS}) is $\text{sign}(\mathbf{h}^{(n + 1)})$.  As the data is complex, it is more mathematically correct to isolate the phase of the transfer function estimates to determine which direction they are (positive or negative), $\text{sign}(\mathbf{h}^{(n + 1)}) = \mathbf{e}^{j\angle\mathbf{h}^{(n + 1)}}$,
\begin{equation} \label{eq:modAEDS_derivative}
	\frac{d}{d{\mathbf{h}^{(n + 1)}}^*}\mathcal{L} = 
	\mathbf{h}^{(n + 1)} - 
	\mathbf{h}^{(n)} + 
	\alpha{\mathbf{e}^{j\angle\mathbf{h}^{(n + 1)}}} 
	+ \lambda\mathrm{x}_r
\end{equation}

An assumption is made in order to compute the transfer function, as only a small change is assumed to happen each iteration,
$
    \text{sgn}(\mathbf{h}^{(n + 1)}) \approx \text{sgn}(\mathbf{h}^{(n)}).
$
This assumption allows simply solving for the update, $\mathbf{h}^{(n + 1)}$, without the complexity of an added sign term.

Using this approximation and assumption of the $L_{\mathrm{1}}$ norm, the update can be found by setting (\ref{eq:modAEDS_derivative}) equal to 0, and solving for the update,
\begin{equation} \label{eq:modAEDS_update_lambda}
       \mathbf{h}^{(n + 1)} =
       \mathbf{h}^{(n)} - 
       \alpha\mathbf{e}^{j\angle\mathbf{h}^{(n)}} -
       \lambda\mathrm{x}_r.
\end{equation}

The value of $\lambda$ is found, as before, by substituting the results of  (\ref{eq:modAEDS_update_lambda}) into the constraint listed in (\ref{eq:modAEDS_objective}) resulting in a value similar to (\ref{eq:AED_constraint}), with the sparsity constraint added in,
\begin{equation} \label{eq:modAEDS_constraint}
    \lambda = \frac{(\mathbf{h}^{(n)} - \alpha\mathbf{e}^{j\angle\mathbf{h}^{(n)}})^\mathrm{H}\mathrm{x}_r}{||
    \mathrm{x}_r||_2^2}.
\end{equation}

Replacing the value for $\lambda$ in (\ref{eq:modAEDS_update_lambda}) with the results in (\ref{eq:modAEDS_objective}) produces the update used in the sparse ModAED (ModAEDS) algorithm,
\begin{equation}
    \begin{aligned}
        \mathbf{h}^{(n+1)} = \mathbf{h}^{(n)}  + \alpha\mathbf{e}^{j\angle\mathbf{h}^\mathrm{(n)}} - \eta\frac{(\mathbf{h}^{(n)} + \alpha\mathbf{e}^{j\angle\mathbf{h}^\mathrm{(n)}})^\mathrm{H}\mathrm{x}_{r}}{||\mathrm{x}_{r}||_2^2}\mathrm{x}_{r}.
    \end{aligned}
\end{equation}
The complete derivation and code listing for the ModAEDS algorithm are found in Appendices \ref{app:AEDS} and \ref{code:AEDS} respectively.

\subsection{Modified Algorithms Implementations, Advantages, Disadvantages}
\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/20MHz_ModAED_Ideal__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/100MHz_ModAED_Ideal__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/150MHz_ModAED_Ideal__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_ModAED_Ideal__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the ModAED algorithm with 250,000 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-1}
        \label{fig:modAED_ideal_30v3000}
\end{figure}
      \begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/20MHz_ModAEDS_Ideal__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/100MHz_ModAEDS_Ideal__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/150MHz_ModAEDS_Ideal__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_ModAEDS_Ideal__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the ModAEDS algorithm with 250,000 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-1}
	\label{fig:modAEDS_ideal__30v3000}
\end{figure}
 \begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
          \centering
          \resizebox{\linewidth}{!}
          {\input{Chapters/Chapter_4/Figures/20MHz_ModAED_NITF__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/100MHz_ModAED_NITF__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/150MHz_ModAED_NITF__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_ModAED_NITF__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the ModAED algorithm with 250,000 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-2}
	\label{fig:modAED_NITF_30v3000}
\end{figure}     
\begin{figure}[tb]
    \centering
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/20MHz_ModAED_narrowband__30vs3000.tikz}}
        \caption{$F_s = 20$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/100MHz_ModAED_narrowband__30vs3000.tikz}}
        \caption{$F_s = 100$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/150MHz_ModAED_narrowband__30vs3000.tikz}}
        \caption{$F_s = 150$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/200MHz_ModAED_narrowband__30vs3000.tikz}}
        \caption{$F_s = 200$ MHz}
    \end{subfigure}
    \caption{Results of the ModAED algorithm with 250,000 data samples, with SNR = 60 dB, at various sampling rates for narrowband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-1}
    \label{fig:modAED_narrowband_30v3000}
\end{figure}
\begin{figure}[tb]
    \centering
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/20MHz_ModAED_noisy__30vs3000.tikz}}
        \caption{$F_s = 20$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/100MHz_ModAED_noisy__30vs3000.tikz}}
        \caption{$F_s = 100$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/150MHz_ModAED_noisy__30vs3000.tikz}}
        \caption{$F_s = 150$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/200MHz_ModAED_noisy__30vs3000.tikz}}
        \caption{$F_s = 200$ MHz}
    \end{subfigure}
    \caption{Results of the modAED algorithm with 250,000 data samples, with SNR = 20 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-1}
    \label{fig:modAED_noisy_30v3000}
\end{figure}
\begin{figure}[tb]
    \centering
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/20MHz_ModAED_Nless__30vs3000.tikz}}
        \caption{$F_s = 20$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/100MHz_ModAED_Nless__30vs3000.tikz}}
        \caption{$F_s = 100$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/150MHz_ModAED_Nless__30vs3000.tikz}}
        \caption{$F_s = 150$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/200MHz_ModAED_Nless__30vs3000.tikz}}
        \caption{$F_s = 200$ MHz}
    \end{subfigure}
    \caption{Results of the ModAED algorithm with 2,500 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-1}
    \label{fig:modAED_Nless_30v3000}
\end{figure}
\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_ModAED_undEst_2__30vs3000.tikz}}
		\caption{$L = 100$}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_ModAED_undEst-10__30vs3000.tikz}}
		\caption{$L=190$}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_ModAED_overEst_2__30vs3000.tikz}}
		\caption{$L = 400$}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_ModAED_overEst-10__30vs3000.tikz}}
		\caption{$L = 210$}
	\end{subfigure}
	\caption{Results of the ModAED algorithm with 250,000 data samples, with SNR = 60 dB for wideband simulation at 200 MHz sampling rate for incorrect estimates of transfer function length, $L$, (a) $L/2$, (b) $L - 10$, (c) $L*2$, and (d) $L + 10$, for impulse response set IR-1}
	\label{fig:modAED_wrongModel_30v3000}
      \end{figure}
 \begin{figure}[!t]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/20MHz_ModAED_rStart__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/100MHz_ModAED_rStart__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/150MHz_ModAED_rStart__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_ModAED_rStart__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the ModAED algorithm with 250,000 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-1 and normalized random initial estimates after 100 iterations}
	\label{fig:modAED_rStart_30v3000}
      \end{figure}
      \begin{figure}[htb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/100MHz_ModAED_Nless__30vs3000.tikz}}
		\caption{ModAED}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/100MHz_ModAEDS_Nless__30vs3000.tikz}}
		\caption{ModAEDS}
	\end{subfigure}
	\caption{Results of the ModAED and ModAEDS algorithms with 2,500 data samples, with SNR = 60 dB, for wideband simulation at 100 MHz sampling rate for impulse response set IR-1}
	\label{fig:modAEDvmodAEDS_Nless_30v3000}
      \end{figure}
      \begin{figure}[htb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/150MHz_ModAED_noisy__30vs3000.tikz}}
		\caption{ModAED}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/150MHz_ModAEDS_noisy__30vs3000.tikz}}
		\caption{ModAEDS}
	\end{subfigure}
	\caption{Results of the ModAED and ModAEDS algorithms with 250,000 data samples, with SNR = 20 dB, for wideband simulation at 150 MHz sampling rate for impulse response set IR-1}
	\label{fig:modAEDvmodAEDS_noisy_30v3000}
      \end{figure}
      \begin{figure}[b]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_ModAED_undEst_2__30vs3000.tikz}}
		\caption{ModAED}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_ModAEDS_undEst_2__30vs3000.tikz}}
		\caption{ModAEDS}
	\end{subfigure}
	\caption{Results of the ModAED and ModAEDS algorithms with 250,000 data samples, with SNR = 60 dB, for wideband simulation at 100 MHz sampling rate for incorrect estimates of model order, $L = L/2$, for impulse response set IR-1}
	\label{fig:modAEDvmodAEDS_wrongModel_30v3000}
\end{figure}
As with the AED algorithm, the ModAED and ModAEDS algorithms focus on estimating the time delay between the channels, rather than estimate the channels entirely. These algorithms also estimate the largest peaks (which ideally are the direct paths) of the impulse responses, and as a result produce similar channel estimates.

Like the AED algorithm, these algorithms require (\ref{eq:ch_4_unit_vector}) as the initial start value for the channel estimates. With the initial estimate, $\mathbf{h}^{(0)} = \mathrm{e}_d$, the ModAED and ModAEDS algorithm performs fairly well in ideal conditions as can be seen in Figures \ref{fig:modAED_ideal_30v3000},  \ref{fig:modAED_NITF_30v3000}, and \ref{fig:modAEDS_ideal_30v3000}. The estimates are centered in the middle of the sample index due to the position of the $d^\text{th}$ index in the initial estimate, and the estimates are not quite as complete as the SVD estimates shown, like the AED algorithm. It should be noted that the ModAED algorithm switches the estimates for the impulse responses for set IR-2, and places them in the reflective position of the $d^{th}$ index. These figures also show that there is minimal observable differences between the ModAED and ModAEDS algorithms, with the ModAEDS being slightly more sparse in the smaller peaks.

Nonideal conditions were also tested as shown in Figures, \ref{fig:modAED_narrowband_30v3000}, \ref{fig:modAED_noisy_30v3000},  \ref{fig:modAED_Nless_30v3000}, and \ref{fig:modAED_wrongModel_30v3000}.
Figure \ref{fig:modAED_narrowband_30v3000} display the estimates for narrowband simulation. As with the LS algorithm, the narrowband suffers due to the relationship between the frequency and time domain. 
Figure \ref{fig:modAED_noisy_30v3000} reflects that more noise does not significantly impact the estimates, .
Figure \ref{fig:modAED_Nless_30v3000} shows that considerably less samples for computation do not severely alter the algorithm either.
The model order, $L$, does not need to be known for the impulse response estimates either, as shown in Figure \ref{fig:modAED_wrongModel_30v3000}.
Using a normalized random start for the initial channel estimate typically results in a random normalized result with no true single maximum peak as is shown in Figure \ref{fig:modAED_rStart_30v3000}.

There is also not much (if any) difference between the performance of the ideal, and some of the nonideal scenarios (lower SNR, less data samples, and incorrect $L$) tested, and so the individual results for the ModAEDS algorithm are not shown, although Figures \ref{fig:modAEDvmodAEDS_Nless_30v3000} , \ref{fig:modAEDvmodAEDS_noisy_30v3000} and \ref{fig:modAEDvmodAEDS_wrongModel_30v3000} are shown to compare the results for both the ModAED and ModAEDS algorithms. Unfortunately, the narrowband simulation, still performs terribly,but slightly more sparse than the results for the modAED algorithm narrowband simulation shown in Figure \ref{fig:modAED_narrowband_30v3000}. Note that this method is similar to the one explained in \cite{art:AdaptBESparse}.

One advantage that these modified algorithms is that they require less computations. The ModAED and ModAEDS algorithms require only $(N - L)(6M(L + 1) + 2)$ and $(N - L)(8M(L + 1) + 2)$ computations respectively; the ModAEDS also computes a complex exponential for each of the $N - L$ lines. This is due to the ModAED and ModAEDS algorithm not requiring a secondary normalization. 

As explained previously, Tables \ref{tab:preExist_Algs_Results_ideal}--\ref{tab:preExist_Algs_Results_randomStart} shows a numerical comparison of the estimates for both of the pre-exisiting algorithms, and for these modified algorithms introduced. A discussion of the numerical results can be found in in the next section, Section \ref{sec:AlgTesting_ch4}. 