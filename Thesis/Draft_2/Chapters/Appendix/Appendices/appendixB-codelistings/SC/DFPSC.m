% Dual Fixed-Point Single Constraint Algorithm 
%   - The algorithm with only one extra constraint
%   Inputs:
%       - hprev: column vector containing all the transfer functions 
%               estimates size: M(L + 1)x1
%       - x: Toeplitz data matrix size: (N - L)xM(L + 1)
%       - eta: stepsize for update (scalar)
%       - epsilon: the offset for the denominator to be nonzero (small)
%       - alpha: the weight for the sparsity constraint
%       - gamma: the weight for the shift-suppression constraint
%       - L: the length of a transfer function estimate
%       - N: the amount of data in the Toeplitz data matrix
%       - M: the number of channels
%       - maxBlockReset: the data block size used for processing
%       - maxIter: the maximum amount of passes through the data
%       - d: the index for the elementary vector
%   Outputs:
%       - EstChannels: vector containing new updated estimate for transfer
%       functions
function EstChannels = DFPSC(hprev, x, eta, alpha, gamma, phi, epsilon, ...
                                        L, N, M, maxBlockReset, maxIter, d)
%% Pre-Processing and Initialization
    % Determine iterations through data
    iters = N - L;
    % Set elementary vector
    ed = zeros(M*(L + 1), 1);
    ed(d) = 1;
    % Set up identity matrix
    I = eye(M*(L + 1));
    % initialize flexible variable
    z = 1;
%% Processing
    % Make multiple passes through data (if data chunks are sufficiently
    % small)
    for iter = 1:maxIter
        % Reset the block size
        block = maxBlockReset;
        % Iterate through data
        for row = 1:block:iters
            % Verify that the indices are valid
            if row > N - block
                % Set maxBlock such that the indices are not violated
                block = N - row - (L + 1);
            end
            % Get "row" of data
            xr = x(row:row + block - 1, :).';
            % Check if first iteration
            if row == 1
                % Call normal SC algorithm to start algorithm
                EstChannels = SC(hprev, x, eta, alpha, gamma, epsilon, ...
                                       L, N, M, maxBlockReset, maxIter, d);
                % Reformat output
                hprev = reshape(EstChannels, M*(L + 1), 1);
                % Determine which indice for the max peak
                [~, c] = max(hprev(L + 1:end));
                % Make corresponding elementary vector
                ec = zeros(M*(L + 1), 1);
                ec(c) = 1;
            % If not the first iteration
            else
                % Compute place holder variable for repeated variables
                inv = (I + gamma*ed*ed' + phi*ec*ec')\I;
                hest = (hprev - alpha*exp(1j*phase(hprev)) + ...
                                        phi*ec*conj(y) + gamma*ed*conj(z));
                den = zeros(block, 1); xrt = xr';
                for cols = 1:block
                    temp = inv*xr(:, cols); 
                    den(cols) = xrt(cols, :)*temp;
                end
                % Compute Lagrange Multiplier
                tilde_lambda = (hest'*inv*xr)./(den + epsilon).';
                % Compute Update
                hnew = inv*(hest - eta*(xr*tilde_lambda'));
                % Save new update for next iteration
                hprev = hnew;
            end
            % Determine new value for z
            z = hprev(d);
            y = hprev(c);
        end
    end
%% Output
   % Format output
   EstChannels = reshape(hnew, L + 1, M); 
end