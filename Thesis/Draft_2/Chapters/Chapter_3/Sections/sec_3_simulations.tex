\begin{figure}[htb]
	\centering
	\resizebox{12cm}{!}{\input{Chapters/Chapter_3/Figures/trans_rec_sim_loc_test.tex}}
	\caption{The environment used to simulate impulse responses including the location of the transmitter, and two of the potential receivers which were used for all testing. The black lines represent the ``walls", or rather boundaries, on which reflections and refractions may occur. 
	% The multipath The environment used to simulate transfer functions with the location of two receivers and the transmitter denoted by the corresponding circles
	}\label{fig:simEnv}
\end{figure}
\section{Simulations}\label{sec:sim_param}
All simulations were done in MATLAB\textsuperscript \textregistered. Realistic impulse responses were derived by applying a ray-tracing algorithm to a simulated indoor environment. An example of the simulated environment used for ray tracing can be seen in Figure \ref{fig:simEnv}. These pre-simulated impulse responses were then fed complex band limited white-noise to approximate a communication signal. 

Multiple impulse responses of different sample rates were used for simulation testing at sample rates 
\begin{multicols}{3}
	\begin{itemize}[noitemsep,nolistsep]
		\item 20 MHz,
		\item 40 MHz,
		\item 80 MHz,
		\item 100 MHz,
		\item 120 MHz,
		\item 140 MHz,
		\item 150 MHz,
		\item 160 MHz,
		\item 180 MHz,
		\item 200 MHz.
	\end{itemize}
\end{multicols} 
\noindent 
The ray tracing algorithm yields a total of 4096 path combinations; these paths were used to generate discrete impulse responses corresponding to the frequencies listed above. There are 21 variations of the 4096 paths, which used differing seed values corresponding to realistic phase shifts in reflections due to the inconsistencies of wall materials. It should also be noted that these paths are rough models for impulse responses inside buildings. The paths use hypothesized refractive indices values, and, as can be seen in Figure \ref{fig:simEnv}, there are only walls used in the model environment, no furniture, doors, windows, or people were simulated for simpler implementation. 

To generate the discrete impulse responses, an ideal impulse response was bandlimited using an ideal lowpass filter, which is approximately replacing each impulse with a sinc function. The filtered impulse response is then sample at the desired sampling rate. 

Two versions of impulse responses were simulated. The first version, referred to as set IR-1, assumes the power of the received signal is greater than the power of the transmitted signal. This might occur if the signal is amplified by some material as it is transmitted. The second set, referred to as set IR-2 assumes the opposite, and is the more likely of the two due to natural attenuation that occurs when refracted and reflected. For the two receivers shown in Figure \ref{fig:simEnv}, both impulse response sets can be seen in Appendix \ref{app:Figures}, in Figures \ref{fig:seedComp_20MHz_sim_TF-1}-- \ref{fig:seedComp_200MHz_sim_TF-1} and in Figures \ref{fig:seedComp_20MHz_sim_TF-2}-- \ref{fig:seedComp_200MHz_sim_TF-2} respectively. These two channels were used for all simulation testing. This wide spread of pre-simulated impulse responses allows for thorough testing of the algorithms.
%Two versions of the transfer functions are simulated. The first version, referred to as set, TF-1, guarantees that the largest peak is the first peak, the second, referred to as set TF-2, does not guarantee this, but for completeness is included. The algorithms will all be compared using the same simulated transfer functions per sample rate. Examples of the simulated environment can be seen in Figure \ref{fig:simEnv}. 
(The ray-tracing algorithm will not be discussed further in this document as it is not the focus of the research.) 

Simulated data is run through each of the 21 variations of the impulse responses for each sample rate. The channel estimation results for the 21 variations are averaged together to prevent anomalies. The algorithm performances are all compared using these averaged results.
\subsection{Different Simulation Parameters} \label{sec:diffSimParam}
For more thorough testing, assorted simulation parameters were varied. 
\subsubsection{Signal Bandwidth} \label{sec:narrow_v_wide}
The generated white-noise fed into the transfer functions was wideband (full bandwidth). Narrowband (approximately 5 MHz) data was also simulated to provide an accurate range of data. To simulate narrowband data, the generated white-noise was filtered to bandlimited and then decimated. 

The data is filtered using a $10^{th}$ order Chebyshev Type I filter with a peak-to-peak passband ripple of $0.5$ dB, and cutoff frequency, 
\begin{equation}
	f_c = 2\frac{5\cdot 10^6}{F_s},
\end{equation}
via  the \textit{cheby1} command in MATLAB. Then the data is filtered normally with the impulse response before being decimated down to $5$ MHz. See Figure \ref{fig:freq_comparison} for visual comparison of the signal bandwidth at 20 MHz sampling rate.
\begin{figure}[tb]
    \centering
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}{\input{Chapters/Chapter_3/Figures/20MHz_W.tikz}}
        \caption{Full bandwidth, $\approx$ 20 MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}{\input{Chapters/Chapter_3/Figures/20MHz_N.tikz}}
        \caption{Narrow bandwidth, $\approx$ 5 MHz}
    \end{subfigure}
    \caption{Power spectral density comparison for (a) wideband, versus (b) narrowband data at 20 MHz.}
    \label{fig:freq_comparison}
  \end{figure}

 It should also be noted that the wideband bandwidth corresponds to the sampling rate, i.e. a signal filtered with an impulse response which has a $20$ MHz sampling rate is assumed to have a bandwidth of 20 MHz. 
\subsubsection{Noise}
In ideal environments, no noise will interfere with the signal, however ideal environments are not realistic, especially with multipath. Additive noise is common. Thus for simulation, two different noise amounts were tested. 
\begin{table}[H]
	\begin{center}
		\begin{tabular}{||c||}
			\hline
			SNR (dB)  \\ [0.5ex]
			\hline\hline
			60\\
			\hline
			 20\\
			\hline
		\end{tabular}
	\end{center}
	\caption{Noise amount with corresponding SNR in dB.}
	\label{tab:Noise}
\end{table}

The signal to noise ratio (SNR) described here is calculated using the variance for the power comparison
\begin{equation}
	SNR = 20\cdot \log_{10}(\frac{\sigma_s}{\sigma_n}),
\end{equation}
where $\sigma_s^2$ is the variance of the signal, and $\sigma_n^2$ is the variance of the noise. 
%\begin{itemize}
%	% Change to sigma values
%	\item Low noise (SNR = 60 dB),
%	% Change to sigma values
%	\item High noise (SNR = 20 dB). 
%\end{itemize}
\subsubsection{Sample Size}
All the algorithms, except for one (which had memory issues due to processing) were tested using data sets of two sizes: 250,000 samples, and 2,500 samples. The algorithm which has memory issues was tested with 30,000 and 300 samples instead. The smaller amount is ${\frac{1}{100}}^{th}$ the size of the larger amount. Theoretically, all the algorithms should perform better with more data, although it should still execute acceptably with less samples. 

% Should I put in a note about how if too little data is used, multiple passes through the data needs to be done for proper estimates?
\subsubsection{Model Order Estimates}
As explained in the literature, many algorithms and techniques depend heavily on knowing/having the correct model order \cite{art:BSI,art:MultiChannelBID,art:LS_Approach_BCID}. The model orders in this thesis correspond to the sampling rate of each impulse response. For example, an impulse response with a $20$ MHz sampling rate has model order $L=20$, etc. The algorithms explored in this thesis also experiment with different model orders in simulation to determine if model order is a breaking point for the algorithms. 

Four model orders, $L$ were tested, with some under-model order and some over-model order estimates
\begin{table}[H]
	\begin{center}
		\begin{tabular}{||c c c c c||}
			\hline
			Model Order & Variant 1 & Variant 2 & Variant 3 & Variant 4 \\ [0.5ex]
			\hline\hline
			$L$ & $L/2$ & $L - 10$ & $2L$ & $L + 10$\\
			\hline
		\end{tabular}
	\end{center}
	\caption{Various model order estimates used for algorithm simulation}
	\label{tab:Model Order}
      \end{table}
\subsubsection{The Ideal Simulation}
For this research, a makeshift ideal case is used, comprising of a combination of the ``ideal'' simulation parameters explained above. The ``Ideal Case'' includes
\begin{itemize}
  \item wideband,
  \item $SNR = 60$ dB,
  \item large data size,
  \item correct channel order, $L$.
\end{itemize}
\noindent This ``Ideal Case'' includes favorable conditions in which the channels should be easier to identify, and thus the delay more correct.   

\subsection{Coding Modifications}
Some alternative programming modifications have been implemented that can make the proposed algorithms faster and/or more accurate. Most, if not all the algorithms derived in this thesis are iterative update algorithms. These adaptive algorithms typically process a single row of data per update. Instead of updating the impulse response estimates by an individual row, the programs are modified such that blocks of multiple rows of data are used per update. This modification takes and computes multiple row calculations simultaneously. The resulting step (update) is averaged from these calculations, as explained in Appendix \ref{code:mods}. 

Another modification is upsampling the received data to compensate for the limited sample rate. Upsampling provides a fractional bin delay estimate instead of a whole bin delay estimate. This allows for more accurate delay estimate. The estimated transfer functions do need to be appropriately modified to compensate for this upsampling as well if implemented.

\subsection{Results Explanation} \label{sec:ch3_sim_results}
Simulation results are shown in two ways in this thesis. The first way is by plot, and the second way is by table. Only results for sampling rates $F_s=20, 100, 150,$ and $200$ MHz are shown in the table and the plots for channels 30 and 3000 in each chapter. Each chapter also only shows the results for the algorithms discussed in the chapter. A complete table of all sampling rates and all of the algorithms are shown in Appendix \ref{app:results}, but more plots are not shown, as they are deemed unnecessary.


The plots are similar to  what is  shown in Figure \ref{fig:peak_comparison}. Two plots are shown side-by-side, shwoing both what the channel estimates should look like, and what the estimates do look like after processing. This shows how well the channel estimate methods work. Each plot shows the estimates for the sample rate version seed 0 (the first of the 21 variants). 

The table show results averaged over the 21 variations of the impulse responses. The simulation tables contain 9 columns. These tables show information about
\begin{itemize}
\item simulation state,
\item sample rate,
\item algorithm,
\item actual distance between receivers
\item averaged estimated distance between receivers
\item minimum estimated  distance between receivers
\item median estimated distance between receivers, 
\item maximum estimated distance between receivers, and  
\item averaged error between estimated distance and the actual distance. 
\end{itemize}

The \textit{Simulation State} column is used to describe the parameter situation and the impulse response set used during simulation. Possible parameter situations include different signal bandwidths, noise rates, sample sizes, and model orders, as explained in more detail above in Section \ref{sec:diffSimParam}.

The \textit{Sample Rate} and \textit{Algorithm} columns are fairly straight forward. The second column details the impulse response sample rate used for simulation. The third column details the algorithm for which results reside in that row of the table. 

The fourth column, $d_{1, 2}$, represents the true distance between receivers in meters. It is the same for all of the tables, as the distance between receivers does not change. The distance between receivers is calculated using precalculated distance estimates. These precalculated distance estimates were computed during generation of the impulse responses (via the ray-tracing algorithm). They compute the distance from transmitter to the receivers. The relative distance between receivers is then calculated using (\ref{eq:distCalc}).

Column five is used to show the averaged estimated distance between receivers,  $\hat{d}_{{avg}_{1, 2}}$, in meters. The distances are calculated using (\ref{eq:tau}) and (\ref{eq:dist}). They are then averaged together over the 21 variants as shown in (\ref{eq:distAvg}).
\begin{equation} \label{eq:distAvg}
  \begin{aligned}
    \tilde{d}_{{avg}_{1, 2}} &= \sum_{v = 0}^{20} \hat{d}_{1,2}^{(v)} \\
    \hat{d}_{{avg}_{1, 2}} & = \frac{\tilde{d}_{{avg}_{1, 2}}}{21},
    \end{aligned}
 \end{equation}
 where $^{(v)}$ denotes which variant of the 21 impulse responses was used.

 The next three columns show the range of the estimates giving the minimum, median and maximum delay estimates in meters. These are shown in the table using  $\hat{d}_{{min}_{1,2}}$, $\hat{d}_{{med}_{1,2}}$, and $\hat{d}_{{max}_{1, 2}}$ respectively. These values are calculated using the built in functions in MATLAB.

 The last column shows the error in meters  between the true distance and the estimated distance averaged. This is calculated using the typical error calculations averaged together
 \begin{equation}
   \begin{aligned}
   \tilde{epsilon}_{{avg}_{1, 2}} &= \sum_{v = 0}^{20}d_{1, 2} - \hat{d}_{1,2}^{(v)} \\
   \epsilon_{{avg}_{1, 2}}& = \frac{\tilde{epsilon}_{{avg}_{1, 2}}}{21}.
   \end{aligned}
 \end{equation}

 