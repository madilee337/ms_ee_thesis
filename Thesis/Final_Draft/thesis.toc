\contentsline {chapter}{\textnormal {ABSTRACT}}{iii}{Doc-Start}
\contentsline {chapter}{\textnormal {PUBLIC ABSTRACT}}{v}{Doc-Start}
\contentsline {chapter}{\textnormal {\textnormal {ACKNOWLEDGMENTS}}}{vi}{chapter*.1}
\contentsline {chapter}{\textnormal {LIST OF TABLES}}{x}{chapter*.3}
\contentsline {chapter}{\textnormal {LIST OF FIGURES}}{xiii}{chapter*.4}
\contentsline {chapter}{\textnormal {\textnormal {ACRONYMS}}}{xxi}{chapter*.5}
\contentsline {chapter}{\numberline {1}INTRODUCTION}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Thesis Outline}{2}{section.1.1}
\contentsline {chapter}{\numberline {2}LITERATURE REVIEW}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Source Localization}{3}{section.2.1}
\contentsline {subsubsection}{TOA, RSS, DOA, FDOA}{3}{section*.6}
\contentsline {subsubsection}{TDOA}{5}{section*.7}
\contentsline {section}{\numberline {2.2}Time Delay Estimation and Blind Channel Identification Algorithms}{6}{section.2.2}
\contentsline {section}{\numberline {2.3}Cross-Relation Method}{8}{section.2.3}
\contentsline {chapter}{\numberline {3}PROBLEM OVERVIEW}{9}{chapter.3}
\contentsline {section}{\numberline {3.1}Objective}{9}{section.3.1}
\contentsline {section}{\numberline {3.2}The Multipath Problem}{10}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}TDOA Calculations}{11}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Time Delay Estimation Methods}{14}{subsection.3.2.2}
\contentsline {subsubsection}{Generalized Cross-Correlation TDOA Approach}{15}{section*.12}
\contentsline {subsubsection}{Other Algorithms}{18}{section*.13}
\contentsline {section}{\numberline {3.3}Simulations}{20}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Different Simulation Parameters}{23}{subsection.3.3.1}
\contentsline {subsubsection}{Signal Bandwidth}{23}{section*.17}
\contentsline {subsubsection}{Noise}{23}{section*.19}
\contentsline {subsubsection}{Sample Size}{24}{section*.21}
\contentsline {subsubsection}{Model Order Estimates}{25}{section*.22}
\contentsline {subsubsection}{The Ideal Simulation}{25}{section*.24}
\contentsline {subsection}{\numberline {3.3.2}Coding Modifications}{26}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Results Explanation}{26}{subsection.3.3.3}
\contentsline {section}{\numberline {3.4}Data Collection}{29}{section.3.4}
\contentsline {section}{\numberline {3.5}Data Sets}{30}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}HT Data Sets}{31}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}Gaussian White-Noise Data Sets}{31}{subsection.3.5.2}
\contentsline {chapter}{\numberline {4}PRE-EXISTING CROSS RELATION METHODS}{34}{chapter.4}
\contentsline {section}{\numberline {4.1}Pre-Existing Algorithms}{34}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}System Model}{34}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Cross Relation}{35}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}A Least-Squares Approach According to Xu et al.}{36}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}Adaptive Eigenvalue Decomposition}{37}{subsection.4.1.4}
\contentsline {subsection}{\numberline {4.1.5}Pre-Existing Algorithms Implementations, Advantages, and Disadvantages}{38}{subsection.4.1.5}
\contentsline {section}{\numberline {4.2}Modified Algorithms}{45}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Modified System Model}{46}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Complex Cross Relation}{49}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Modified Adaptive Eigenvalue Decomposition}{52}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Modified AED with Sparsity}{53}{subsection.4.2.4}
\contentsline {subsection}{\numberline {4.2.5}Modified Algorithms Implementations, Advantages, Disadvantages}{55}{subsection.4.2.5}
\contentsline {section}{\numberline {4.3}Algorithm Testing and Results}{59}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Simulation}{60}{subsection.4.3.1}
\contentsline {chapter}{\numberline {5}THE ADAPTIVE CROSS-CHANNEL WITH SPARSE SHIFT-SUPPRESSION (AXIS) METHOD}{74}{chapter.5}
\contentsline {section}{\numberline {5.1}AXIS Derivation}{74}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Algorithm Advantages, Implementations, and Modifications}{75}{subsection.5.1.1}
\contentsline {section}{\numberline {5.2}Variations}{79}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Varied AXIS}{80}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Dual Fixed-Peak AXIS}{83}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Non-Sparse AXIS}{86}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}Single Constraint AXIS (SC)}{88}{subsection.5.2.4}
\contentsline {subsubsection}{Variations of the SC Algorithm}{90}{section*.73}
\contentsline {paragraph}{\textbf {Varied SC (VSC)}}{91}{section*.74}
\contentsline {paragraph}{\textbf {Dual Fixed-Peak SC (DFPSC)}}{91}{figure.caption.77}
\contentsline {paragraph}{\textbf {Non-Sparse SC (NSSC)}}{93}{section*.79}
\contentsline {section}{\numberline {5.3}Data Verification}{95}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Simulation}{95}{subsection.5.3.1}
\contentsline {subsubsection}{``Ideal'' Simulation}{95}{section*.80}
\contentsline {subsubsection}{Signal Bandwidth}{95}{section*.81}
\contentsline {subsubsection}{Noise}{96}{section*.82}
\contentsline {subsubsection}{Sample Size}{96}{section*.83}
\contentsline {subsubsection}{Model Order}{96}{section*.84}
\contentsline {subsubsection}{Initial Estimates}{97}{section*.85}
\contentsline {section}{\numberline {5.4}Algorithm Comparison}{116}{section.5.4}
\contentsline {chapter}{\numberline {6}THE ``BRUTE FORCE" METHOD}{117}{chapter.6}
\contentsline {section}{\numberline {6.1}Brute Force Derivation}{117}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Algorithm Advantages, Disadvantages and Implementation}{118}{subsection.6.1.1}
\contentsline {section}{\numberline {6.2}Variations}{119}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Adaptive Brute Force}{120}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}Varied Adaptive Brute Force}{124}{subsection.6.2.2}
\contentsline {subsection}{\numberline {6.2.3}Dual Fixed-Peak Brute Force}{126}{subsection.6.2.3}
\contentsline {section}{\numberline {6.3}Data Verification}{134}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Simulation}{134}{subsection.6.3.1}
\contentsline {subsubsection}{``Ideal'' Simulation}{135}{section*.122}
\contentsline {subsubsection}{Signal Bandwidth}{136}{section*.123}
\contentsline {subsubsection}{Noise}{137}{section*.124}
\contentsline {subsubsection}{Sample Size}{137}{section*.125}
\contentsline {subsubsection}{Model Order}{137}{section*.126}
\contentsline {subsubsection}{Initial Estimates}{138}{section*.127}
\contentsline {section}{\numberline {6.4}Algorithm Comparison}{148}{section.6.4}
\contentsline {chapter}{\numberline {7}RESULTS}{149}{chapter.7}
\contentsline {section}{\numberline {7.1}Comparison of Algorithms}{149}{section.7.1}
\contentsline {subsection}{\numberline {7.1.1}Algorithm Complexity}{149}{subsection.7.1.1}
\contentsline {subsection}{\numberline {7.1.2}Simulation Results}{151}{subsection.7.1.2}
\contentsline {subsubsection}{MATLAB Conditions}{151}{section*.139}
\contentsline {subsubsection}{Results}{152}{section*.140}
\contentsline {paragraph}{\textbf {\textit {Effects of Various Parameters:}}}{152}{section*.141}
\contentsline {subparagraph}{\relax $\@@underline {\hbox {Signal Bandwidth and Sample Rate:}}\mathsurround \z@ $\relax }{152}{section*.142}
\contentsline {subparagraph}{\relax $\@@underline {\hbox {Noise:}}\mathsurround \z@ $\relax }{153}{section*.143}
\contentsline {subparagraph}{\relax $\@@underline {\hbox {Sample Size:}}\mathsurround \z@ $\relax }{153}{section*.144}
\contentsline {subparagraph}{\relax $\@@underline {\hbox {Channel Order:}}\mathsurround \z@ $\relax }{153}{section*.145}
\contentsline {subparagraph}{\relax $\@@underline {\hbox {Initial Channel Estimates:}}\mathsurround \z@ $\relax }{154}{section*.146}
\contentsline {subsection}{\numberline {7.1.3}Results with Real Observed Data}{154}{subsection.7.1.3}
\contentsline {subsubsection}{Testing Conditions}{155}{section*.147}
\contentsline {subsubsection}{Results}{156}{section*.150}
\contentsline {section}{\numberline {7.2}Final Observations}{157}{section.7.2}
\contentsline {chapter}{\numberline {8}CONCLUSION}{159}{chapter.8}
\contentsline {section}{\numberline {8.1}Research}{159}{section.8.1}
\contentsline {section}{\numberline {8.2}Future Work}{160}{section.8.2}
\contentsline {chapter}{REFERENCES}{162}{section.8.3}
\contentsline {chapter}{\textnormal {APPENDICES}}{167}{chapter.9}
\contentsline {section}{A\hspace {12pt} Math Derivations and Notation}{168}{appendix.A}
\contentsline {subsection}{\numberline {A.1}Notation}{168}{subsection.A.0.1}
\contentsline {subsubsection}{Multiple Receiver Format}{171}{section*.154}
\contentsline {subsection}{\numberline {A.2}Derivations}{172}{subsection.A.0.2}
\contentsline {subsubsection}{Modified Adaptive Eigenvalue Decomposition}{173}{section*.155}
\contentsline {subsubsection}{Modified Adaptive Eigenvalue Decomposition with Sparsity}{174}{section*.156}
\contentsline {subsubsection}{Adaptive Cross-Channel Identification with Sparse Shift-Suppression}{175}{section*.157}
\contentsline {subsubsection}{Varied AXIS}{177}{section*.158}
\contentsline {subsubsection}{Dual-Fixed Peak AXIS}{179}{section*.159}
\contentsline {subsubsection}{Single Constraint}{182}{section*.160}
\contentsline {subsubsection}{Varied Single Constraint}{184}{section*.161}
\contentsline {subsubsection}{Dual Fixed-Peak Single Constraint}{186}{section*.162}
\contentsline {subsubsection}{Brute Force}{188}{section*.163}
\contentsline {subsubsection}{Varied Brute Force}{189}{section*.164}
\contentsline {subsubsection}{Dual Fixed-Peak Brute Force}{190}{section*.165}
\contentsline {section}{B\hspace {12pt} Code Listings}{192}{appendix.B}
\contentsline {subsection}{\numberline {B.1}Modifications}{192}{subsection.B.0.1}
\contentsline {subsection}{\numberline {B.2}The Least Squares (SVD) Approach According to Xu et al.}{195}{subsection.B.0.2}
\contentsline {subsection}{\numberline {B.3}The Adaptive Eigenvalue Decomposition}{196}{subsection.B.0.3}
\contentsline {subsection}{\numberline {B.4}The Modified Adaptive Eigenvalue Decomposition}{198}{subsection.B.0.4}
\contentsline {subsection}{\numberline {B.5}The Modified Adaptive Eigenvalue Decomposition with Sparsity}{200}{subsection.B.0.5}
\contentsline {subsection}{\numberline {B.6}The Adaptive Cross-Channel Identification with Sparse Shift-Suppression}{202}{subsection.B.0.6}
\contentsline {subsection}{\numberline {B.7}The Varied Adaptive Cross-Channel Identification with Sparse Shift-Suppression}{204}{subsection.B.0.7}
\contentsline {subsection}{\numberline {B.8}The Dual Fixed-Peak Adaptive Cross-Channel Identification with Sparse Shift-Suppression}{207}{subsection.B.0.8}
\contentsline {subsection}{\numberline {B.9}The Single Constraint Method}{210}{subsection.B.0.9}
\contentsline {subsection}{\numberline {B.10}The Varied Single Constraint Method}{212}{subsection.B.0.10}
\contentsline {subsection}{\numberline {B.11}The Dual Fixed-Peak Single Constraint Method}{215}{subsection.B.0.11}
\contentsline {subsection}{\numberline {B.12}The Brute Force Method}{218}{subsection.B.0.12}
\contentsline {subsection}{\numberline {B.13}The Varied Brute Force Method}{220}{subsection.B.0.13}
\contentsline {subsection}{\numberline {B.14}The Dual Fixed-Peak Brute Force Method}{222}{subsection.B.0.14}
\contentsline {subsection}{\numberline {B.15}The Cross Correlation Function}{225}{subsection.B.0.15}
\contentsline {subsection}{\numberline {B.16}The Overhead File to Call the Algorithms}{226}{subsection.B.0.16}
\contentsline {section}{C\hspace {12pt} Oversized Figures}{237}{appendix.C}
\contentsline {section}{D\hspace {12pt} Further Results}{260}{appendix.D}
\contentsline {subsection}{\numberline {D.1}Simulation}{260}{subsection.D.0.1}
