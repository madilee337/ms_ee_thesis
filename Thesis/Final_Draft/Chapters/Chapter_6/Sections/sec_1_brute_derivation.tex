\section{Brute Force Derivation}
The derivation of this algorithm, referred to as the ``Brute Force" (BF) method, also starts by setting up the Lagrangian,
\begin{equation} \label{eq:brute-lagrangian}
    \mathcal{L} = {\mathbf{h}^{(n + 1)}}^\mathrm{H}\mathbf{X}^\mathrm{H}\mathbf{X}{\mathbf{h}^{(n + 1)}} + \alpha||{\mathbf{h}^{(n + 1)}}^\mathrm{*}||_1 + \lambda({\mathbf{h}^{(n + 1)}}^\mathrm{H}\mathrm{e}_d - 1).
\end{equation}
The update that minimizes the Lagrangian is obtained by taking the gradient with respect to the conjugate of the updated impulse response 
\begin{equation} \label{eq:brute-derivative-lagrangian}
    \frac{d}{d{\mathbf{h}^{(n + 1)}}^*}\mathcal{L} = \mathbf{X}^\mathrm{H}\mathbf{X}\mathbf{h}^{(n +1)} + \alpha\mathbf{e}^{j\angle{\mathbf{h}}^{(n + 1)}} + \lambda\mathrm{e}_d.
\end{equation}
With the assumptions made in Chapter \ref{ch:3}, upon setting (\ref{eq:brute-derivative-lagrangian}) to 0, and solving for $\mathbf{h}^{(n + 1)}$ yields
\begin{equation} \label{eq:brute-update-lambda}
    \mathbf{h}^{(n + 1)} = -(\mathbf{X}^\mathrm{H}\mathbf{X})^{-1}
    (\alpha\mathbf{e}^{j\angle{\mathbf{h}}^{(n)}} + \lambda\mathrm{e}_d),
\end{equation}
as an update equation. 

Since there is only one constraint, and thus one Lagrange multiplier, a system of equations is not required. Replacing $\mathbf{h}^{(n + 1)}$  in the constraint with the update derived in (\ref{eq:brute-update-lambda}) produces this definition for $\tilde{\lambda}$
\begin{equation} \label{eq:brute-tilde-lambda}
    \tilde{\lambda} = 
    \frac{-1 - [(\mathbf{X}^\mathrm{H}\mathbf{X})^{-1}(\alpha\mathbf{e}^{j\angle{\mathbf{h}}^{(n)}})]^\mathrm{H}\mathrm{e}_d}
    {\mathrm{e}_d^\mathrm{H}((\mathbf{X}^\mathrm{H}\mathbf{X})^{-1})^\mathrm{H}\mathrm{e}_d}.
\end{equation}

Exchanging $\lambda$ in Equation (\ref{eq:brute-update-lambda}) with the conjugate of the definition of Equation (\ref{eq:brute-tilde-lambda}) provides the complete update equation used in this algorithm,
\begin{equation} \label{eq:brute-update-complete}
    \mathbf{h}^{(n + 1)} = -(\mathbf{X}^\mathrm{H}\mathbf{X})^{-1}
    \Big(\alpha\mathbf{e}^{j\angle{\mathbf{h}}^{(n)}} + 
    \frac{-1 - [(\mathbf{X}^\mathrm{H}\mathbf{X})^{-1}(\alpha\mathbf{e}^{j\angle{\mathbf{h}}^{(n)}})]^\mathrm{H}\mathrm{e}_d}
    {\mathrm{e}_d^\mathrm{H}((\mathbf{X}^\mathrm{H}\mathbf{X})^{-1})^\mathrm{H}\mathrm{e}_d}\mathrm{e}_d\Big).
\end{equation}

The full derivation and code are located in Appendices \ref{app:BF}, and \ref{code:BF} respectively.
\subsection{Algorithm Advantages, Disadvantages and Implementation}
The main advantage of this algorithm is only a single step is required to achieve the impulse response estimate, like the SVD algorithm.  The major disadvantage to this algorithm is the matrix operations required. Both a matrix inverse, which is computationally expensive, $\mathcal{O}((M(L +1))^3)$, and matrix multiplication, which can also be computationally expensive, $\mathcal{O}((M(L +1))^2(N - L))$.

To implement this algorithm, the initial value of $h^{(n)} = \mathrm{e}_d$, just as for the ModAED and AXIS based algorithm. Although starting with a better initial estimate may provide better results, as there is only one step.

Most of the channel estimates resulting from the non-adaptive BF algorithm, as shown in Figures \ref{fig:NBF_ideal_30v3000} and \ref{fig:NBF_NITF_30v3000} do not appear to be very good estimates. The 20 MHz channel estimates are actually quite good, for both impulse response sets, however the others are noisy, and seemingly random. As the majority of the results for the ``Ideal'' situation seemed random, the results for the other simulation situations are not shown, as there is not much to glean from them.

\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/20MHz_NBF_Ideal__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/100MHz_NBF_Ideal__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/150MHz_NBF_Ideal__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_NBF_Ideal__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the non-adaptive BF algorithm with 250,000 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-1, seed 0.}
	\label{fig:NBF_ideal_30v3000}
      \end{figure}
      
\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/20MHz_NBF_NITF__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/100MHz_NBF_NITF__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/150MHz_NBF_NITF__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_NBF_NITF__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the non-adaptive BF algorithm with 250,000 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-2, seed 0.}
	\label{fig:NBF_NITF_30v3000}
\end{figure}

