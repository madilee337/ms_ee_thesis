\section{Pre-Existing Algorithms}
The least squares (LS) BCI algorithm put forth by Xu et al. in \cite{art:LS_Approach_BCID,art:Det_BID_Multi-FIRSystems} uses the cross-relation (CR), and the assumption that the transfer functions are in the nullspace of the data matrix, as shown in the following sections. Xu's algorithm is labeled the SVD algorithm, as all the algorithms utilize a least-squares approach, and Xu's algorithm uses the Singular Value Decomposition (SVD) for channel identification. The Adaptive Eigenvalue Decomposition (AED) as referred to in \cite{art:AEDDecomp,book:AcousticMIMOSigProc,conf:AED} is also a least-squares approach using the same nullspace assumptions. 
\subsection{System Model}
Consider a source at some unknown location transmitting an unknown signal, $s(n)$, in a multipath environment. The transmission is detected by multiple receivers. The system is modeled as
\begin{equation}
\label{eq:system}
\begin{cases}
x_{1}(n)=s(n)*h_{1}(n)+w_{1}(n) \\ 
\ \vdots \\ 
x_{M}(n)=s(n)*h_{M}(n)+w_{M}(n), 
\end{cases}
\end{equation}
where $*$ denotes convolution, $h_{i}(n)$ denotes the individual channel mapping from the source to the $i^\mathrm{th}$ receiver, $x_{i}(n)$ the value received from the transmitted signal at the $i^{th}$ receiver, and $\mathrm{w}(n) = [w_{1}(n)\hspace{0.1cm}w_{2}(n)...w_{M}(n)]$ is additive white noise, and $n$ denotes the $n^\mathrm{th}$ sample. $M$ denotes the number of receivers. This model assumes a finite impulse response, and is a single input, multiple output (SIMO) system, as is shown in Figure \ref{fig:transmission_figure}.

\begin{figure}[htb]
	\centering
	\resizebox{0.35\linewidth}{!}
	{\input{Chapters/Chapter_4/Figures/signalModel_0.tikz}}
        \caption{System Model}
	\label{fig:transmission_figure}
\end{figure}

\subsection{Cross Relation} \label{sec:ch_4_LS_conj_cr}
Neglecting noise, the signal model, (\ref{eq:system}), can be expressed individually as $x_i(n) = s(n)*h_i(n)$. Convolving $x_i(n)$ with $h_j(n)$, where $i \neq j$ produces a CR,
\begin{align} \notag
  h_j(n)*x_i(n) &= h_j(n)*[s(n)*h_i(n)] \\
                &= h_i(n)*[s(n)*h_j(n)] \\
  h_j(n)*x_i(n) &= h_i(n)*x_j(n)
\end{align}
\begin{equation} \label{eq:cr-derivation}
  \boxed{h_j(n)*x_i(n) - h_i(n)*x_j(n) = 0.}
\end{equation}

The CR is the key to the BCI algorithms. Equation (\ref{eq:cr-derivation}) can be rewritten for two receiver measurements as the matrix equation\footnote{For multiple receivers ($M>2$), (\ref{eq:cr-matrix}) can be modified as seen in Appendix \ref{app:notation}.}
\begin{equation} \label{eq:cr-matrix}
\begin{bmatrix} \mathrm{X}_i(L) & \vdots & -\mathrm{X}_j(L)
\end{bmatrix}
\begin{bmatrix} \mathrm{h}_j \\ \mathrm{h}_i \end{bmatrix} = \mathbf{0},
\end{equation}
where $\mathrm{X}_m(L)$ is a Toeplitz matrix of the form
\begin{equation} \notag
\mathrm{X}_m(L) = \begin{bmatrix}
x_m(L) & x_m(L - 1) & \cdots & x_m(0) \\
x_m(L + 1) & x_m(L) & \cdots & x_m(1) \\
\vdots & \ddots & \ddots & \vdots \\
x_m(N) & x_m(N - 1) & \cdots & x(N - L - 1)
\end{bmatrix},
\end{equation}
and $\mathbf{h}_m$ is the impulse response vector 
\begin{equation} \notag
\mathrm{h}_m \triangleq [h_m(0), \cdots, h_m(L)]^{\mathrm{T}}.
\end{equation}
For brevity, (\ref{eq:cr-matrix}) can be rewritten as
\begin{equation} \label{eq:cr-brief}
\mathbf{X}\mathbf{h} = \mathbf{0}.
\end{equation}
The size of $\mathbf{X}$ is $(N - (L + 1))\times M(L + 1)$, and $\mathbf{h}$ is of size $M(L + 1)\times 1$, where the amount of channels, $M=2$, $L$ is the channel order, and $N$ is the data size for all of the calculations in this research.
Equation (\ref{eq:cr-brief}) is the basis for the approach derived by Xu et al. \cite{art:LS_Approach_BCID}, referred to as the SVD algorithm, and the AED algorithm. Equation (\ref{eq:cr-brief}) implies that the $\mathbf{h}$ is in the nullspace of the data matrix $\mathbf{X}$ (and vice versa). 
\subsection{A Least-Squares Approach According to Xu et al.}
When there is noise, (\ref{eq:cr-brief}) is not exact. Indeed
\begin{equation} \label{eq:cr-brief_approx}
\mathbf{X}\mathbf{h} \approx \mathbf{0}.
\end{equation}
The approach of Xu frames the problem as, 
\begin{equation} \label{eq:min_cr_brief}
  \begin{aligned}
    &\underset{\mathbf{h}}{\mathrm{minimize}}&  &||\mathbf{X}\mathbf{h}||_2^2& \\
    &\text{subject to}&  &||\mathbf{h}||_2^2 = 1,&
  \end{aligned}
\end{equation}\noindent where $\mathbf{h}$ is an approximation of the true impulse responses.


The solution is found by taking the singular value decomposition (SVD) of the matrix $\mathbf{X}$. As the data matrix $\mathbf{X}$ does not have actually have a nullspace due to the noise, an approximate nullspace is derived by taking the right singular vector of $\mathbf{X}$, which corresponds to the smallest singular value of $\mathbf{X}$. The constraint ensures that a nonzero filter vector is selected.

This algorithm is referred to as the SVD algorithm. The code for the SVD algorithm can be seen in the code listing in Appendix \ref{code:LS}. 

\subsection{Adaptive Eigenvalue Decomposition}
The Adaptive Eigenvalue Decomposition (AED) \cite{art:AEDDecomp,book:AcousticMIMOSigProc,conf:AED}, as the name implies, takes an adaptive approach, which minimizes the difference between update estimates. The algorithm attempts to solve this minimization equation for $\mathbf{h}^{(n)}$
\begin{equation} \label{eq:AED_objective}
\begin{aligned}
& \min_{\mathbf{h}^{(n)}}&  & {\mathbf{h}^{(n)}}^\mathrm{T}\mathbf{R}_{1,2}{\mathbf{h}^{(n)}}& \\
& \text{subject to}&  &{\mathbf{h}^{(n)}}^\mathrm{T}\mathbf{h}^{(n)} = 1,&
\end{aligned}
\end{equation}
where $\mathbf{h}^{(n)}$ is the current iteration of the impulse response estimate, and  $\mathbf{R}_{1,2}$ is the covariance matrix between channels 1 and 2. The covariance matrix is computed by taking the expectation of the $L_2$-norm of the data matrix $\mathbf{X}$, which is computed as
\begin{equation}
  \begin{bmatrix}
	\mathbf{R}_{{\mathrm{x}_1},{\mathrm{x}_1}} & \mathbf{R}_{{\mathrm{x}_1},{\mathrm{x}_2}} \\
	\mathbf{R}_{{\mathrm{x}_2},{\mathrm{x}_1}} & \mathbf{R}_{{\mathrm{x}_2},{\mathrm{x}_2}}
      \end{bmatrix},
    \end{equation}
 where $\mathbf{R}_{{\mathrm{x}_i},{\mathrm{x}_j}} = E[{\mathrm{x}_i\mathrm{x}_j^\mathrm{T}}]$. Thus (\ref{eq:AED_objective}) can equivalently be written as
\begin{equation} \label{eq:AED_objective_2}
  \begin{aligned}
    &  \min_{\mathbf{h}^{(n)}} & & E[||\mathbf{X}\mathbf{h}||_2^2]& \\
    &\text{subject to}&  &||\mathbf{h}||_2^2 = 1,&
  \end{aligned}
\end{equation}
assuming that the data is not complex. Equation (\ref{eq:AED_objective_2}) is very similar to the objective function used by the SVD approach shown in (\ref{eq:min_cr_brief}), showing a logical connection between these two algorithms. The large difference is just in how they are calculated.

To compute the solution for the AED algorithm using gradient descent, the adaptive constrained LMS algorithm, found in \cite{art:LMS}, is used for the update equation. The update equation is
\begin{equation} \label{eq:AED_update}
	\begin{split}
          \tilde{\mathbf{h}}^{(n + 1)} &= \mathbf{h}^{(n)}  - \eta\frac{{\mathbf{h}^{(n)}}^{H}\mathrm{x_r}}
		{||\mathrm{h}^{(n)}||_2^2}\mathrm{x}_r \\
		\mathbf{h}^{(n + 1)}&= \frac{\tilde{\mathbf{h}}^{(n + 1)}}{||\tilde{\mathbf{h}}^{(n + 1)}||_2^2},	
	\end{split}
\end{equation}
a normalized update where $\mathrm{x}_{r}$ is a row of the data matrix $\mathbf{X}$,  transposed as all vectors are assumed to be column formatted. Note that \cite{art:AEDDecomp,book:AcousticMIMOSigProc,conf:AED} assume that the signal is acoustic, and thus not adjusted for the complex RF signals, (\ref{eq:AED_update}) makes the necessary adjustments. 

The code for the AED algorithm can be seem in the code listing in Appendix \ref{code:AED}.
\subsection{Pre-Existing Algorithms Implementations, Advantages, and Disadvantages}
\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/20MHz_LS_Ideal__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/100MHz_LS_Ideal__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/150MHz_LS_Ideal__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_LS_Ideal__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the SVD algorithm with 30,000 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-1, seed 0.}
	\label{fig:LS_ideal_30v3000}
\end{figure}
\begin{figure}[!b]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/20MHz_LS_Nless__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/100MHz_LS_Nless__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/150MHz_LS_Nless__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_LS_Nless__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the SVD algorithm with 300 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-1, seed 0.}
	\label{fig:LS_Nless_30v3000}
      \end{figure}
      \begin{figure}[htb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/20MHz_LS_NITF__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/100MHz_LS_NITF__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/150MHz_LS_NITF__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_LS_NITF__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the SVD algorithm with 30,000 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-2, seed 0.}
	\label{fig:LS_NITF_30v3000}
\end{figure}
\begin{figure}[htb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/20MHz_LS_narrowband__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/100MHz_LS_narrowband__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/150MHz_LS_narrowband__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_LS_narrowband__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the SVD algorithm with 30,000 data samples, with SNR = 60 dB, at various sampling rates for narrowband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-1, seed 0.}
	\label{fig:LS_narrowband_30v3000}
\end{figure}
\begin{figure}[htb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/20MHz_LS_noisy__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/100MHz_LS_noisy__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/150MHz_LS_noisy__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_LS_noisy__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the SVD algorithm with 30,000 data samples, with SNR = 20 dB at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-1, seed 0.}
	\label{fig:LS_noisy_30v3000}
      \end{figure}
      \begin{figure}[htb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_LS_undEst_2__30vs3000.tikz}}
		\caption{$L = 100$}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_LS_undEst-10__30vs3000.tikz}}
		\caption{$L = 190$}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_LS_overEst_2__30vs3000.tikz}}
		\caption{$L = 400$}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_LS_overEst-10__30vs3000.tikz}}
		\caption{$L = 210$}
	\end{subfigure}
	\caption{Results of the SVD algorithm with 30,000 data samples, with SNR = 60 dB for wideband at 200 MHz sampling rate with incorrect channel orders, $L$ (a) $L/2$, (b) $L - 10$, (c) $L*2$, and (d) $L + 10$, for impulse response set IR-1, seed 0.}
	\label{fig:LS_wrongModel_30v3000}
      \end{figure}
       \begin{figure}[htb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/20MHz_AED_Ideal__30vs3000.tikz}}
		\caption{$F_s=20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/100MHz_AED_Ideal__30vs3000.tikz}}
		\caption{$F_s=100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/150MHz_AED_Ideal__30vs3000.tikz}}
		\caption{$F_s= 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_AED_Ideal__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the AED algorithm with 250,000 data samples, with SNR = 60 dB for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-1, seed 0.}
	\label{fig:AED_ideal_30v3000}
      \end{figure}
             \begin{figure}[htb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/20MHz_AED_rStart__30vs3000.tikz}}
		\caption{$F_s=20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/100MHz_AED_rStart__30vs3000.tikz}}
		\caption{$F_s=100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/150MHz_AED_rStart__30vs3000.tikz}}
		\caption{$F_s= 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_AED_rStart__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the AED algorithm with 250,000 data samples, with SNR = 60 dB for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-1, seed 0 using a normalized random start.}
	\label{fig:AED_rStart_30v3000}
      \end{figure}
      \begin{figure}[!b]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/20MHz_AED_Nless__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/100MHz_AED_Nless__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/150MHz_AED_Nless__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_AED_Nless__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the AED algorithm with 2,500 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-1, seed 0.}
	\label{fig:AED_Nless_30v3000}
        \end{figure}
      \begin{figure}[htb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/20MHz_AED_narrowband__30vs3000.tikz}}
		\caption{$F_s=20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/100MHz_AED_narrowband__30vs3000.tikz}}
		\caption{$F_s=100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/150MHz_AED_narrowband__30vs3000.tikz}}
		\caption{$F_s= 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_AED_narrowband__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the AED algorithm with 250,000 data samples, with SNR = 60 dB for narrowband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-1, seed 0.}
	\label{fig:AED_narrowband_30v3000}
      \end{figure}
      \begin{figure}[htb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/20MHz_AED_NITF__30vs3000.tikz}}
		\caption{$F_s=20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/100MHz_AED_NITF__30vs3000.tikz}}
		\caption{$F_s=100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/150MHz_AED_NITF__30vs3000.tikz}}
		\caption{$F_s= 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_AED_NITF__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the AED algorithm with 250,000 data samples, with SNR = 60 dB for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-2, seed 0.}
	\label{fig:AED_NITF_30v3000}
      \end{figure}
      \begin{figure}[htb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/20MHz_AED_noisy__30vs3000.tikz}}
		\caption{$F_s=20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/100MHz_AED_noisy__30vs3000.tikz}}
		\caption{$F_s=100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/150MHz_AED_noisy__30vs3000.tikz}}
		\caption{$F_s= 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_AED_noisy__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the AED algorithm with 250,000 data samples, with SNR = 20 dB for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-1, seed 0.}
	\label{fig:AED_noisy_30v3000}
      \end{figure}
      \begin{figure}[htb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_AED_undEst_2__30vs3000.tikz}}
		\caption{$L = 100$}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_AED_undEst-10__30vs3000.tikz}}
		\caption{$L = 190$}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_AED_overEst_2__30vs3000.tikz}}
		\caption{$L = 400$}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_AED_overEst-10__30vs3000.tikz}}
		\caption{$L = 210$}
	\end{subfigure}
	\caption{Results of the AED algorithm with 30,000 data samples, with SNR = 60 dB for wideband at 200 MHz sampling rate with incorrect channel orders, $L$ (a) $L/2$, (b) $L - 10$, (c) $L*2$, and (d) $L + 10$, for impulse response set IR-1, seed 0.}
        \label{fig:AED_wrongModel_30v3000}
        \end{figure}
Both pre-existing algorithms show promise, but there are issues associated with both. 

The SVD algorithm can work rather well in the ``ideal'' simulation\footnote{Refer to section \ref{sec:sim_param} for an explanation of the ``Ideal Case''.}, as can be seen in Figure \ref{fig:LS_ideal_30v3000}. 
The channel estimates are very similar to the actual channels, with peaks in the correct corresponding places. 
The authors of \cite{art:LS_Approach_BCID} note in their paper that high SNR, and more data perform better, and discuss necessary and sufficient conditions (such as rank of data matrix $\mathbf{X}$, coprime nature of polynomials and nullspace dimensionality) in which the channels are identifiable.   

However it must be noted that in nonideal conditions (small amount of data, narrowband, low SNR, or incorrect $L$), as seen in Figures \ref{fig:LS_Nless_30v3000}, \ref{fig:LS_narrowband_30v3000}, \ref{fig:LS_noisy_30v3000}, and \ref{fig:LS_wrongModel_30v3000} the algorithm does noticeably poorly. 
Figure \ref{fig:LS_Nless_30v3000} shows the results with fewer data points. Impulse response estimates for the lower sampling rates still perform well. For the higher sampling rates, the model order, $L$, is too close to the number of data points, and so performs poorly. This is because the data essentially becomes an underdetermined system, with too little information to effectively solve for all the model parameters. 
Figure \ref{fig:LS_narrowband_30v3000} shows the results of the SVD approach for the simulated narrowband data. The narrowband estimates do not appear to resolve the two channels at all. The results seem to plot the same estimates, with small variations, for both channels. The plots are intuitive as narrowness in frequency generally results in broadness in the time domain.
Figure \ref{fig:LS_noisy_30v3000} shows the results for lower SNR. Once reasonable noise is added ($SNR=20$ dB), the algorithm appears to deteriorate to random results. 
If the impulse response length, $L$, is incorrect, the estimate also deteriorates, as seen in Figure \ref{fig:LS_wrongModel_30v3000}. Note that the correct model order for $200$ MHz sampling rate simulation is $L = 200$.

Figure \ref{fig:LS_NITF_30v3000} shows the results when the first peak is not the largest peak. Some of the estimates are decent (such as $Fs = 20$ MHz plot), but most of the estimates do not identify the channels correctly. 

Besides the reliability on an ideal setup, the SVD approach also suffers with respect to computational costs. The SVD is well known to be a costly operation, resulting in a slow processing, and utilizes a large amount of memory. The data matrix $X$ of size $(N - L)\times M(L +1)$, has complexity $\mathcal{O}\big((N - L)^2M(L+1)\big)$, or more specifically there are $4\big((N - L)^2M(L + 1)\big) + 8\Big((N - L)\big(M(L + 1)\big)^2\Big) + 9\big(M(L + 1)\big)^3$ computations involved in the calculation for the SVD \cite{book:moon,book:matComp}. Thus, the viability of this option is also limited to the system memory capacity. Due to this limitation, the SVD algorithm can only process approximately 30,000 data points before running out of memory in MATLAB.  

The AED algorithm is a less computationally complex algorithm which iterates through the data matrix row by row to update the channel estimates, requiring only $(N-L)(9M(L+1)+ 2)$ computations. 
The AED algorithm also differs from the SVD algorithm in that it focuses on estimating the time delay between the channels, rather than trying to perfectly estimate the channels themselves. The AED algorithm estimates the largest peaks (which are ideally the direct paths) of the impulse responses, and with the assumptions stated in Chapter \ref{ch:3}, estimates the time delay between peaks. For TDOA, the minimal information is enough, however if identifying the channel profile exactly is important, this algorithm may be unacceptable. 

In \cite{art:AEDDecomp,conf:AED}, Benesty specifies that in order to accurately estimate the time delay between channels the transfer function approximation must initially start with an unit vector, 
\begin{equation} \label{eq:ch_4_unit_vector}
\mathrm{e}_d = \begin{bmatrix}
0 & 0 & \cdots & 0 & 1 & 0 & \cdots & 0 & 0
\end{bmatrix}^\mathrm{T}
\end{equation}
where the value of the $d^{\text{th}}$ index is equal to 1. The $d^{\text{th}}$ index should fall somewhere in the middle of the first or second impulse response, $L$. Due to the mathematical relationship between the two impulse responses, a corresponding peak will appear at the delay amount.

With the initial estimate, $\mathbf{h}^{(0)} = \mathrm{e}_d$, the AED algorithm performs fairly well in ideal conditions as can be seen in Figure \ref{fig:AED_ideal_30v3000}. It should be observed that the estimates are centered in the middle of the sample index due to the position of the $d^\text{th}$ index in the initial impulse response. It should also be observed that the estimates are not quite as complete as the SVD estimates shown. 

However, not all the sources for the AED algorithm explain what the initial transfer function estimate should be \cite{book:AcousticMIMOSigProc}. The only stipulation for the initial estimates for the transfer function approximation is that the $||\mathbf{h}^{(n)}||_2^2 = 1$. So, with this stipulation, a normalized random start, requires more updates, and iterations. However, this typically results in a random normalized result with no true single maximum peak as is shown in Figure \ref{fig:AED_rStart_30v3000}.

Other nonideal conditions can be seen in Figures \ref{fig:AED_Nless_30v3000}, \ref{fig:AED_narrowband_30v3000}, \ref{fig:AED_noisy_30v3000}, and \ref{fig:AED_wrongModel_30v3000}. Figure \ref{fig:AED_Nless_30v3000} shows the results with ${\frac{1}{100}}^{th}$ the data sample size. As with the SVD algorithm, the narrowband data cannot appear to resolve the peaks of the two channels, see Figure \ref{fig:AED_narrowband_30v3000}. The AED algorithm does not appear to be affected greatly by more noise as shown in Figure \ref{fig:AED_noisy_30v3000}, producing similar results to the ideal case in Figure \ref{fig:AED_ideal_30v3000}. Also, the model order does not greatly affect the channel estimates as seen in Figure \ref{fig:AED_wrongModel_30v3000}.

Figure \ref{fig:AED_NITF_30v3000} shows the results of the AED algorithm with IR-2 set. The channel estimates produced are fairly good. The interesting thing to note is that the channels appear to be flipped in order, and $\hat{\mathbf{h}}_2$ is more reflective of $\mathbf{h}_1$, rather than $\mathbf{h}_2$ (which makes $\hat{\mathbf{h}}_1$ reflective of $\mathbf{h}_2$ in turn).

Tables \ref{tab:preExist_Algs_Results_ideal}--\ref{tab:preExist_Algs_Results_randomStart} shows a numerical comparison of the estimates for both of these pre-existing algorithms, and for the modified algorithms introduced  in the next section. A discussion of the numerical results can be found in Section \ref{sec:AlgTesting_ch4}. 