%% Algorithm Selection Function
% This function determines which algorithm is needed to process the data
%   Input:
%       - alg: denotes the algorithm to use
%       - data: the data to process the algorithm with
%       - L: the model order
%       - channels: which two of the channels are desired for processing
%   Output:
%       - delayEst: time Distance Estimate
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function delayEst = algSelect(alg, data, L, channels)
    % Set amount for interpolation
    interpolationAmt = 1;
    % Number or channels
    M = 2;
    % determine data size
    N = length(data);
    % determine peak index for unitary vector
    peak_index = floor((L + 1)/2);
    % Start unitary vector initial estimate
    hest = zeros(M*(L + 1), 1);
    hest(peak_index) = 1;
    % Set nonzero offset for denominator
    epsilon = 0.1e-9;
    % Set step size for adaptive algorithms
    eta = 0.5e-3;
    % Set weighting variables for objective constraints
    alpha = 0.1e-4;
    gamma = 0.1e-4;
    phi = 0.1e-4;
    % Set default blocksize for faster algorithm processing
    maxBlockReset = 1500;
    % Set number of iterations for algorithm
    maxIter = 1;
    % Separate into two channels and normalize
    channel1 = data(channels(1), :).'; channel1 = channel1/norm(channel1);
    channel2 = data(channels(2), :).'; channel2 = channel2/norm(channel2);
    % Create the data matrices
    cols = channel1((L + 1):end);
    rows = flipud(channel1(1:(L + 1)));
    X1 = toeplitz(cols, rows);
    cols = channel2((L + 1):end);
    rows = flipud(channel2(1:(L + 1)));
    X2 = toeplitz(cols, rows);
    % Create toeplitz data matrix 
    X = [X1 -X2];
    % Determine which algorithm to use
    switch alg
        % If Cross-Correlation algorithm
        case 'CC'
            EstChannel = CC(data(channels, :));
        % If Least Squares according to Xu et. al (SVD) algorithm
        case 'SVD'
            EstChannel = LS(X, L, M);
        % If Adaptive Eigenvalue Decomposition algorithm
        case 'AED'
            EstChannel = AED(hest, X, eta, epsilon, L, ...
                                           N, M, maxBlockReset, ...
                                                             maxIter);
        % If Modified Adaptive Eigenvalue Decompostion algorithm
        case 'ModAED'
            EstChannel = modAED(hest, X, eta, epsilon, ...
                                             L, N, M, ...
                                               maxBlockReset, maxIter);
        % If Modified Sparse Adaptive Eigenvalue Decomposition
        % algorithm
        case 'ModAEDS'
            EstChannel = AEDS(hest, X, eta, alpha, ...
                                            epsilon, L, N, M, ...
                                               maxBlockReset, maxIter);
        % If Non Sparse Adaptive Cross-Channel Identification with 
        % Shift-Suppression algorithm
        case 'NSAXIS'
            EstChannel = NS_AXIS(hest, X, eta, alpha, ...
                                          epsilon, L, N, M, ...
                                            maxBlockReset, maxIter, ...
                                                           peak_index);
        % If Adaptive Cross-Channel Identification with Sparse
        % Shift-Suppression algorithm
        case 'AXIS'
            EstChannel = AXIS(hest, X, eta, alpha, ...
                                          epsilon, L, N, M, ...
                                            maxBlockReset, maxIter, ...
                                                           peak_index);
        % If Varied Adaptive Cross-Channel Identification with Sparse
        % Shift-Suppression
        case 'VAXIS'
            EstChannel = vAXIS(hest, X, eta, alpha, ...
                                           epsilon, L, N, M, ...
                                            maxBlockReset, maxIter, ... 
                                                           peak_index);
        % If Dual Fixed-Peak Adaptive Cross-Channel Identification with
        % Spares Shift-Suppression algorithm
        case 'DFPAXIS'
            EstChannel = DFPAXIS(hest, X, eta, alpha, ...
                                            epsilon, L, N, M, ...
                                            maxBlockReset, maxIter, ... 
                                                           peak_index);
        % If Non Sparse "Brute Force" algorithm
        case 'NSBF'
            EstChannel = BF_NS(hest, X, eta, alpha, ...
                                            epsilon, L, N, M, ...
                                                N - L, maxIter, ...
                                                         peak_index);
        % If NonAdaptive "Brute Force" algorithm
        case 'NBF'
            EstChannel = BF(hest, X, eta, alpha, ...
                                            epsilon, L, N, M, ...
                                                N - L, maxIter, ...
                                                         peak_index);
        % If Adaptive "Brute Force" algorithm
        case 'ABF'
            EstChannel = BF(hest, X, eta, alpha, ...
                                        epsilon, L, N, M, ...
                                            maxBlockReset, maxIter, ...
                                                           peak_index);
        % If Varied "Brute Force" algorithm
        case 'VBF'
            EstChannel = vBF(hest, X, eta, alpha, ...
                                         epsilon, L, N, M, ...
                                            maxBlockReset, maxIter, ...
                                                           peak_index);
        % If Dual Fixed-Peak "Brute Force" algorithm
        case 'DFPBF'
            EstChannel = DFPBF(hest, X, eta, alpha, ...
                                            epsilon, L, N, M, ...
                                            maxBlockReset, maxIter, ...
                                                           peak_index);
        % If Non-Sparse Single Constraint algorithm
        case 'NSSC'
            EstChannel = NS_SC(hest, X, eta, alpha, gamma, ...
                                        epsilon, L, N, M, ...
                                           maxBlockReset,  maxIter, ...
                                                           peak_index);
        % If Single Constraint algorithm
        case 'SC'
            EstChannel = SC(hest, X, eta, alpha, gamma, ...
                                        epsilon, L, N, M, ...
                                           maxBlockReset,  maxIter, ...
                                                           peak_index);
        % If Varied Single Constraint algorithm
        case 'VSC'
            EstChannel = vSC(hest, X, eta, alpha, gamma,...
                                         epsilon, L, N, M, ...
                                            maxBlockReset, maxIter, ...
                                                           peak_index);
        % If Dual Fixed-Peak Single Constraint algorithm
        case 'DFPSC'
            EstChannel = DFPSC(hest, X, eta, alpha, ...
                                           gamma, phi, epsilon,L, ...
                                               N, M, maxBlockReset, ...
                                                  maxIter, peak_index);
    end
    % Compute delay
    if strcmp(alg, 'CC')
        % Normalize
        EstChannel = EstChannel/max(EstChannel);
        % Estimate Delay
        [~, pe] = max(interp(abs(EstChannel), ...
                                        interpolationAmt));
        indexes = ...
                interp(-(length(data) - 1):(length(data) - 1),...
                                         interpolationAmt);
        delayEst = abs(indexes(pe));
    else
        % Determine peak location
        [~, ind1] = max(abs(EstChannel(:, 1)));
        [~, ind2] = max(abs(EstChannel(:, 2)));
        % Compute difference
        delayEst = abs(ind1 - ind2);
    end
end