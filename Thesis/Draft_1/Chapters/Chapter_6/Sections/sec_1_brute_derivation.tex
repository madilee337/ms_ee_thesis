\section{Brute Force Derivation}
The derivation of this algorithm, referred to as the Brute Force (BF) method, starts by setting up the Lagrangian,
\begin{equation} \label{eq:brute-lagrangian}
    \mathcal{L} = {\mathbf{h}^{(n + 1)}}^\mathrm{H}\mathbf{X}^\mathrm{H}\mathbf{X}{\mathbf{h}^{(n + 1)}} + \alpha||{\mathbf{h}^{(n + 1)}}^\mathrm{*}||_1 + \lambda({\mathbf{h}^{(n + 1)}}^\mathrm{H}\mathrm{e}_d - 1).
\end{equation}
Taking the derivative with respect to the conjugate of the updated transfer function provides
\begin{equation} \label{eq:brute-derivative-lagrangian}
    \frac{d}{d{\mathbf{h}^{(n + 1)}}^*}\mathcal{L} = \mathbf{X}^\mathrm{H}\mathbf{X}\mathbf{h}^{(n +1)} + \alpha\mathbf{e}^{j\angle{\mathbf{h}}^{(n + 1)}} + \lambda\mathrm{e}_d.
\end{equation}
Assuming, as before, that $\mathbf{e}^{j\angle{\mathbf{h}}^{(n + 1)}}$ is approximately equivalent to $\mathbf{e}^{j\angle{\mathbf{h}}^{(n)}}$, then upon setting Equation (\ref{eq:brute-derivative-lagrangian}) to 0, and solving for $\mathbf{h}^{(n + 1)}$ yields
\begin{equation} \label{eq:brute-update-lambda}
    \mathbf{h}^{(n + 1)} = -(\mathbf{X}^\mathrm{H}\mathbf{X})^{-1}
    (\alpha\mathbf{e}^{j\angle{\mathbf{h}}^{(n)}} + \lambda\mathrm{e}_d),
\end{equation}
an update equation. There is only one Lagrangian multiplier, a system of equations does not need to be set up. Replacing $\mathbf{h}^{(n + 1)}$  in the constraint with the update derived in Equation (\ref{eq:brute-update-lambda}) affords this definition for $\tilde{\lambda}$
\begin{equation} \label{eq:brute-tilde-lambda}
    \tilde{\lambda} = 
    \frac{-1 - [(\mathbf{X}^\mathrm{H}\mathbf{X})^{-1}(\alpha\mathbf{e}^{j\angle{\mathbf{h}}^{(n)}})]^\mathrm{H}\mathrm{e}_d}
    {\mathrm{e}_d^\mathrm{H}((\mathbf{X}^\mathrm{H}\mathbf{X})^{-1})^\mathrm{H}\mathrm{e}_d}.
\end{equation}

Exchanging $\lambda$ with the conjugate of Equation (\ref{eq:brute-tilde-lambda}) in Equation (\ref{eq:brute-update-lambda}) produces the complete update equation used in this algorithm,
\begin{equation} \label{eq:brute-update-complete}
    \mathbf{h}^{(n + 1)} = -(\mathbf{X}^\mathrm{H}\mathbf{X})^{-1}
    \Big(\alpha\mathbf{e}^{j\angle{\mathbf{h}}^{(n)}} + 
    \frac{-1 - [(\mathbf{X}^\mathrm{H}\mathbf{X})^{-1}(\alpha\mathbf{e}^{j\angle{\mathbf{h}}^{(n)}})]^\mathrm{H}\mathrm{e}_d}
    {\mathrm{e}_d^\mathrm{H}((\mathbf{X}^\mathrm{H}\mathbf{X})^{-1})^\mathrm{H}\mathrm{e}_d}\mathrm{e}_d\Big).
\end{equation}

\subsection{Algorithm Advantages, Implementations, and Modifications}
Advantages: One step

Implementations: Initialize $h^{(n)}$ to 0

The BF algorithm is effective, however it contains large matrix inverses, which are costly both in time and in computations. This algorithm can easily be converted into an adaptive algorithm, divvying the data matrix, $\mathbf{X}$ into blocks of data can help combat the heavy computations. Let $\mathbf{X}_r$ denote a section of rows, or a block of data, then Equation (\ref{eq:brute-update-complete}) would become
\begin{equation} \label{eq:brute-adaptive-update-complete}
    \mathbf{h}^{(n + 1)} = -(\mathbf{X}_r^\mathrm{H}\mathbf{X}_r)^{-1}
    \Big(\alpha\mathbf{e}^{j\angle{\mathbf{h}}^{(n)}} + 
    \eta\frac{-1 + [(\mathbf{X}_r^\mathrm{H}\mathbf{X}_r)^{-1}(\alpha\mathbf{e}^{j\angle{\mathbf{h}}^{(n)}})]^\mathrm{H}\mathrm{e}_d}
    {\mathrm{e}_d^\mathrm{H}((\mathbf{X}_r^\mathrm{H}\mathbf{X}_r)^{-1})^\mathrm{H}\mathrm{e}_d}\mathrm{e}_d\Big).
\end{equation}
Depending on the size of the blocks of data, this can take very few steps.

\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/20MHz_NBF_Ideal__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/100MHz_NBF_Ideal__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/150MHz_NBF_Ideal__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_NBF_Ideal__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the Non Adaptive BF algorithm with 250,000 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for transfer function set TF-1}
	\label{fig:NBF_ideal_30v3000}
\end{figure}
\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/20MHz_ABF_Ideal__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/100MHz_ABF_Ideal__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/150MHz_ABF_Ideal__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_ABF_Ideal__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the Adaptive BF algorithm with 250,000 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for transfer function set TF-1}
	\label{fig:ABF_ideal_30v3000}
\end{figure}
\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/20MHz_ABF_NITF__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/100MHz_ABF_NITF__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/150MHz_ABF_NITF__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_ABF_NITF__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the Adaptive BF algorithm with 250,000 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for transfer function set TF-2}
	\label{fig:ABF_NITF_30v3000}
\end{figure}

\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/20MHz_ABF_narrowband__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/100MHz_ABF_narrowband__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/150MHz_ABF_narrowband__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_ABF_narrowband__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the Adaptive BF algorithm with 250,000 data samples, with SNR = 60 dB, at various sampling rates for narrowband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for transfer function set TF-1}
	\label{fig:ABF_narrow_30v3000}
\end{figure}

\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/20MHz_ABF_Nless__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/100MHz_ABF_Nless__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/150MHz_ABF_Nless__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_ABF_Nless__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the Adaptive BF algorithm with 2,500 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for transfer function set TF-1}
	\label{fig:ABF_Nless_30v3000}
\end{figure}
\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/20MHz_ABF_noisy__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/100MHz_ABF_noisy__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/150MHz_ABF_noisy__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_ABF_noisy__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the Adaptive BF algorithm with 250,000 data samples, with SNR = 20 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for transfer function set TF-1}
	\label{fig:ABF_noisy_30v3000}
\end{figure}
\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_ABF_undEst_2__30vs3000.tikz}}
		\caption{$L = 100$}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_ABF_undEst-10__30vs3000.tikz}}
		\caption{$L = 190$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_ABF_overEst_2__30vs3000.tikz}}
		\caption{$L = 400$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_ABF_overEst-10__30vs3000.tikz}}
		\caption{$L = 200$ MHz}
	\end{subfigure}
	\caption{Results of the Adaptive BF algorithm with 250,000 data samples, with SNR = 60 dB for wideband simulation at 200 MHz sampling rate for incorrect model order estimates (a) $L/2$, (b) $L-10$, (c) $2L$, and (d) $L + 10$, for transfer function set TF-1}
	\label{fig:ABF_wrongModel_30v3000}
\end{figure}