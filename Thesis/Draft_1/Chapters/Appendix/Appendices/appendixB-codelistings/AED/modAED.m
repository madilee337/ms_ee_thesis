% Modififed Adaptive Eigenvalue Decomposition Algorithm 
%   - The most basic update algorithm for channel estimation
%   Inputs:
%       - hprev: column vector containing all the transfer functions 
%               estimates size: M(L + 1)x1
%       - x: Toeplitz data matrix size: (N - L)xM(L + 1)
%       - eta: stepsize for update (scalar)
%       - epsilon: the offset for the denominator to be nonzero (small)
%       - L: the length of a transfer function estimate
%       - N: the amount of data in the Toeplitz data matrix
%       - M: the number of channels
%       - maxBlockReset: the data block size used for processing
%       - maxIter: the maximum amount of passes through the data
%   Outputs:
%       - EstChannels: vector containing new updated estimate for transfer
%       functions
function EstChannels = modAED(hprev, x, eta, epsilon, L, N, M, ...
                                                    maxBlockReset, maxIter)
%% Pre-processing and Initializatin
    % Determine iterations through data
    iters = N - (L + 1);    
%% Processing
    % Make Multiple Passes through data (if data chunks are sufficiently
    % small)
    for iter = 1:maxIter
        % Reset block size
        block = maxBlockReset;
        % Iterate through data
        for row = 1:block:iters
            % Verify that the indices are valid
            if row > N - block
                % Set maxBlock such that the indices are not violated
                block = N - row - (L + 1);
            end
            % Get "row" of data
            xr = x(row:row + block - 1, :).';
            % Compute Lagrange multiplier
            tilde_lambda = (hprev'*xr)/(norm(xr) + epsilon);
            % Compute update
            hnew = hprev - eta*xr*tilde_lambda';
            % Save new update for next iteration
            hprev = hnew;
        end
    end
%% Output     
   % Format output
   EstChannels = reshape(hnew, L + 1, M);
end