\appendixsection{Modifications} \label{code:mods}
The code listings here utilize a modification that has shown to provide faster and more accurate results without the need for a myriad of iterations over the same data. The adaptive algorithms use multiple rows of data simultaneously such that each step includes more information and can thus make a better approximation. A mathematical explanation for the modAED algorithm is shown here for improved understanding, although it should be noted, that this modification applies to the other algorithms the same way. 

Using the notation described in Appendix \ref{app:notation}, the data block, $\mathrm{X}_r$, is of size $b \times M(L + 1)$,  and the previous update vector, $\mathbf{h}^{(n)}$, is of size $M(L + 1) \times 1$, as shown below:

\begin{center}
	\begin{tabular}{c c}
		$
		\mathrm{X}_r = \begin{bmatrix}
		\horzbar\mathrm{x}_{1}\horzbar \\
		\horzbar\mathrm{x}_{2}\horzbar \\
		\vdots \\
		\horzbar\mathrm{x}_{b-1}\horzbar \\
		\horzbar\mathrm{x}_{b}\horzbar	
		\end{bmatrix} $
		 &
		\renewcommand*{\arraystretch}{.6}
		$
		\mathbf{h}^{(n)} = \begin{bmatrix}
		\vertbar \\
		\mathbf{h}_j^{(n)} \\
		\vertbar \\
		\cdots \\
		\vertbar \\
		\mathbf{h}_i^{(n)} \\
		\vertbar 
		\end{bmatrix}.
		$	
	\end{tabular}
\end{center}
where $M = 2$. Note that for this explanation, not all the vector are assumed to be in column formatting; rather the vectors will follow the same formatting shown above. 

As it is repeatedly mathematically shown in Appendix \ref{app:maths}, a typical update involves a scaled version of the previous transfer function estimate vector multiplied by a row of the data matrix and then added to the previous transfer function estimate.  The update for the modAED algorithm as shown in Appendix \ref{app:AED}, is
\begin{equation} \label{eq:code_normal_modAEDUpdate}
	\mathbf{h}^{(n + 1)} = \mathbf{h}^{(n)} - \eta\frac{
		\mathrm{x}_r^\mathrm{*}{\mathbf{h}^{(n)}}}{||\mathrm{x}_r||^2_2}\mathrm{x}_r^\mathrm{T}.
\end{equation}

As the update is written, the matrix multiplication would be
\renewcommand*{\arraystretch}{.6}
\begin{equation} \label{eq:code_matMult_modAEDUpdate}
%	\begin{aligned}
	\underbrace{
		\begin{bmatrix}
			\vertbar \\
			\mathbf{h}_j^{(n + 1)} \\
			\vertbar \\
			\cdots \\
			\vertbar \\
			\mathbf{h}_i^{(n + 1)} \\
			\vertbar
		\end{bmatrix}}_{M(L + 1) \times 1} = 
		\underbrace{
		\begin{bmatrix}
			\vertbar \\
			\mathbf{h}_j^{(n)} \\
			\vertbar \\
			\cdots \\
			\vertbar \\
			\mathbf{h}_i^{(n)} \\
			\vertbar 
		\end{bmatrix}}_{M(L + 1) \times 1}
			- \eta\underbrace{\left(\underbrace{
							\begin{bmatrix}
								\horzbar \mathrm{x}_r ^*\horzbar
							\end{bmatrix}
							\begin{bmatrix}
								\vertbar \\
								\mathbf{h}_j^{(n)} \\
								\vertbar \\
								\cdots \\
								\vertbar \\
								\mathbf{h}_i^{(n)} \\
								\vertbar 
							\end{bmatrix}}_{1 \times 1}
							\Biggl/\left(
							\underbrace{
							\begin{bmatrix}
								\horzbar \mathrm{x}_r \horzbar
							\end{bmatrix}
							\begin{bmatrix}
								\vertbar \\
								\mathrm{x}_r^\mathrm{H} \\
								\vertbar
							\end{bmatrix}}_{1 \times 1}
							\right)\right)}_{1 \times 1} \underbrace{
							\begin{bmatrix}
								\vertbar \\
								\mathrm{x}_r^\mathrm{T} \\
								\vertbar
							\end{bmatrix} }_{M(L + 1)\times 1}.\\
%	\end{aligned}
\end{equation}
This method updates each individual coefficient of the previous update vector, $\mathbf{h}^{(n)}$, with the same weighted scalar of the data.

The data block modification requires Equation (\ref{eq:code_normal_modAEDUpdate}) to be rearranged, and modified without loss of generality, to
\begin{equation} \label{eq:code_modified_modAEDUpdate}
\mathbf{h}^{(n + 1)} = \mathbf{h}^{(n)} - \eta\mathrm{X}_r^\mathrm{T}\left({\frac{{{\mathbf{h}^{(n)}}^\mathrm{H}\mathrm{X}_r^\mathrm{T}}}{||\mathrm{X}_r||^2_2}}\right)^\mathrm{H}.
\end{equation}

The matrix multiplication would (as follows) be slightly different from Equation (\ref{eq:code_matMult_modAEDUpdate}). 
\renewcommand*{\arraystretch}{.6}
\begin{multline} \label{eq:code_matMult_modified_modAEDUpdate}
	%	\begin{aligned}
	\underbrace{
		\begin{bmatrix}
			\vertbar \\
			\mathbf{h}_j^{(n + 1)} \\
			\vertbar \\
			\cdots \\
			\vertbar \\
			\mathbf{h}_i^{(n + 1)} \\
			\vertbar
		\end{bmatrix}}_{M(L + 1) \times 1} = 
	\underbrace{
		\begin{bmatrix}
			\vertbar \\
			\mathbf{h}_j^{(n)} \\
			\vertbar \\
			\cdots \\
			\vertbar \\
			\mathbf{h}_i^{(n)} \\
			\vertbar 
		\end{bmatrix}}_{M(L + 1) \times 1} 
		- \eta \underbrace{	\begin{bmatrix}
											\vertbar & \vertbar & & \vertbar \\
											\mathrm{x}_{1} & \mathrm{x}_{2} & \cdots & \mathrm{x}_{b-1} \\
											\vertbar & \vertbar & & \vertbar	
										\end{bmatrix}}_{M(L + 1)\times b} \\
				\underbrace{\left(\underbrace{
				\begin{bmatrix}
					\horzbar {\mathbf{h}_j^{(n)}}^* \horzbar & \vdots & \horzbar{\mathbf{h}_i^{(n)}}^* 	\horzbar 
				\end{bmatrix}
				\begin{bmatrix}
					\vertbar & \vertbar & & \vertbar \\
					\mathrm{x}_{1} & \mathrm{x}_{2} & \cdots & \mathrm{x}_{b-1} \\
					\vertbar & \vertbar & & \vertbar	
				\end{bmatrix}}_{1 \times b}
		.\Biggl/\left(
		\underbrace{
			\begin{bmatrix}
				||\mathrm{x}_1||_2^2 &
				||\mathrm{x}_2||_2^2 &
				\cdots &
				||\mathrm{x}_b||_2^2
			\end{bmatrix}}_{1 \times b}
		\right)\right)^\mathrm{H}}_{b \times 1}.
	%	\end{aligned}
\end{multline}
Note that for this instance, the $./$ is an element-wise divide, meaning that each element of the left side is divided by the corresponding element on the right side of the divide (this is the same notation as in MATLAB).

When the transfer function estimate, $\mathbf{h}^{(n + 1)}$ ,is then updated, the method differs in that each individual coefficient of the filter is individually updated, providing a better, more accurate estimate. This modification packs more information into each step, thus eliminating the need for more passes through the data, and a smarter more specialized adaptive method. 
 \newpage