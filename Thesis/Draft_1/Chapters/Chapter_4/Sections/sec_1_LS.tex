\section{Least-Squares Blind Channel Identification according to Xu et. al}
The least squares (LS) BCI algorithm put forth by Xu et. al in \cite{art:LS_Approach_BCID} uses the cross-relation (CR), and the assumption that the transfer functions are in the nullspace of the data matrix as described in the following sections.
\subsection{System Model for the Least-Squares Approach}
Consider a source, at some unknown location, transmitting an unknown signal, $s(n)$, in a multipath environment. The transmission is detected by multiple receivers. The system is modeled as
\begin{equation}
    \label{eq:system}
    \begin{cases}
        x_{1}(n)=s(n)*h_{1}(n)+w_{1}(n) \\ 
        \ \vdots \\ 
        x_{M}(n)=s(n)*h_{M}(n)+w_{M}(n), 
    \end{cases}
\end{equation}
where $*$ denotes convolution, $h_{i}(n)$ denotes the individual channel mapping from the source to the $i^\mathrm{th}$ receiver, $x_{i}(n)$ the value received from the transmitted signal at the $i^{th}$ receiver, and $\mathrm{w}(n) = [w_{1}(n)\hspace{0.1cm}w_{2}(n)...w_{M}(n)]$ is additive white noise, and $n$ denotes the $n^\mathrm{th}$ sample. $M$ denotes the number of receivers. This model assumes a finite impulse response, and is a single input, multiple output (SIMO) system, as is shown in Figure \ref{fig:transmission_figure}.
\begin{figure}[htb]
    \centering
    \includegraphics[width = .5\textwidth]{Chapters/Chapter_4/Figures/data_transmission.png}
    \caption{System Model}
    \label{fig:transmission_figure}
\end{figure}

\subsection{Complex Cross Relation} \label{sec:ch_4_LS_conj_cr}
Neglecting noise, the signal model, Equation (\ref{eq:system}), can be expressed individually as $x_i(n) = s(n)*h_i(n)$. Convolving $x_i(n)$ with $h_j(n)$, where $i \neq j$ produces a CR,
\begin{equation}
    \label{eq:cr-derivation}
    \begin{split}
        h_j(n)*x_i(n) = h_j(n)*[s(n)*h_i(n)] \\
        h_j(n)*x_i(n) = h_i(n)*[s(n)*h_j(n)] \\
       h_j(n)*x_i(n) = h_i(n)*x_j(n) \\
       \boxed{h_j(n)*x_i(n) - h_i(n)*x_j(n) = 0}.
    \end{split}
\end{equation}

The CR is the key to the BCI algorithms. Equation (\ref{eq:cr-derivation}) can be rewritten for two receiver measurements as the matrix equation\footnote{For multiple receivers ($>2$), Equation (\ref{eq:cr-matrix}) can be modified as seen in Appendix \ref{app:notation}}
\begin{equation} \label{eq:cr-matrix}
        \begin{bmatrix} \mathrm{X}_i(L) & \vdots & -\mathrm{X}_j(L)
        \end{bmatrix}
        \begin{bmatrix} \mathbf{h}_j \\ \mathbf{h}_i \end{bmatrix} = 0,
\end{equation}
where $\mathrm{X}_m(L)$ is a Toeplitz matrix of the form
\begin{equation} \notag
    \mathrm{X}_m(L) = \begin{bmatrix}
                        x_m(L) & x_m(L - 1) & \cdots & x_m(0) \\
                        x_m(L + 1) & x_m(L) & \cdots & x_m(1) \\
                        \vdots & \ddots & \ddots & \vdots \\
                        x_m(N) & x_m(N - 1) & \cdots & x(N - L - 1)
                      \end{bmatrix},
\end{equation}
and $\mathbf{h}_m$ is the impulse response vector 
\begin{equation} \notag
        \mathbf{h}_m \triangleq [h_m(0), \cdots, h_m(L)]^{\mathrm{T}}.
    \end{equation}
For brevity, Equation (\ref{eq:cr-matrix}) can be rewritten as
\begin{equation} \label{eq:cr-brief}
    \mathbf{X}\mathbf{h} = \mathbf{0}.
\end{equation}
Equation (\ref{eq:cr-brief}) is the basis for the approach derived by Xu et. al \cite{art:LS_Approach_BCID}, referred to as the LS algorithm\footnote{although all the algorithms are LS algorithms to some extent}. Equation (\ref{eq:cr-brief}) implies that the $\mathbf{h}$ is in the nullspace of the data matrix $\mathbf{X}$ (and vice versa). 
\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/20MHz_LS_Ideal__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/100MHz_LS_Ideal__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/150MHz_LS_Ideal__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_LS_Ideal__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the LS algorithm with 30,000 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for transfer function set TF-1}
	\label{fig:LS_ideal_30v3000}
\end{figure}
\begin{figure}[!b]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/20MHz_LS_Nless__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/100MHz_LS_Nless__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/150MHz_LS_Nless__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_LS_Nless__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the LS algorithm with 300 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for transfer function set TF-1}
	\label{fig:LS_Nless_30v3000}
\end{figure}
\subsection{A Least-Squares Approach According to Xu et. al}
The first approach looks at minimizing Equation (\ref{eq:cr-brief}), 
\begin{equation} \label{eq:min_cr_brief}
\begin{aligned}
&\underset{\mathbf{h}}{\mathrm{minimize}}&  &||\mathbf{X}\mathbf{h}||_2^2& \\
&\text{subject to}&  &||\mathbf{h}||_2^2 = 1,&
\end{aligned}
\end{equation}
\begin{figure}[htb]
    \centering
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/20MHz_LS_NITF__30vs3000.tikz}}
        \caption{$F_s = 20$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/100MHz_LS_NITF__30vs3000.tikz}}
        \caption{$F_s = 100$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/150MHz_LS_NITF__30vs3000.tikz}}
        \caption{$F_s = 150$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/200MHz_LS_NITF__30vs3000.tikz}}
        \caption{$F_s = 200$ MHz}
    \end{subfigure}
    \caption{Results of the LS algorithm with 30,000 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for transfer function set TF-2}
    \label{fig:LS_NITF_30v3000}
\end{figure}
\begin{figure}[htb]
    \centering
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/20MHz_LS_narrowband__30vs3000.tikz}}
        \caption{$F_s = 20$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/100MHz_LS_narrowband__30vs3000.tikz}}
        \caption{$F_s = 100$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/150MHz_LS_narrowband__30vs3000.tikz}}
        \caption{$F_s = 150$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/200MHz_LS_narrowband__30vs3000.tikz}}
        \caption{$F_s = 200$ MHz}
    \end{subfigure}
    \caption{Results of the LS algorithm with 30,000 data samples, with SNR = 60 dB, at various sampling rates for narrowband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for transfer function set TF-1}
    \label{fig:LS_narrowband_30v3000}
\end{figure}
\begin{figure}[htb]
    \centering
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/20MHz_LS_noisy__30vs3000.tikz}}
        \caption{$F_s = 20$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/100MHz_LS_noisy__30vs3000.tikz}}
        \caption{$F_s = 100$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/150MHz_LS_noisy__30vs3000.tikz}}
        \caption{$F_s = 150$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/200MHz_LS_noisy__30vs3000.tikz}}
        \caption{$F_s = 200$ MHz}
    \end{subfigure}
    \caption{Results of the LS algorithm with 30,000 data samples, with SNR = 20 dB at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for transfer function set TF-1}
    \label{fig:LS_noisy_30v3000}
\end{figure}
where $\mathbf{h}$ is an approximation of the true transfer functions. This is done by taking the singular value decomposition (SVD) of the matrix $\mathbf{X}$, as $\mathbf{h}$ is assumed to be in the nullspace of $\mathbf{X}$. The constraint implies that the transfer function estimate cannot be zero, although it does not play into the calculation of the SVD.

The code for this algorithm can be seen in the code listing in Appendix \ref{code:LS}. 

\begin{figure}[htb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_LS_undEst_2__30vs3000.tikz}}
		\caption{$L = 100$}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_LS_undEst-10__30vs3000.tikz}}
		\caption{$L = 190$}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_LS_overEst_2__30vs3000.tikz}}
		\caption{$L = 400$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_LS_overEst-10__30vs3000.tikz}}
		\caption{$L = 210$ MHz}
	\end{subfigure}
	\caption{Results of the LS algorithm with 30,000 data samples, with SNR = 60 dB for wideband at 200 MHz sampling rate with incorrect lengths of transfer functions $L$ (a) $L/2$, (b) $L - 10$, (c) $L*2$, and (d) $L + 10$, for transfer function set TF-1,}
	\label{fig:LS_wrongModel_30v3000}
\end{figure}

\subsection{LS Algorithm Advantages, Implementations  and Disadvantages}
This algorithm can work rather well in ideal (high SNR, wideband, first peak corresponds to largest peak, large amount of data, exact length of transfer functions ($L$, generally referred to as model order) known) simulation, as can be seen in Figure \ref{fig:LS_ideal_30v3000}. 
The channel estimates are very similar to the actual channels, with peaks in the right corresponding places. 
The authors of \cite{art:LS_Approach_BCID} note in their paper that high SNR, and more data perform better, and discuss necessary and sufficient conditions (such as rank of data matrix $\mathbf{X}$, coprime nature of polynomials and nullspace dimensionality) in which the channels are identifiable.   
 
However it must be noted that in nonideal conditions (small amount of data, first peak does not correspond to the largest peak, narrowband, low SNR, or incorrect $L$), as seen in Figures \ref{fig:LS_Nless_30v3000},  \ref{fig:LS_NITF_30v3000}, \ref{fig:LS_narrowband_30v3000},  
\ref{fig:LS_noisy_30v3000}, and \ref{fig:LS_wrongModel_30v3000} the algorithm does noticeably poorer. 

Figure \ref{fig:LS_Nless_30v3000} shows the results with less data points. Transfer functions estimates for the lower sampling rates still perform well. For the higher sampling rates, the length of the transfer functions, $L$, is too close to the amount of data points, and so performs poorly. 
Figure \ref{fig:LS_NITF_30v3000} shows the results when the first peak is not the largest peak. Some of the estimates are decent (such as $Fs = 20$ MHz plot), but most of the estimates do not identify the channels correctly. 
Figure \ref{fig:LS_narrowband_30v3000} shows the results of the LS approach for narrowband data. The narrowband estimates do not appear to resolve the two channels at all. The results seem to plot the same estimates, with small variations, for both channels. The plots are intuitive to some extent as narrowness in frequency generally results in broadness in the time domain.
Figure \ref{fig:LS_noisy_30v3000} shows the results for lower SNR. Once reasonable noise is added, the algorithm appears to deteriorate to random nonsensical results. 
If the transfer function length, $L$, is incorrect, the estimate also deteriorates, as seen in Figure \ref{fig:LS_wrongModel_30v3000}. Note that the correct model order for $200$ MHz sampling rate simulation is $L = 200$.

Besides the reliability on an ideal setup, the LS approach also suffers with respect to computational costs. The SVD is well known to be a costly operation, resulting in a slow processing, and utilizes a large amount of memory. Thus, the viability of this option is also limited to the system memory capacity. Due to this limitation, the LS algorithm can only process approximately 30,000 data points before running out of memory in MATLAB.  