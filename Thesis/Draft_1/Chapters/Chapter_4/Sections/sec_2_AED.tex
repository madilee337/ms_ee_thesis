\section{Adaptive Eigenvalue Decomposition} \label{sec:AED}
The Adaptive Eigenvalue Decomposition (AED) as referred to in \cite{art:AEDDecomp,book:AcousticMIMOSigProc,conf:AED} varies from the similar ''modified" AED algorithm (modAED) discussed in this thesis. 
The AED algorithm was presented for acoustic signals, and thus assumed to be real, also differing from the assumption used in this research that the data is complex RF signals.
A similar approach is taken however, and produces similar results as the AED algorithm, as seen in Figures \ref{fig:AED_ideal_30v3000}, and \ref{fig:modAED_ideal_30v3000}. To avoid misrepresenting the AED algorithm, the algorithm derived here will be referred to as the modified AED (modAED) algorithm. 
\begin{figure}[tb]
    \centering
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/20MHz_AED_Ideal__30vs3000.tikz}}
        \caption{$F_s = 20$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/100MHz_AED_Ideal__30vs3000.tikz}}
        \caption{$F_s = 100$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/150MHz_AED_Ideal__30vs3000.tikz}}
        \caption{$F_s = 150$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/200MHz_AED_Ideal__30vs3000.tikz}}
        \caption{$F_s = 200$ MHz}
    \end{subfigure}
    \caption{Results of the AED algorithm with 250,000 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for transfer function set TF-1}
    \label{fig:AED_ideal_30v3000}
\end{figure}
\begin{figure}[tb]
    \centering
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/20MHz_ModAED_Ideal__30vs3000.tikz}}
        \caption{$F_s = 20$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/100MHz_ModAED_Ideal__30vs3000.tikz}}
        \caption{$F_s = 100$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/150MHz_ModAED_Ideal__30vs3000.tikz}}
        \caption{$F_s = 150$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/200MHz_ModAED_Ideal__30vs3000.tikz}}
        \caption{$F_s = 200$ MHz}
    \end{subfigure}
    \caption{Results of the modAED algorithm with 250,000 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for transfer function set TF-1}
    \label{fig:modAED_ideal_30v3000}
\end{figure}
\subsection{System Model for the Modified Adaptive Eigenvalue Decomposition}
The system model remains the same as the model shown in Figure \ref{fig:transmission_figure} for the AED algorithm, however for ease of computation, a slightly different model is used for all the other algorithms.
As before, consider a source, at some unknown location, transmitting an unknown signal, $s(n)$, in a multipath environment. The transmission is detected by multiple receivers. The system is modeled as
\begin{equation}
    \label{eq:system_conj}
    \begin{cases}
        x_{1}(n)=s(n)*h^*_{1}(n)+w_{1}(n) \\ 
        \ \vdots \\ 
        x_{M}(n)=s(n)*h^*_{M}(n)+w_{M}(n), 
    \end{cases}
\end{equation}
where $*$ denotes convolution, $h^*_{i}(n)$ denotes the complex conjugate of each individual channel mapping from the source to the $i^\mathrm{th}$ receiver, $x_{i}(n)$ the value received from the transmitted signal at the $i^{th}$ receiver, and $\mathrm{w}(n) = [w_{1}(n)\hspace{0.1cm}w_{2}(n)...w_{M}(n)]$ is additive white noise, and $n$ denotes the $n^\mathrm{th}$ sample. $M$ denotes the number of receivers. This model also assumes a finite impulse response, and is a SIMO system.
\subsection{Complex Conjugate Cross Relation}
As done previously, neglecting noise, the signal model, Equation (\ref{eq:system_conj}), can be expressed individually as $x_i(n) = s(n)*h^*_i(n)$. Convolving $x_i(n)$ with $h^*_j(n)$, where $i \neq j$ produces a CR, 
\begin{equation}
    \label{eq:cr-derivation_conj}
    \begin{split}
        h^*_j(n)*x_i(n) = h^*_j(n)*[s(n)*h^*_i(n)] \\
       \boxed{h^*_j(n)*x_i(n) - h^*_i(n)*x_j(n) = 0.}
    \end{split}
\end{equation}
Equation (\ref{eq:cr-derivation_conj}) can also be expanded for two receiver measurements as the matrix equation\footnote{For multiple receivers ($>2$), Equation (\ref{eq:cr-matrix_conj}) can be modified as seen in the appendix, \ref{app:notation}}

\begin{equation} \label{eq:cr-matrix_conj}
        \begin{bmatrix} \mathrm{X}_i(L) & \vdots & -\mathrm{X}_j(L)
        \end{bmatrix}
        \begin{bmatrix} \mathbf{h}^*_j \\ \mathbf{h}^*_i \end{bmatrix} = 0,
\end{equation}
where $\mathrm{X}_m(L)$ is a Toeplitz matrix of the same form described in section \ref{sec:ch_4_LS_conj_cr},
%\begin{equation} \notag
%    \mathrm{X}_m(L) = \begin{bmatrix}
%                        x_m(L) & x_m(L - 1) & \cdots & x_m(0) \\
%                        x_m(L + 1) & x_m(L) & \cdots & x_m(1) \\
%                        \vdots & \ddots & \ddots & \vdots \\
%                        x_m(N) & x_m(N - 1) & \cdots & x(N - L - 1)
%                      \end{bmatrix}
%\end{equation}
and $\mathbf{h}^*_m$ is the impulse response vector 
\begin{equation} \notag
        \mathbf{h}^*_m \triangleq [h_m(0), \cdots, h_m(L)]^{\mathrm{H}}.
    \end{equation}
For brevity, Equation (\ref{eq:cr-matrix_conj}) can be rewritten as
\begin{equation} \label{eq:cr-brief_conj}
    \mathbf{X}\mathbf{h}^* = \mathbf{0}.
\end{equation}
Equation (\ref{eq:cr-brief_conj}) is the basis for the approaches derived by Benesty \cite{art:AEDDecomp}, and explored in this research. As explained in 
\ref{sec:ch_4_LS_conj_cr}, Equation (\ref{eq:cr-brief_conj}) implies that $\mathbf{h}^*$ is in the null space of $\mathbf{X}$ (and vice versa). 

\subsection{Modified Adaptive Eigenvalue Decomposition}
The AED based algorithms take an adaptive least-squares approach. It looks at minimizing the update between estimates, 
\begin{equation} \label{eq:AED_objective}
    \begin{aligned}
        & \min_{{\mathbf{h}^{(n + 1)}}^{\mathrm{*}}}&  &||\mathbf{h}^{(n + 1)} - \mathbf{h}^{(n)}||_2^2& \\
        & \text{subject to}&  &{\mathbf{h}^{(n + 1)}}^\mathrm{H}\mathrm{x}_{r} = 0,&
    \end{aligned}
\end{equation}
where $\mathrm{x}_{r}$ is a row of the data matrix $\mathbf{X}$,  transposed as all vectors are assumed to be column formatted.

\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/20MHz_ModAED_NITF__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/100MHz_ModAED_NITF__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/150MHz_ModAED_NITF__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_ModAED_NITF__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the modAED algorithm with 250,000 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for transfer function set TF-2}
	\label{fig:modAED_NITF_30v3000}
\end{figure}
Both algorithms (the AED, and the modAED) enforce the CR relationship as a constraint, rather than the objective function like the LS approach described in the last section. 
The AED algorithm uses a different update algorithm than the modAED algorithm derived below, as can be seen in \cite{art:AEDDecomp,conf:AED,book:AcousticMIMOSigProc}.
The update equation for this algorithm is derived using typical minimization techniques. 

Initally setting  up the Lagrangian produces 
\begin{equation} \label{eq:AED_lagrangian}
    \begin{aligned}
        \mathcal{L} = {\mathbf{h}^{(n + 1)}}^\mathrm{H}\mathbf{h}^{(n + 1)}
        - {\mathbf{h}^{(n + 1)}}^\mathrm{H}\mathbf{h}^{(n)}
        - {\mathbf{h}^{(n)}}^\mathrm{H}\mathbf{h}^{(n + 1)}
        + {\mathbf{h}^{(n)}}^\mathrm{H}\mathbf{h}^{(n)}
        + \lambda{\mathbf{h}^{(n + 1)}}^\mathrm{H}\mathrm{x}_{r}.
    \end{aligned}
\end{equation}

The update is then obtained  by taking the derivative of Equation (\ref{eq:AED_lagrangian}), with respect to the updated transfer function estimate, $\mathbf{h}^{(n + 1)}$. Then, setting the derivative equal to 0 and solving for the updated transfer function estimate  forms
\begin{equation} \label{eq:AED_update_lambda}
    \begin{aligned}
       &\frac{d}{d\mathbf{h}^{(n + 1)}}\mathcal{L}& &=& &2\mathbf{h}^{(n + 1)} - 2\mathbf{h}^{(n)} + \lambda\mathrm{x}_r& \\
       &\mathbf{h}^{(n + 1)}& &=& &\mathbf{h}^{(n)} - \lambda\mathrm{x}_r.&
    \end{aligned}
\end{equation}

The value for $\lambda$ is found by substituting the results of Equation (\ref{eq:AED_update_lambda}) into the constraint listed in Equation (\ref{eq:AED_objective}) as
\begin{equation} \label{eq:AED_constraint}
    \begin{aligned}
        \tilde{\lambda} = \frac{{\mathbf{h}^{(n)}}^\mathrm{H}\mathrm{x}_r}
        {||\mathrm{x}_r||_2^2}
    \end{aligned}
\end{equation}
Note that $\tilde{\lambda} = \lambda^\mathrm{H}$. Replacing the $\lambda$ in Equation (\ref{eq:AED_update_lambda}) with the conjugate of Equation (\ref{eq:AED_constraint}) provides the update used in the modAED algorithm,
\begin{equation} \label{eq:modAED_final_update}
    \begin{aligned}
        \mathbf{h}^{(n + 1)} = \mathbf{h}^{(n)} - \eta\frac{\mathrm{x}_r^\mathrm{H}{\mathbf{h}^{(n)}}}
        {||\mathrm{x}_r||_2^2}\mathrm{x}_r.
    \end{aligned}
\end{equation}
Note, $\eta$ in Equation (\ref{eq:modAED_final_update}) was added (as a step size) to make the equation more adaptive, limiting the change between steps. 

The complete derivation and the code listing for the modAED algorithm are found in Appendix \ref{app:AED}, and \ref{code:modAED} respectively.

For easier comparison, the AED algorithm update normalizes by the transfer function estimate rather than the data
\begin{equation}
	\begin{split}
		\mathbf{h}^{(n + 1)} &= \mathbf{h}^{(n)}  - \eta\frac{{\mathbf{h}^{(n)}}^{H}\mathrm{x_r}}
		{||\mathrm{h}^{(n)}||_2^2}\mathrm{x}_r \\
		&= \frac{\mathbf{h}^{(n + 1)}}{||\mathbf{h}^{(n + 1)}||_2^2}	
		\end{split}
\end{equation}
Note that this update has been slightly modified to compensate for the complex data for the true update (for Acoustic data assumed to be real), see \cite{book:AcousticMIMOSigProc, art:AEDDecomp,conf:AED}.

\begin{figure}[tb]
    \centering
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/20MHz_ModAED_narrowband__30vs3000.tikz}}
        \caption{$F_s = 20$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/100MHz_ModAED_narrowband__30vs3000.tikz}}
        \caption{$F_s = 100$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/150MHz_ModAED_narrowband__30vs3000.tikz}}
        \caption{$F_s = 150$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/200MHz_ModAED_narrowband__30vs3000.tikz}}
        \caption{$F_s = 200$ MHz}
    \end{subfigure}
    \caption{Results of the modAED algorithm with 250,000 data samples, with SNR = 60 dB, at various sampling rates for narrowband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for transfer function set TF-1}
    \label{fig:modAED_narrowband_30v3000}
\end{figure}
\begin{figure}[tb]
    \centering
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/20MHz_ModAED_noisy__30vs3000.tikz}}
        \caption{$F_s = 20$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/100MHz_ModAED_noisy__30vs3000.tikz}}
        \caption{$F_s = 100$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/150MHz_ModAED_noisy__30vs3000.tikz}}
        \caption{$F_s = 150$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/200MHz_ModAED_noisy__30vs3000.tikz}}
        \caption{$F_s = 200$ MHz}
    \end{subfigure}
    \caption{Results of the modAED algorithm with 250,000 data samples, with SNR = 20 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for transfer function set TF-1}
    \label{fig:modAED_noisy_30v3000}
\end{figure}
\begin{figure}[tb]
    \centering
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/20MHz_ModAED_Nless__30vs3000.tikz}}
        \caption{$F_s = 20$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/100MHz_ModAED_Nless__30vs3000.tikz}}
        \caption{$F_s = 100$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/150MHz_ModAED_Nless__30vs3000.tikz}}
        \caption{$F_s = 150$ MHz}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering
        \resizebox{\linewidth}{!}
            {\input{Chapters/Chapter_4/Figures/200MHz_ModAED_Nless__30vs3000.tikz}}
        \caption{$F_s = 200$ MHz}
    \end{subfigure}
    \caption{Results of the modAED algorithm with 2,500 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for transfer function set TF-1}
    \label{fig:modAED_Nless_30v3000}
\end{figure}
\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_ModAED_undEst_2__30vs3000.tikz}}
		\caption{$L = 100$}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_ModAED_undEst-10__30vs3000.tikz}}
		\caption{$L=190$}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_ModAED_overEst_2__30vs3000.tikz}}
		\caption{$L = 400$}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_ModAED_overEst-10__30vs3000.tikz}}
		\caption{$L = 210$}
	\end{subfigure}
	\caption{Results of the modAED algorithm with 250,000 data samples, with SNR = 60 dB for wideband simulation at 200 MHz sampling rate for incorrect estimates of transfer function length, $L$, (a) $L/2$, (b) $L - 10$, (c) $L*2$, and (d) $L + 10$, for transfer function set TF-1}
	\label{fig:modAED_wrongModel_30v3000}
\end{figure}
\subsection{Modified Adaptive Eigenvalue Decomposition Algorithm Advantages, Implementations and Disadvantages}
The set of AED algorithms differ from the LS algorithm in that they focus on estimating the time delay between the channels, rather than trying to perfectly estimate the channels themselves. The AED algorithms estimate the direct paths (largest peaks) of the impulse responses, and with the assumptions stated in Chapter \ref{ch:3}, estimates the time delay between peaks. For TDOA, the minimal information is enough, however if identifying the channel profile exactly is important, this algorithm suffers. 

In \cite{art:AEDDecomp,conf:AED}, Benesty specifies that in order to accurately estimate the time delay between channels the transfer function approximation must initially start with a type of unit vector, 
\begin{equation} \label{eq:ch_4_unit_vector}
    \mathrm{e}_d = \begin{bmatrix}
                        0 & 0 & \cdots & 0 & 1 & 0 & \cdots & 0 & 0
                   \end{bmatrix}^\mathrm{T}
\end{equation}
where the $d^\text{th}$ index is equal to 1. The $d^\text{th}$ index should fall somewhere in the middle of the length of the first or second transfer function, $L$. Due to the mathematical relationship between the two transfer functions, a corresponding peak will appear at the delay amount.

With the initial estimate, $\mathbf{h}^{(0)} = \mathrm{e}_d$, the AED algorithms (AED and modAED) both perform fairly well as can be seen in Figure \ref{fig:AED_ideal_30v3000}, and \ref{fig:modAED_ideal_30v3000} (in ideal conditions). It should be observed that the estimates are centered in the middle of the sample index due to the position of the $d^\text{th}$ index in the initial transfer function. It should also be observed that both estimates are not quite as complete as the LS estimates shown in the previous section, and the results of the AED algorithm are more noisy than the results of the modAED algorithm.

Nonideal conditions were also tested as shown in Figures \ref{fig:modAED_NITF_30v3000}, \ref{fig:modAED_narrowband_30v3000}, \ref{fig:modAED_noisy_30v3000},  \ref{fig:modAED_Nless_30v3000}, and \ref{fig:modAED_wrongModel_30v3000}.
Figure \ref{fig:modAED_NITF_30v3000} shows the outcome of the modAED algorithm for  the set of transfer functions, TF-2, where the largest peak is not necessarily the first peak. It should ne noted that the algorithm switches the estimates for the transfer functions, and places them in the reflective position of the $d^{th}$ index.
Figure \ref{fig:modAED_narrowband_30v3000} display the estimates for narrowband simulation. As with the LS algorithm, the narrowband suffers due to the relationship between the frequency and time domain. 
Figure \ref{fig:modAED_noisy_30v3000} reflects that more noise does not significantly impact the estimates, .
Figure \ref{fig:modAED_Nless_30v3000} shows that considerably less samples for computation do not severely alter the algorithm either.
The model order, $L$, does not need to be known for the transfer function estimates either, as shown in Figure \ref{fig:modAED_wrongModel_30v3000}.

\begin{figure}[!t]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/20MHz_ModAED_rStart__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/100MHz_ModAED_rStart__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/150MHz_ModAED_rStart__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_ModAED_rStart__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the modAED algorithm with 250,000 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for transfer function set TF-1 and normalized random initial estimates after 100 iterations}
	\label{fig:modAED_rStart_30v3000}
\end{figure}

However, not all the sources for the AED algorithm explain what the initial transfer function estimate should be \cite{book:AcousticMIMOSigProc}. The only stipulation for the initial estimates for the transfer function approximation is that the $||\mathbf{h}^{(n)}||_2^2 = 1$. So with this stipulation, a normalized random start, requires more updates, and iterations. However, this typically results in a random normalized result with no true single maximum peak as is shown in Figure \ref{fig:modAED_rStart_30v3000}. 

\begin{figure}[!b]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/20MHz_ModAEDS_Ideal__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/100MHz_ModAEDS_Ideal__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/150MHz_ModAEDS_Ideal__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_ModAEDS_Ideal__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the modAEDS algorithm with 250,000 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for transfer function set TF-1}
	\label{fig:modAEDS_ideal__30v3000}
\end{figure}
%\begin{figure}[tb]
%	\centering
%	\begin{subfigure}[b]{0.45\linewidth}
%		\centering
%		\resizebox{\linewidth}{!}
%		{\input{Chapters/Chapter_4/Figures/20MHz_ModAEDS_NITF__30vs3000.tikz}}
%		\caption{$F_s = 20$ MHz}
%	\end{subfigure}
%	\begin{subfigure}[b]{0.45\linewidth}
%		\centering
%		\resizebox{\linewidth}{!}
%		{\input{Chapters/Chapter_4/Figures/100MHz_ModAEDS_NITF__30vs3000.tikz}}
%		\caption{$F_s = 100$ MHz}
%	\end{subfigure}
%	\begin{subfigure}[b]{0.45\linewidth}
%		\centering
%		\resizebox{\linewidth}{!}
%		{\input{Chapters/Chapter_4/Figures/150MHz_ModAEDS_NITF__30vs3000.tikz}}
%		\caption{$F_s = 150$ MHz}
%	\end{subfigure}
%	\begin{subfigure}[b]{0.45\linewidth}
%		\centering
%		\resizebox{\linewidth}{!}
%		{\input{Chapters/Chapter_4/Figures/200MHz_ModAEDS_NITF__30vs3000.tikz}}
%		\caption{$F_s = 200$ MHz}
%	\end{subfigure}
%	\caption{Results of the modAEDS algorithm with 250,000 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for transfer function set TF-2}
%	\label{fig:modAEDS_NITF__30v3000}
%\end{figure}
%\begin{figure}[tb]
%	\centering
%	\begin{subfigure}[b]{0.45\linewidth}
%		\centering
%		\resizebox{\linewidth}{!}
%		{\input{Chapters/Chapter_4/Figures/20MHz_ModAEDS_narrowband__30vs3000.tikz}}
%		\caption{$F_s = 20$ MHz}
%	\end{subfigure}
%	\begin{subfigure}[b]{0.45\linewidth}
%		\centering
%		\resizebox{\linewidth}{!}
%		{\input{Chapters/Chapter_4/Figures/100MHz_ModAEDS_narrowband__30vs3000.tikz}}
%		\caption{$F_s = 100$ MHz}
%	\end{subfigure}
%	\begin{subfigure}[b]{0.45\linewidth}
%		\centering
%		\resizebox{\linewidth}{!}
%		{\input{Chapters/Chapter_4/Figures/150MHz_ModAEDS_narrowband__30vs3000.tikz}}
%		\caption{$F_s = 150$ MHz}
%	\end{subfigure}
%		\begin{subfigure}[b]{0.45\linewidth}
%			\centering
%			\resizebox{\linewidth}{!}
%			{\input{Chapters/Chapter_4/Figures/200MHz_ModAEDS_narrowband__30vs3000.tikz}}
%			\caption{$F_s = 200$ MHz}
%		\end{subfigure}
%	\caption{Results of the modAEDS algorithm with 250,000 data samples, with SNR = 60 dB, at various sampling rates for narrrowband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for transfer function set TF-1}
%	\label{fig:modAEDS_narrowband_30v3000}
%\end{figure}
\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/100MHz_ModAED_Nless__30vs3000.tikz}}
		\caption{modAED}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/100MHz_ModAEDS_Nless__30vs3000.tikz}}
		\caption{modAEDS}
	\end{subfigure}
	\caption{Results of the modAED and modAEDS algorithms with 2,500 data samples, with SNR = 60 dB, for wideband simulation at 100 MHz sampling rate for transfer function set TF-1}
	\label{fig:modAEDvmodAEDS_Nless_30v3000}
\end{figure}
%\begin{figure}[tb]
%	\centering
%	\begin{subfigure}[b]{0.45\linewidth}
%		\centering
%		\resizebox{\linewidth}{!}
%		{\input{Chapters/Chapter_4/Figures/20MHz_ModAEDS_Nless__30vs3000.tikz}}
%		\caption{$F_s = 20$ MHz}
%	\end{subfigure}
%	\begin{subfigure}[b]{0.45\linewidth}
%		\centering
%		\resizebox{\linewidth}{!}
%		{\input{Chapters/Chapter_4/Figures/100MHz_ModAEDS_Nless__30vs3000.tikz}}
%		\caption{$F_s = 100$ MHz}
%	\end{subfigure}
%	\begin{subfigure}[b]{0.45\linewidth}
%		\centering
%		\resizebox{\linewidth}{!}
%		{\input{Chapters/Chapter_4/Figures/150MHz_ModAEDS_Nless__30vs3000.tikz}}
%		\caption{$F_s = 150$ MHz}
%	\end{subfigure}
%	\begin{subfigure}[b]{0.45\linewidth}
%		\centering
%		\resizebox{\linewidth}{!}
%		{\input{Chapters/Chapter_4/Figures/200MHz_ModAEDS_Nless__30vs3000.tikz}}
%		\caption{$F_s = 200$ MHz}
%	\end{subfigure}
%	\caption{Results of the modAEDS algorithm with 2,500 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for transfer function set TF-1}
%	\label{fig:modAEDS_Nless_30v3000}
%\end{figure}
\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/150MHz_ModAED_noisy__30vs3000.tikz}}
		\caption{modAED}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/150MHz_ModAEDS_noisy__30vs3000.tikz}}
		\caption{modAEDS}
	\end{subfigure}
	\caption{Results of the modAED and modAEDS algorithms with 250,000 data samples, with SNR = 20 dB, for wideband simulation at 150 MHz sampling rate for transfer function set TF-1}
	\label{fig:modAEDvmodAEDS_noisy_30v3000}
\end{figure}
%\begin{figure}[tb]
%	\centering
%	\begin{subfigure}[b]{0.45\linewidth}
%		\centering
%		\resizebox{\linewidth}{!}
%		{\input{Chapters/Chapter_4/Figures/20MHz_ModAEDS_noisy__30vs3000.tikz}}
%		\caption{$F_s = 20$ MHz}
%	\end{subfigure}
%	\begin{subfigure}[b]{0.45\linewidth}
%		\centering
%		\resizebox{\linewidth}{!}
%		{\input{Chapters/Chapter_4/Figures/100MHz_ModAEDS_noisy__30vs3000.tikz}}
%		\caption{$F_s = 100$ MHz}
%	\end{subfigure}
%	\begin{subfigure}[b]{0.45\linewidth}
%		\centering
%		\resizebox{\linewidth}{!}
%		{\input{Chapters/Chapter_4/Figures/150MHz_ModAEDS_noisy__30vs3000.tikz}}
%		\caption{$F_s = 150$ MHz}
%	\end{subfigure}
%	\begin{subfigure}[b]{0.45\linewidth}
%		\centering
%		\resizebox{\linewidth}{!}
%		{\input{Chapters/Chapter_4/Figures/200MHz_ModAEDS_noisy__30vs3000.tikz}}
%		\caption{$F_s = 200$ MHz}
%	\end{subfigure}
%	\caption{Results of the modAEDS algorithm with 250,000 data samples, with SNR = 20 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for transfer function set TF-1}
%	\label{fig:modAEDS_noisy_30v3000}
%\end{figure}
\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_ModAED_undEst_2__30vs3000.tikz}}
		\caption{modAED}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_4/Figures/200MHz_ModAEDS_undEst_2__30vs3000.tikz}}
		\caption{modAEDS}
	\end{subfigure}
	\caption{Results of the modAED and modAEDS algorithms with 250,000 data samples, with SNR = 60 dB, for wideband simulation at 100 MHz sampling rate for incorrect estimates of model order, $L = L/2$, for transfer function set TF-1}
	\label{fig:modAEDvmodAEDS_wrongModel_30v3000}
\end{figure}
%\begin{figure}[tb]
%	\centering
%	\begin{subfigure}[b]{0.45\linewidth}
%		\centering
%		\resizebox{\linewidth}{!}
%		{\input{Chapters/Chapter_4/Figures/200MHz_ModAEDS_undEst_2__30vs3000.tikz}}
%		\caption{$L = 100$}
%	\end{subfigure}
%	\begin{subfigure}[b]{0.45\linewidth}
%		\centering
%		\resizebox{\linewidth}{!}
%		{\input{Chapters/Chapter_4/Figures/200MHz_ModAEDS_undEst-10__30vs3000.tikz}}
%		\caption{$L=190$}
%	\end{subfigure}
%	\begin{subfigure}[b]{0.45\linewidth}
%		\centering
%		\resizebox{\linewidth}{!}
%		{\input{Chapters/Chapter_4/Figures/200MHz_ModAEDS_overEst_2__30vs3000.tikz}}
%		\caption{$L = 400$}
%	\end{subfigure}
%	\begin{subfigure}[b]{0.45\linewidth}
%		\centering
%		\resizebox{\linewidth}{!}
%		{\input{Chapters/Chapter_4/Figures/200MHz_ModAEDS_overEst-10__30vs3000.tikz}}
%		\caption{$L = 210$}
%	\end{subfigure}
%	\caption{Results of the modAEDS algorithm with 250,000 data samples, with SNR = 60 dB for wideband simulation at 200 MHz sampling rate for incorrect estimates of transfer function length, $L$, (a) $L/2$, (b) $L - 10$, (c) $L*2$, and (d) $L + 10$, for transfer function set TF-1}
%	\label{fig:modAEDS_wrongModel_30v3000}
%\end{figure}

\subsection{Modified AED with Sparsity} 
Some sources claim that a sparsity constraint is needed in order to accurately estimate the channels \cite{art:AdaptBESparse, art:IDinBDeconv, art:SparseBDeConv}.
Adding a sparsity constraint changes the objective function only slightly to
\begin{equation} \label{eq:modAEDS_objective}
    \begin{aligned}
        & \min_{{\mathbf{h}^{(n + 1)}}^\mathrm{H}}&  &||\mathbf{h}^{(n + 1)} - \mathbf{h}^{(n)}||_2^2 + \alpha||{\mathbf{h}^{(n + 1)}}^\mathrm{*}||_1& \\
        & \text{subject to}&  &{\mathbf{h}^{(n + 1)}}^\mathrm{H}\mathrm{x}_{r} = 0.&
    \end{aligned}
\end{equation}
Sparsity is typically enforced by utilizing an $L_1$-norm \cite{art:IDinBDeconv,art:AdaptiveTDEUsingFilterConstraints,thesis:SLvTDOA, art:SparseBDeConv, conf:ETDOARoomRefl}. 
The update equation is derived, as before, by first setting up the Lagrangian
\begin{equation} \label{eq:lagrangian_modAEDS}
    \mathcal{L} = {\mathbf{h}^{(n + 1)}}^\mathrm{H}{\mathbf{h}^{(n + 1)}} -
                  {\mathbf{h}^{(n + 1)}}^\mathrm{H}{\mathbf{h}^{(n)}} -
                  {\mathbf{h}^{(n)}}^\mathrm{H}{\mathbf{h}^{(n + 1)}} + 
                  {\mathbf{h}^{(n)}}^\mathrm{H}{\mathbf{h}^{(n)}} +
                  \alpha||{\mathbf{h}^{(n + 1)}}^\mathrm{H}||_1 +
                  \lambda{\mathbf{h}^{(n + 1)}}^\mathrm{H}\mathrm{x}_r.
\end{equation}

Taking the derivative of Equation (\ref{eq:lagrangian_modAEDS}) is slightly more complicated due to the $L_{\mathrm{1}}$ norm, as the $L_{\mathrm{1}}$ norm contains a discontinuity at 0. However, it is common practice to use a subgradient approximation \cite{web:subgradient} in place of the true derivative. The subgradient for the $L_{\mathrm{1}}$ norm in Equation  (\ref{eq:lagrangian_modAEDS}) is $\text{sign}(\mathbf{h}^{(n + 1)})$.  As the data is complex, it is more mathematically correct to isolate the phase of the transfer function estimates to determine which direction they are (positive or negative), $\text{sgn}(\mathbf{h}^{(n + 1)}) = \mathbf{e}^{j\angle\mathbf{h}^{(n + 1)}}$,
\begin{equation} \label{eq:modAEDS_derivative}
	\frac{d}{d{\mathbf{h}^{(n + 1)}}^*}\mathcal{L} = 
	\mathbf{h}^{(n + 1)} - 
	\mathbf{h}^{(n)} + 
	\alpha{\mathbf{e}^{j\angle\mathbf{h}^{(n + 1)}}} 
	+ \lambda\mathrm{x}_r
\end{equation}

An assumption is made in order to compute the transfer function,
as only a small change is assumed to happen each iteration,
$
    \text{sgn}(\mathbf{h}^{(n + 1)}) \approx \text{sgn}(\mathbf{h}^{(n)}).
$
This assumption allows simply solving for the update, $\mathbf{h}^{(n + 1)}$, without the complexity of an added sign term.

Using this approximation and assumption of the $L_{\mathrm{1}}$ norm, the update can be found by setting \ref{eq:modAEDS_derivative} equal to 0, and solving for the update,
\begin{equation} \label{eq:modAEDS_update_lambda}
       \mathbf{h}^{(n + 1)} =
       \mathbf{h}^{(n)} - 
       \alpha\mathbf{e}^{j\angle\mathbf{h}^{(n)}} -
       \lambda\mathrm{x}_r.
\end{equation}

The value of $\lambda$ is found, as before, by substituting the results of Equation (\ref{eq:modAEDS_update_lambda}) into the constraint listed in Equation (\ref{eq:modAEDS_objective}) resulting in a value similar to Equation (\ref{eq:AED_constraint}), with the sparsity constraint added in,
\begin{equation} \label{eq:modAEDS_constraint}
    \lambda = \frac{(\mathbf{h}^{(n)} - \alpha\mathbf{e}^{j\angle\mathbf{h}^{(n)}})^\mathrm{H}\mathrm{x}_r}{||
    \mathrm{x}_r||_2^2}.
\end{equation}

Replacing the value for $\lambda$ in Equation (\ref{eq:modAEDS_update_lambda}) with the results in Equation (\ref{eq:modAEDS_objective}) produces the update used in the sparse modAED (modAEDS) algorithm,
\begin{equation}
    \begin{aligned}
        \mathbf{h}^{(n+1)} = \mathbf{h}^{(n)}  + \alpha\mathbf{e}^{j\angle\mathbf{h}^\mathrm{(n)}} - \eta\frac{(\mathbf{h}^{(n)} + \alpha\mathbf{e}^{j\angle\mathbf{h}^\mathrm{(n)}})^\mathrm{H}\mathrm{x}_{r}}{||\mathrm{x}_{r}||_2^2}\mathrm{x}_{r}.
    \end{aligned}
\end{equation}
The complete derivation and code listing for the modAEDS algorithm are found in Appendices \ref{app:AEDS} and \ref{code:AEDS} respectively.


\subsection{Modified AED with Sparsity Algorithm Advantages, Implementations and Disadvantages}
This modAEDS algorithm produces reliable estimate that could often accurately estimate the delay between the receivers dependably, as can be seen in Figure \ref{fig:modAEDS_ideal__30v3000}.  However, the observable difference between the modAEDS (Figure \ref{fig:modAEDS_ideal__30v3000}) and the modAED (Figure \ref{fig:modAED_ideal_30v3000}) algorithms is minimal (if at all).

There is also not much (if any) difference between the performance of the ideal, and some of the nonideal scenarios (lower SNR, less data samples, and incorrect $L$) tested , and so the individual results are not shown, although Figures \ref{fig:modAEDvmodAEDS_Nless_30v3000} , \ref{fig:modAEDvmodAEDS_noisy_30v3000} and \ref{fig:modAEDvmodAEDS_wrongModel_30v3000} are shown to compare the results for both the modAED and modAEDS algorithms.
%in Figures \ref{fig:modAEDS_ideal__30v3000}, \ref{fig:modAEDS_Nless_30v3000}, \ref{fig:modAEDS_noisy_30v3000}, and \ref{fig:modAEDS_wrongModel_30v3000}. 
%Figure \ref{fig:modAEDS_NITF__30v3000} 
The modAEDS algorithm also does a decent job at estimating the transfer functions, even for the nonideal transfer function, TF-2, set, although it performs similarly to the modAED algorithm. 
Unfortunately, the narrowband simulation, still performs terribly, 
% as shown in Figure \ref{fig:modAEDS_narrowband_30v3000}
but slightly more sparse than the results for the modAED algorithm narrowband simulation shown in Figure \ref{fig:modAED_narrowband_30v3000}.

Note that this method is similar to the one explained in \cite{art:AdaptBESparse}.

%, even when the model order L was not exactly correct. However, the estimates produced by this method often still contained some spurious peaks, particularly with a poor model order estimate. 
%This modification fixes the random normalized results as is shown in Figure <PUT A FIGURE HERE>.

%Occasionally, spurious peaks are still produced, particularly with poor model order estimate
% 
%This motivates an algorithm that suppresses shifting as is explained in the next few chapters.
