\contentsline {chapter}{\textnormal {ABSTRACT}}{iii}{Doc-Start}
\contentsline {chapter}{\textnormal {PUBLIC ABSTRACT}}{v}{Doc-Start}
\contentsline {chapter}{\textnormal {\textnormal {ACKNOWLEDGMENTS}}}{vi}{chapter*.1}
\contentsline {chapter}{\textnormal {\textnormal {ACRONYMS}}}{x}{chapter*.3}
\contentsline {chapter}{\numberline {1}INTRODUCTION}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Thesis Outline}{2}{section.1.1}
\contentsline {chapter}{\numberline {2}LITERATURE REVIEW}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Source Localization}{3}{section.2.1}
\contentsline {subsubsection}{TOA, RSS, DOA}{3}{section*.4}
\contentsline {subsubsection}{TDOA}{5}{section*.5}
\contentsline {section}{\numberline {2.2}Time Delay Estimation and Blind Channel Identification Algorithms}{5}{section.2.2}
\contentsline {section}{\numberline {2.3}Cross-Relation Method}{7}{section.2.3}
\contentsline {chapter}{\numberline {3}PROBLEM OVERVIEW}{9}{chapter.3}
\contentsline {section}{\numberline {3.1}Objective}{9}{section.3.1}
\contentsline {section}{\numberline {3.2}Simulations}{10}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Narrowband vs Wideband}{10}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Coding Modifications}{11}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}Data Collection}{12}{section.3.3}
\contentsline {section}{\numberline {3.4}Data Sets}{13}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}USU Data Sets}{13}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}UMD Data Sets}{13}{subsection.3.4.2}
\contentsline {chapter}{\numberline {4}CROSS RELATION METHODS}{16}{chapter.4}
\contentsline {section}{\numberline {4.1}Least-Squares Blind Channel Identification according to Xu et. al}{16}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}System Model for the Least-Squares Approach}{16}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Complex Cross Relation}{16}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}A Least-Squares Approach According to Xu et. al}{19}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}LS Algorithm Advantages, Implementations and Disadvantages}{20}{subsection.4.1.4}
\contentsline {section}{\numberline {4.2}Adaptive Eigenvalue Decomposition}{23}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}System Model for the Modified Adaptive Eigenvalue Decomposition}{24}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Complex Conjugate Cross Relation}{26}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Modified Adaptive Eigenvalue Decomposition}{27}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Modified Adaptive Eigenvalue Decomposition Algorithm Advantages, Implementations and Disadvantages}{30}{subsection.4.2.4}
\contentsline {subsection}{\numberline {4.2.5}Modified AED with Sparsity}{34}{subsection.4.2.5}
\contentsline {subsection}{\numberline {4.2.6}Modified AED with Sparsity Algorithm Advantages, Implementations and Disadvantages}{37}{subsection.4.2.6}
\contentsline {section}{\numberline {4.3}Algorithm Testing and Results}{38}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Simulation}{38}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Real World Data Testing}{38}{subsection.4.3.2}
\contentsline {chapter}{\numberline {5}THE ADAPTIVE CROSS-CHANNEL WITH SPARSE SHIFT-SUPPRESSION (AXIS) METHOD}{39}{chapter.5}
\contentsline {section}{\numberline {5.1}AXIS Derivation}{39}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Algorithm Advantages, Implementations, and Modifications}{43}{subsection.5.1.1}
\contentsline {section}{\numberline {5.2}Variations}{46}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Varied AXIS}{46}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Dual Fixed-Peak AXIS}{48}{subsection.5.2.2}
\contentsline {section}{\numberline {5.3}Data Verification}{51}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Simulation}{51}{subsection.5.3.1}
\contentsline {subsubsection}{Wideband vs Narrowband}{51}{section*.38}
\contentsline {subsection}{\numberline {5.3.2}Observed Data}{51}{subsection.5.3.2}
\contentsline {subsubsection}{Wideband vs Narrowband}{51}{section*.39}
\contentsline {section}{\numberline {5.4}Issues with the Algorithms}{51}{section.5.4}
\contentsline {section}{\numberline {5.5}Possible Improvements}{52}{section.5.5}
\contentsline {chapter}{\numberline {6}THE BRUTE FORCE METHOD}{53}{chapter.6}
\contentsline {section}{\numberline {6.1}Brute Force Derivation}{53}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Algorithm Advantages, Implementations, and Modifications}{54}{subsection.6.1.1}
\contentsline {section}{\numberline {6.2}Variations}{54}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Varied Brute Force}{55}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}Dual Fixed-Peak Brute Force}{57}{subsection.6.2.2}
\contentsline {section}{\numberline {6.3}Data Verification}{62}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Simulation}{62}{subsection.6.3.1}
\contentsline {subsubsection}{Wideband vs Narrowband}{62}{section*.59}
\contentsline {subsection}{\numberline {6.3.2}Observed Data}{63}{subsection.6.3.2}
\contentsline {subsubsection}{Wideband vs Narrowband}{63}{section*.60}
\contentsline {section}{\numberline {6.4}Issues with the Algorithms}{63}{section.6.4}
\contentsline {section}{\numberline {6.5}Possible Improvements}{63}{section.6.5}
\contentsline {chapter}{\numberline {7}RESULTS}{74}{chapter.7}
\contentsline {section}{\numberline {7.1}Comparison of Algorithms}{74}{section.7.1}
\contentsline {section}{\numberline {7.2}Effects of Various Parameters}{74}{section.7.2}
\contentsline {subsection}{\numberline {7.2.1}Bandwidth}{74}{subsection.7.2.1}
\contentsline {subsection}{\numberline {7.2.2}Noise}{74}{subsection.7.2.2}
\contentsline {subsection}{\numberline {7.2.3}Sample Rate}{74}{subsection.7.2.3}
\contentsline {subsection}{\numberline {7.2.4}Number of Samples}{74}{subsection.7.2.4}
\contentsline {section}{\numberline {7.3}Observations with Real Data}{74}{section.7.3}
\contentsline {chapter}{\numberline {8}CONCLUSION}{75}{chapter.8}
\contentsline {section}{\numberline {8.1}Future Work}{75}{section.8.1}
\contentsline {chapter}{REFERENCES}{76}{section.8.2}
\contentsline {chapter}{\textnormal {APPENDICES}}{79}{chapter.9}
\contentsline {section}{A\hspace {12pt} Math Derivations and Notation}{80}{appendix.A}
\contentsline {subsection}{\numberline {A.1}Notation}{80}{subsection.A.0.1}
\contentsline {subsubsection}{Multiple Receiver Format}{82}{section*.62}
\contentsline {subsection}{\numberline {A.2}Derivations}{83}{subsection.A.0.2}
\contentsline {subsubsection}{Modified Adaptive Eigenvalue Decomposition}{84}{section*.63}
\contentsline {subsubsection}{Modified Adaptive Eigenvalue Decomposition with Sparsity}{85}{section*.64}
\contentsline {subsubsection}{Adaptive Cross-Channel Identification with Sparse Shift-Suppression}{86}{section*.65}
\contentsline {subsubsection}{Varied AXIS}{88}{section*.66}
\contentsline {subsubsection}{Dual-Fixed Peak AXIS}{90}{section*.67}
\contentsline {subsubsection}{Brute Force}{94}{section*.68}
\contentsline {subsubsection}{Varied Brute Force}{95}{section*.69}
\contentsline {subsubsection}{Dual Fixed-Peak Brute Force}{96}{section*.70}
\contentsline {subsubsection}{Single Constraint}{98}{section*.71}
\contentsline {subsubsection}{Varied Single Constraint}{100}{section*.72}
\contentsline {subsubsection}{Dual Fixed-Peak Single Constraint}{102}{section*.73}
\contentsline {section}{B\hspace {12pt} Code Listings}{104}{appendix.B}
\contentsline {subsection}{\numberline {B.1}Modifications}{104}{subsection.B.0.1}
\contentsline {subsection}{\numberline {B.2}The Least Squares Approach According to Xu et. al.}{107}{subsection.B.0.2}
\contentsline {subsection}{\numberline {B.3}The Adaptive Eigenvalue Decomposition}{108}{subsection.B.0.3}
\contentsline {subsection}{\numberline {B.4}The Modified Adaptive Eigenvalue Decomposition}{110}{subsection.B.0.4}
\contentsline {subsection}{\numberline {B.5}The Modified Adaptive Eigenvalue Decomposition with Sparsity}{112}{subsection.B.0.5}
\contentsline {subsection}{\numberline {B.6}The Adaptive Cross-Channel Identification with Sparse Shift-Suppression}{114}{subsection.B.0.6}
\contentsline {subsection}{\numberline {B.7}The Varied Adaptive Cross-Channel Identification with Sparse \\ Shift-Suppression}{116}{subsection.B.0.7}
\contentsline {subsection}{\numberline {B.8}The Dual Fixed-Peak Adaptive Cross-Channel Identification with Sparse Shift-Suppression}{119}{subsection.B.0.8}
\contentsline {subsection}{\numberline {B.9}The Brute Force Method}{122}{subsection.B.0.9}
\contentsline {subsection}{\numberline {B.10}The Varied Brute Force Method}{124}{subsection.B.0.10}
\contentsline {subsection}{\numberline {B.11}The Dual Fixed-Peak Brute Force Method}{126}{subsection.B.0.11}
\contentsline {subsection}{\numberline {B.12}The Single Constraint Method}{129}{subsection.B.0.12}
\contentsline {subsection}{\numberline {B.13}The Varied Single Constraint Method}{131}{subsection.B.0.13}
\contentsline {subsection}{\numberline {B.14}The Dual Fixed-Peak Single Constraint Method}{134}{subsection.B.0.14}
\contentsline {subsection}{\numberline {B.15}The Cross Correlation Function}{137}{subsection.B.0.15}
\contentsline {subsection}{\numberline {B.16}The Overhead File to Call the Algorithms}{138}{subsection.B.0.16}
\contentsline {section}{C\hspace {12pt} Further Results}{139}{appendix.C}
