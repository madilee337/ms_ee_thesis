\contentsline {chapter}{\textnormal {ABSTRACT}}{iii}{Doc-Start}
\contentsline {chapter}{\textnormal {PUBLIC ABSTRACT}}{v}{Doc-Start}
\contentsline {chapter}{\textnormal {\textnormal {ACKNOWLEDGMENTS}}}{vi}{chapter*.1}
\contentsline {chapter}{\textnormal {LIST OF TABLES}}{x}{chapter*.3}
\contentsline {chapter}{\textnormal {LIST OF FIGURES}}{xiii}{chapter*.4}
\contentsline {chapter}{\textnormal {\textnormal {ACRONYMS}}}{xxi}{chapter*.5}
\contentsline {chapter}{\numberline {1}INTRODUCTION}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Thesis Outline}{2}{section.1.1}
\contentsline {chapter}{\numberline {2}LITERATURE REVIEW}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Source Localization}{3}{section.2.1}
\contentsline {subsubsection}{TOA, RSS, DOA, FDOA}{3}{section*.6}
\contentsline {subsubsection}{TDOA}{5}{section*.7}
\contentsline {section}{\numberline {2.2}Time Delay Estimation and Blind Channel Identification Algorithms}{6}{section.2.2}
\contentsline {section}{\numberline {2.3}Cross-Relation Method}{8}{section.2.3}
\contentsline {chapter}{\numberline {3}PROBLEM OVERVIEW}{9}{chapter.3}
\contentsline {section}{\numberline {3.1}Objective}{9}{section.3.1}
\contentsline {section}{\numberline {3.2}The Multipath Problem}{10}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}TDOA Calculations}{11}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Time Delay Estimation Methods}{14}{subsection.3.2.2}
\contentsline {subsubsection}{Generalized Cross-Correlation TDOA Approach}{15}{section*.12}
\contentsline {subsubsection}{Other Algorithms}{18}{section*.13}
\contentsline {section}{\numberline {3.3}Simulations}{19}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Different Simulation Parameters}{23}{subsection.3.3.1}
\contentsline {subsubsection}{Signal Bandwidth}{23}{section*.17}
\contentsline {subsubsection}{Noise}{24}{section*.19}
\contentsline {subsubsection}{Sample Size}{24}{section*.21}
\contentsline {subsubsection}{Model Order Estimates}{24}{section*.22}
\contentsline {subsubsection}{The Ideal Simulation}{25}{section*.24}
\contentsline {subsection}{\numberline {3.3.2}Coding Modifications}{25}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Results Explanation}{26}{subsection.3.3.3}
\contentsline {section}{\numberline {3.4}Data Collection}{28}{section.3.4}
\contentsline {section}{\numberline {3.5}Data Sets}{30}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}USU Data Sets}{30}{subsection.3.5.1}
\contentsline {chapter}{\numberline {4}PRE-EXISTING CROSS RELATION METHODS}{31}{chapter.4}
\contentsline {section}{\numberline {4.1}Pre-Existing Algorithms}{31}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}System Model}{31}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Cross Relation}{32}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}A Least-Squares Approach According to Xu et al.}{33}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}Adaptive Eigenvalue Decomposition}{34}{subsection.4.1.4}
\contentsline {subsection}{\numberline {4.1.5}Pre-Existing Algorithms Implementations, Advantages, and Disadvantages}{35}{subsection.4.1.5}
\contentsline {section}{\numberline {4.2}Modified Algorithms}{42}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Modified System Model}{43}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Complex Cross Relation}{46}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Modified Adaptive Eigenvalue Decomposition}{49}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Modified AED with Sparsity}{50}{subsection.4.2.4}
\contentsline {subsection}{\numberline {4.2.5}Modified Algorithms Implementations, Advantages, Disadvantages}{52}{subsection.4.2.5}
\contentsline {section}{\numberline {4.3}Algorithm Testing and Results}{56}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Simulation}{57}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Real World Data Testing}{72}{subsection.4.3.2}
\contentsline {chapter}{\numberline {5}THE ADAPTIVE CROSS-CHANNEL WITH SPARSE SHIFT-SUPPRESSION (AXIS) METHOD}{73}{chapter.5}
\contentsline {section}{\numberline {5.1}AXIS Derivation}{73}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Algorithm Advantages, Implementations, and Modifications}{74}{subsection.5.1.1}
\contentsline {section}{\numberline {5.2}Variations}{78}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Varied AXIS}{79}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Dual Fixed-Peak AXIS}{82}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Non-Sparse AXIS}{85}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}Single Constraint AXIS (SC)}{87}{subsection.5.2.4}
\contentsline {subsubsection}{Variations of the SC Algorithm}{89}{section*.70}
\contentsline {paragraph}{\textbf {Varied SC (VSC)}}{90}{section*.71}
\contentsline {paragraph}{\textbf {Dual Fixed-Peak SC (DFPSC)}}{90}{figure.caption.74}
\contentsline {paragraph}{\textbf {Non-Sparse SC (NSSC)}}{92}{section*.76}
\contentsline {section}{\numberline {5.3}Data Verification}{94}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Simulation}{94}{subsection.5.3.1}
\contentsline {subsubsection}{``Ideal'' Simulation}{94}{section*.77}
\contentsline {subsubsection}{Signal Bandwidth}{94}{section*.78}
\contentsline {subsubsection}{Noise}{95}{section*.79}
\contentsline {subsubsection}{Sample Size}{95}{section*.80}
\contentsline {subsubsection}{Model Order}{95}{section*.81}
\contentsline {subsubsection}{Initial Estimates}{96}{section*.82}
\contentsline {subsection}{\numberline {5.3.2}Observed Data}{115}{subsection.5.3.2}
\contentsline {section}{\numberline {5.4}Algorithm Comparison}{115}{section.5.4}
\contentsline {chapter}{\numberline {6}THE ``BRUTE FORCE" METHOD}{116}{chapter.6}
\contentsline {section}{\numberline {6.1}Brute Force Derivation}{116}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Algorithm Advantages, Disadvantages and Implementation}{117}{subsection.6.1.1}
\contentsline {section}{\numberline {6.2}Variations}{118}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Adaptive Brute Force}{119}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}Varied Adaptive Brute Force}{123}{subsection.6.2.2}
\contentsline {subsection}{\numberline {6.2.3}Dual Fixed-Peak Brute Force}{125}{subsection.6.2.3}
\contentsline {section}{\numberline {6.3}Data Verification}{133}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Simulation}{133}{subsection.6.3.1}
\contentsline {subsubsection}{``Ideal'' Simulation}{134}{section*.119}
\contentsline {subsubsection}{Signal Bandwidth}{135}{section*.120}
\contentsline {subsubsection}{Noise}{136}{section*.121}
\contentsline {subsubsection}{Sample Size}{136}{section*.122}
\contentsline {subsubsection}{Model Order}{136}{section*.123}
\contentsline {subsubsection}{Initial Estimates}{137}{section*.124}
\contentsline {section}{\numberline {6.4}Algorithm Comparison}{147}{section.6.4}
\contentsline {chapter}{\numberline {7}RESULTS}{148}{chapter.7}
\contentsline {section}{\numberline {7.1}Comparison of Algorithms}{148}{section.7.1}
\contentsline {subsection}{\numberline {7.1.1}Algorithm Complexity}{148}{subsection.7.1.1}
\contentsline {subsection}{\numberline {7.1.2}Simulation Results}{150}{subsection.7.1.2}
\contentsline {subsubsection}{MATLAB Conditions}{150}{section*.136}
\contentsline {subsubsection}{Results}{151}{section*.137}
\contentsline {paragraph}{\textbf {\textit {Effects of Various Parameters:}}}{151}{section*.138}
\contentsline {subparagraph}{\relax $\@@underline {\hbox {Signal Bandwidth and Sample Rate:}}\mathsurround \z@ $\relax }{151}{section*.139}
\contentsline {subparagraph}{\relax $\@@underline {\hbox {Noise:}}\mathsurround \z@ $\relax }{152}{section*.140}
\contentsline {subparagraph}{\relax $\@@underline {\hbox {Sample Size:}}\mathsurround \z@ $\relax }{152}{section*.141}
\contentsline {subparagraph}{\relax $\@@underline {\hbox {Channel Order:}}\mathsurround \z@ $\relax }{153}{section*.142}
\contentsline {subparagraph}{\relax $\@@underline {\hbox {Initial Channel Estimates:}}\mathsurround \z@ $\relax }{153}{section*.143}
\contentsline {subsection}{\numberline {7.1.3}Results with Real Observed Data}{153}{subsection.7.1.3}
\contentsline {subsubsection}{Testing Conditions}{153}{section*.144}
\contentsline {subsubsection}{Results}{154}{section*.145}
\contentsline {section}{\numberline {7.2}Final Observations}{154}{section.7.2}
\contentsline {chapter}{\numberline {8}CONCLUSION}{155}{chapter.8}
\contentsline {section}{\numberline {8.1}Research}{155}{section.8.1}
\contentsline {section}{\numberline {8.2}Future Work}{155}{section.8.2}
\contentsline {chapter}{REFERENCES}{157}{section.8.3}
\contentsline {chapter}{\textnormal {APPENDICES}}{162}{chapter.9}
\contentsline {section}{A\hspace {12pt} Math Derivations and Notation}{163}{appendix.A}
\contentsline {subsection}{\numberline {A.1}Notation}{163}{subsection.A.0.1}
\contentsline {subsubsection}{Multiple Receiver Format}{166}{section*.147}
\contentsline {subsection}{\numberline {A.2}Derivations}{167}{subsection.A.0.2}
\contentsline {subsubsection}{Modified Adaptive Eigenvalue Decomposition}{168}{section*.148}
\contentsline {subsubsection}{Modified Adaptive Eigenvalue Decomposition with Sparsity}{169}{section*.149}
\contentsline {subsubsection}{Adaptive Cross-Channel Identification with Sparse Shift-Suppression}{170}{section*.150}
\contentsline {subsubsection}{Varied AXIS}{172}{section*.151}
\contentsline {subsubsection}{Dual-Fixed Peak AXIS}{174}{section*.152}
\contentsline {subsubsection}{Single Constraint}{177}{section*.153}
\contentsline {subsubsection}{Varied Single Constraint}{179}{section*.154}
\contentsline {subsubsection}{Dual Fixed-Peak Single Constraint}{181}{section*.155}
\contentsline {subsubsection}{Brute Force}{183}{section*.156}
\contentsline {subsubsection}{Varied Brute Force}{184}{section*.157}
\contentsline {subsubsection}{Dual Fixed-Peak Brute Force}{185}{section*.158}
\contentsline {section}{B\hspace {12pt} Code Listings}{187}{appendix.B}
\contentsline {subsection}{\numberline {B.1}Modifications}{187}{subsection.B.0.1}
\contentsline {subsection}{\numberline {B.2}The Least Squares (SVD) Approach According to Xu et al.}{190}{subsection.B.0.2}
\contentsline {subsection}{\numberline {B.3}The Adaptive Eigenvalue Decomposition}{191}{subsection.B.0.3}
\contentsline {subsection}{\numberline {B.4}The Modified Adaptive Eigenvalue Decomposition}{193}{subsection.B.0.4}
\contentsline {subsection}{\numberline {B.5}The Modified Adaptive Eigenvalue Decomposition with Sparsity}{195}{subsection.B.0.5}
\contentsline {subsection}{\numberline {B.6}The Adaptive Cross-Channel Identification with Sparse Shift-Suppression}{197}{subsection.B.0.6}
\contentsline {subsection}{\numberline {B.7}The Varied Adaptive Cross-Channel Identification with Sparse Shift-Suppression}{199}{subsection.B.0.7}
\contentsline {subsection}{\numberline {B.8}The Dual Fixed-Peak Adaptive Cross-Channel Identification with Sparse Shift-Suppression}{202}{subsection.B.0.8}
\contentsline {subsection}{\numberline {B.9}The Single Constraint Method}{205}{subsection.B.0.9}
\contentsline {subsection}{\numberline {B.10}The Varied Single Constraint Method}{207}{subsection.B.0.10}
\contentsline {subsection}{\numberline {B.11}The Dual Fixed-Peak Single Constraint Method}{210}{subsection.B.0.11}
\contentsline {subsection}{\numberline {B.12}The Brute Force Method}{213}{subsection.B.0.12}
\contentsline {subsection}{\numberline {B.13}The Varied Brute Force Method}{215}{subsection.B.0.13}
\contentsline {subsection}{\numberline {B.14}The Dual Fixed-Peak Brute Force Method}{217}{subsection.B.0.14}
\contentsline {subsection}{\numberline {B.15}The Cross Correlation Function}{220}{subsection.B.0.15}
\contentsline {subsection}{\numberline {B.16}The Overhead File to Call the Algorithms}{221}{subsection.B.0.16}
\contentsline {section}{C\hspace {12pt} Oversized Figures}{232}{appendix.C}
\contentsline {section}{D\hspace {12pt} Further Results}{255}{appendix.D}
\contentsline {subsection}{\numberline {D.1}Simulation}{255}{subsection.D.0.1}
