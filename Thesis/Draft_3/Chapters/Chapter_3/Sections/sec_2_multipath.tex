\section{The Multipath Problem}
Source localization techniques tend to rely on LOS, or rather a direct path for accurate results \cite{art:InnovIndoorGeolocation,book:sourceLocal}. Ideally, in free space, when a signal is transmitted, a single peak is received at each transmitter. There should be a relative time delay between these peaks which corresponds to the distance between the receivers. In enclosed environments, reflections, refractions and general distortion can occur when the transmitted waves interact with walls, furniture and other obstructions. Due to this interaction, multiple attenuated and delayed versions of the signal can reach each transmitter. Thus the resulting signal should contain the direct path plus multiple attenuated and delayed paths. This is referred to as the multipath problem \cite{art:MLEofTDinMulti}.
\begin{figure}[t]
	\centering
	\resizebox{\linewidth}{!}{\input{Chapters/Chapter_3/Figures/trans_rec_sim_loc_test_1.tikz}}
	\caption{An example of the multipath problem, where each color of line denotes a different path from the transmitter to a receiver, and the black lines denote walls. The solid line paths are possible reflective paths, and the dashed lines are direct paths. }\label{fig:simEnv_rec_trans}
\end{figure}

As mentioned previously, most source localization techniques rely on the assumption that a direct path exists, and is received. As can be seen in Figure \ref{fig:simEnv_rec_trans}, a direct path may or may not be received at the receiver. The case that a direct path is not received is referred to as no line of sight (NLOS) conditions. In NLOS conditions, the received signal must cover more distance to reach the receiver, resulting in larger estimation errors \cite{book:sourceLocal,art:InnovIndoorGeolocation,art:MLEofTDinMulti,art:ImpSourceLocal}. %``In NLOS propagation conditions, the received signal will travel longer distances...this results in...large localization error[s]"\cite{book:sourceLocal}.  
When NLOS paths are received, it is recommended by some sources, to identify and remove the NLOS measurements from the localization process. This is because NLOS paths can cause issues, making them undesirable \cite{book:sourceLocal,art:MLEofTDinMulti,thesis:Geolocation}. %NLOS conditions are challenging to determine, involve more assumptions, can be time consuming, and may need more receiver nodes to be  
Identifying NLOS conditions presents unique challenges which are too involved to be included in this research 
\cite{art:ImpSourceLocal,book:sourceLocal,thesis:Geolocation}. 

Ideally, the first peak received is the largest peak, as it signifies the shortest path the transmitted signal took from transmitter to receiver, i.e. the direct path. However due to the wave interactions, the signal can either be amplified or attenuated in its path to, and at the receiver. This means that the first peak may not be the largest peak. First peaks are challenging to distinguish as they could be distortions from side lobes \cite{art:MLEofTDinMulti,book:AcousticMIMOSigProc}.
 
\subsection{TDOA Calculations}
TDOA uses intersecting hyperbolae to show the possible locations of the transmitter, with the receiver positions corresponding to the foci points. TDOA measurements present a challenge due to high nonlinearity. Nonlinearity can occur not only when computing the hyperbolae, but also with respect to the relationship between the source location and the data. this results in a nonconvex maximum likelihood (ML) function  There have been multiple approaches proposed for solving the TDOA equations.

To solve the ML function, the equations can be modified and a ML method can be used. In ideal situations, this can prove optimal. However, it does not have a general closed form solution, can be computationally expensive and can experience convergence difficulties. Closed form solutions have been proposed, but are suboptimal, and are limited by the amount of receivers, location of the receivers, and/or the signal to noise ratio (SNR) \cite{conf:geoTDOAHyperbolic,thesis:Geolocation,conf:locMovTDOA_FDOA,art:SimpEffEstHyperbolicLoc,conf:iSourceLocTDOA_FDOA}. 

One common way is to linearize the equations using a Taylor series approximation. This approach to solving the TDOA equations in popular as it is simple, involving an iterative calculation. However, an initial estimate for the transmitter location is needed for calculation, and there is no guarantee of convergence, especially if little to nothing is known about the transmitter \cite{conf:geoTDOAHyperbolic,art:SimpEffEstHyperbolicLoc,thesis:Geolocation,conf:locMovTDOA_FDOA,conf:iSourceLocTDOA_FDOA}.

The third way typically used is a geometric way, using hyperbolic asymptotes. This way is not as computationally intensive, using simplified linear equations corresponding to the hyperbolic asymptotes. Unfortunately this way tends to be  less accurate \cite{conf:geoTDOAHyperbolic,thesis:Geolocation,conf:geoTDOA_FDOA}.  

A less common way, referred to as a grid-search method or look-up-table (LUT) method, is used for a pictoral representation of the likelihood of the transmitter location. Although computationally expensive, there is no need to deal with the nonlinear TDOA  computations. The LUT method computes a type of heat map which shows a rough probability likelihood of the location of the transmitter. This method is used in this research for the probability aspect.  
%The GCC shows the relative delay between transmitters. A simple way to convert the GCC measurements to a TDOA approach is to use a type of look-up-table (LUT) method. The LUT method described here is used for pictoral representations of the TDOA method. 
%The LUT method is a type of heat map  which shows a rough probability likelihood without some of the nonlinear computations TDOA is typically calculated with.  

Prior to computations, the area over which the locator may be present is divided into a grid or array pattern, as shown in Figure \ref{fig:simEnv_TDOA}. The method can be simply explained in four steps.
\begin{figure}[t]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}{\input{Chapters/Chapter_3/Figures/gridFig_TDOA_1.tikz}}
		\caption{Simulation environment with two receivers and test point, with an example grid overlayed.}
		\label{fig:CC_a}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}{\input{Chapters/Chapter_3/Figures/CC_40MHz_seed0_limited.tikz}}
		\caption{An example of a time likelihood curve via the magnitude plot for the CC for channels 1 and 2.}
		\label{fig:CC_b}
	\end{subfigure}
	\caption{Simple pictoral TDOA method.}
	\label{fig:simEnv_TDOA}
\end{figure}
Cycling through the squares on the grid, the following steps must be taken:
\begin{enumerate}
	\item Calculate the distance from the test point square to the receivers as shown in Figure (\ref{fig:CC_a}). This is done using the Euclidean distance
	\begin{equation}
		\begin{aligned}
		d_1 &= \sqrt{(x_t - x_1)^2 + (y_t - y_1)^2}, \\
		d_2 &=\sqrt{(x_t - x_2)^2 + (y_t - y_2)^2},
		\end{aligned}
	\end{equation}
	where $(x_t, y_t)$ denote the coordinates for the test point, and  $(x_1, y_1)$ and $(x_2, y_2)$ the coordinates for receivers 1 and 2 respectively.
	\item Determine the relative difference from the distance measurements in step 1,
	\begin{equation} \label{eq:distCalc}
		\Delta d_{1,2} = d_1 - d_2.
	\end{equation}
	\item Calculate the time difference using the distance speed time formula,
	\begin{equation}
		\Delta t = \Delta d_{1,2}\cdot\frac{F_s}{c},
	\end{equation}
	where $F_s$ is the sample rate associated with the impulse response, and c is the speed of light, approximately $3 \times 10^8$ m/s. Note that $\Delta t$ corresponds to values on the x-axis for a time likelihood plot, such as the CC magnitude plot, as shown in Figure (\ref{fig:CC_b}). 
	\item  Each square in the grid has a corresponding heatmap value that can be found from the time likelihood plot. The heatmap value is the CC magnitude that corresponds the delay found in step 3. 
\end{enumerate}
The values are quantized and then color-coded. This method shows the most likely place for the transmitter, as well a level of uncertainty. The results of the example situation are shown in Figure \ref{fig:CC_res}, where a brighter color indicates a higher likelihood of the transmitter being at that location.
\begin{figure}[t]
		\centering
		\resizebox{\linewidth}{!}{\input{Chapters/Chapter_3/Figures/xcorr_250e3_visual_results.tikz}}
		\caption{The results of CC TDOA using the LUT method for 250,000 data samples, and two receivers.}\label{fig:CC_res}
	\end{figure}
\subsection{Time Delay Estimation Methods}
In order to compute TDOA, a time delay estimation (TDE) is needed. There are two ways that the TDE are computed in this thesis. The first method is looking at the relative delay between channels. The second is using the time-lag which maximizes the cross-correlation. 

As the first peaks are more difficult to discern, the relative delay between channels is estimated using the greatest peaks between transfer functions, with the assumption that they are the first peaks. An example of delay estimation is shown in Figure \ref{fig:peak_comparison}.
\begin{figure}[htb]
	\centering
	\resizebox{12.5cm}{!}{\input{Chapters/Chapter_3/Figures/peak_comparison_wBracket.tikz}}
	\caption{An example of the normalized delay between two channels,  $\mathbf{h}_1$ and $\mathbf{h}_2$.}
	\label{fig:peak_comparison}
\end{figure} 
\noindent The relative time delay between channel 1 and channel 2 in Figure \ref{fig:peak_comparison} can be explained mathematically as
\begin{equation} \label{eq:tau}
\tau_{1,2} = \tau_1 - \tau_2,
\end{equation}
\noindent where $\tau_i$ is the delay estimated from the transmitter to the $i^{\text{th}}$ receiver.  
% May not need to mention the wavelength make sure to differentiate between lambda (wavelength) and lambda (lagrange multiplier)
Distance is computed using the wavelength\footnote{Note that $\lambda$ here denotes wavelength, but in the following chapters, $\lambda$ denotes a Lagrange muliplier.} and the delay from (\ref{eq:tau}),
\begin{equation} \label{eq:dist}
\begin{split}
d_{1, 2} &= \tau_{1, 2} \cdot \frac{c}{F_s}\\
&= \tau_{1, 2} \cdot \lambda.
\end{split}	
\end{equation} 

Computing the cross-correlation is explained in detail below. But as was shown in Figure \ref{fig:CC_b}, the relative time delay corresponds to the x-value of the largest peak. 

It should also be noted that all of the calculations in this thesis are discrete.                       
\subsubsection{Generalized Cross-Correlation TDOA Approach}\label{sec:GCC}
The Generalized Cross Correlation (GCC) algorithm computes the time-lag that maximizes the cross-correlation (CC). The CC can be expressed as 
\begin{equation} \label{eq:CC}
\begin{split}
\hat{\tau} 		
&=\argmax_\tau\mathrm{R}_{{{x}_k},{{x}_l}}(\tau) \\
&= \argmax_\tau{E[{{x}_k}(n){{x}_l}(n + \tau_{k,l})]}.
\end{split}
\end{equation} 
\noindent In (\ref{eq:CC}), $x_k(n)$ and $x_l(n)$ represent received signals of the form
\begin{equation}
\begin{aligned}
{{x}_k(n)} &= h_k(n)*s(n - \tau_k) + w_k(n) \\
{{x}_l(n)} &= h_l(n)*s(n - \tau_l) + w_l(n),
\end{aligned} 
\end{equation}
\noindent where $h_p(n)$ is the impulse response, $s(n-\tau_p)$ is the signal and $w_p(n)$ is the additive noise at the $n^{\text{th}}$ sample for $p = k, l$.
The expectation for the CC\footnote{Note that most versions of the CC functions involve a scaling value \cite{book:AcousticMIMOSigProc,conf:MAPEofTD,art:GCC,art:AEDDecomp}, but the default for the \textit{xcorr} function in MATLAB, which is used as the baseline for comparing the results of the algorithms researched in this thesis, does not \cite{web:CC}.} can be approximated as
\begin{equation}
\hat{\mathrm{R}}_{{{x}_k},{{x}_l}}(\tau) = \sum\limits_{n = 0}^{N - |\tau|-1}x_k(n)x^*_l(n + \tau),
\end{equation}
where $N$ is the total number of samples \cite{book:ProbRVandRP,conf:MAPEofTD,book:AcousticMIMOSigProc,art:GCC,art:ResearchTDE,thesis:Geolocation}.
However it is common practice to convert from the time (sample) domain to the frequency domain (using the Fast Fourier Transform (FFT), and inverse FFT (IFFT) respectively) for the CC computation as it can be implemented more efficiently. Converting it to the frequency domain is referred to as the cross-spectrum
\begin{equation} \label{eq:freq_CC}
\hat{\mathrm{R}}_{{{x}_k},{{x}_l}}(\tau) = \frac{1}{2N}\sum\limits_{v = 0}^{N - 1}X_k(f_v)X^*_l(f_v) e^{j 2\pi f_v\tau},
\end{equation}
\noindent where $f$ is the frequency in Hertz (Hz), and 
\begin{equation}
X_p(f) = \sum\limits_{u= 0}^{N - 1}x_p(u)e^{-j2\pi f_v u
}
\end{equation}
\cite{art:GCC,art:AEDDecomp,conf:AED,conf:MAPEofTD,book:AcousticMIMOSigProc,thesis:SLvTDOA}.

The GCC is a more generalized version of the CC, weighting the cross-spectrum estimate. The weighting function should be chosen such that the CC function results in a sharp peak at the time delay \cite{art:GCC,conf:MAPEofTD,book:AcousticMIMOSigProc}. The common equation used to portray the GCC is
\begin{equation}
\hat{\mathrm{R}}_{{{x}_k},{{x}_l}}(\tau) = \sum\limits_{v = 0}^{N - 1}\Phi(f_v)X_k(f_v)X^*_l(f_v) e^{j 2\pi f_v\tau},
\end{equation}
where $\Phi(f_v)$ is a weighting function, which is determined by the application. There are many common GCC methods including 
\begin{itemize}
	\item the classical CC (CCC) method, shown in (\ref{eq:freq_CC}), \cite{book:AcousticMIMOSigProc,art:GCC,conf:AED,art:AEDDecomp,art:ResearchTDE,thesis:SLvTDOA,thesis:Geolocation} 
	\item the phase transform (PHAT) \cite{conf:AED,art:AdaptiveTDEUsingFilterConstraints,art:BSN_Chan,art:AEDDecomp,art:GCC,conf:MAPEofTD,book:AcousticMIMOSigProc}, 
	\item the maximum likelihood (ML) method, sometimes referred to as Hannan-Thomson (HT) method \cite{art:AEDDecomp,art:GCC,art:MLEofTDinMulti,conf:MAPEofTD,book:AcousticMIMOSigProc,thesis:Geolocation}, 
	\item the smoothed coherence transform (SCOT) method \cite{art:AEDDecomp,art:GCC,book:AcousticMIMOSigProc} , 
	\item the Eckart filter method \cite{art:GCC,book:AcousticMIMOSigProc}, 
	\item the Roth processor method \cite{art:GCC}, and
	\item the maximum \textit{a posteriori} (MAP) method \cite{conf:MAPEofTD,book:moon}.
\end{itemize}
Of the common methods, the CCC, PHAT and ML GCC methods are most used as can be seen from their collective sources. 

The CCC method is typically used for its straight forward simple approach, however it may lead to a large bias in the estimate \cite{thesis:SLvTDOA,art:GCC,conf:AED}. The PHAT method was created to avoid the delta function spreading that was observed with both the SCOT and Roth methods, although some spreading may still occur when nonideal situations. It is to be used ``ad hoc", and can enhance small errors, have erratic phase  estimates, and may need additional weighting to combat signal power, \cite{art:GCC,conf:AED,conf:MAPEofTD}. The ML method is asymptotically optimal to achieve the Cramer Rao lower bound (CRLB), but in order to achieve optimality, the sample space needs to be large, the environment free of multipath, with little to no noise, and the spectra of the signals, and attenutaion factors must be known \textit{a priori} \cite{thesis:SLvTDOA, art:GCC,art:AEDDecomp,book:AcousticMIMOSigProc}. In general, although GCC approaches are commonly used for TDOA they still struggle with the presence of multipath \cite{art:BSN_Chan,thesis:SLvTDOA,conf:AED,art:AEDDecomp,art:ResearchTDE} In multipath environments, more peaks are likely to occur, making the delay peak spread, and can even become distorted by merging with other peaks \cite{art:GCC}. 

As most of these methods require more information known prior to processing, only the CCC method is used in this thesis as a baseline to compare the algorithms that make up this research. 
\subsubsection{Other Algorithms}
The majority of this thesis is devoted to the derivation and testing of several cross-relation (CR) algorithms. A few of the algorithms discussed offer a brief explanation of preexisting work. And the rest are new algorithms that attempt to use channel identification to improve performance in the presence of multipath. 

The preexisting work discussed in this thesis include the Adaptive Eigenvalue Decomposition \cite{art:AEDDecomp,conf:AED,book:AcousticMIMOSigProc}, and the channel identification algorithm proposed by Xu et al. \cite{art:Det_BID_Multi-FIRSystems, art:LS_Approach_BCID}, which is referred to as the SVD algorithm in this thesis to differentiate it from the other algorithms. These explanations can be found in Chapter \ref{ch:4}.

There are eight original algorithms and modified verions that are introduced by this thesis including:
\begin{itemize}
	\item Modified Adaptive Eigenvalue Decomposition (ModAED), found in Chapter \ref{ch:4},
	\item Sparse Modified Adaptive Eigenvalue Decomposition (ModAEDS), found in Chapter \ref{ch:4},
	\item Adaptive Cross Channel Identification with Sparse Shift-Suppression (AXIS), found in Chapter \ref{ch:5},
	\item Varied Adaptive Cross-Channel Identification with Sparse Shift-Suppression (VAXIS), found in Chapter \ref{ch:5},
	\item Dual Fixed-Peak Adaptive Cross-Channel Identification with Sparse Shift-Suppression (DFPAXIS), found in Chapter \ref{ch:5},
	\item Single Constraint Adaptive Cross-Channel Identification with Sparse Shift-Suppression (SC), found in Chapter \ref{ch:5},
	\item Varied Single Constraint (VSC), found in Chapter \ref{ch:5},
	\item Dual Fixed-Peak Single Constraint (DFPSC), also found in Chapter \ref{ch:5}.
\end{itemize}
A few other possible approaches were explored, which are also included in this thesis. The results of these algorithms were disappointing, and cannot claim to improve performance of channel identification in multipath environments due to their unreliability. These algorithms are discussed in Chapter \ref{ch:6}.  

All of the algorithms explored in this thesis take (either by collecting or simulating the data) a received signal as input, and use the CR relationship to do channel identification. The received signal is assumed to be a transmitted signal convolved with an impulse response. Channel identification is then done to recover the impulse response without any knowledge concerning the transmitted signal. The resulting estimated impulse response is the output of the algorithms. 

The algorithms presented in the following chapters are least-squares (LS) algorithms, and all but one of the algorithms are adaptive, using gradient descent for channel identification. For channel identification, the data is collected and then organized into a matrix, of size $N - (L + 1)\times (L + 1)$, where $N$, is the total number of data samples, and $L$ is the channel order.  The algorithms iterate through the data row by row for the adaptive update. 