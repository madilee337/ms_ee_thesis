\section{Variations}
A couple variations were derived and tested as well to see if any improvement could be made to the BF algorithm. Some of these variations are similar to the variations for the AXIS algorithm.
\subsection{Adaptive Brute Force}
The BF algorithm may seem an ideal approach due to its single step nature. However it contains large matrix inverses, which are costly both in time and in computations, and does not show promising results. This algorithm can easily be converted into an adaptive algorithm, dividing the data matrix, $\mathbf{X}$ into blocks of data can help combat the heavy computations, and theoretically guide the algorithm into providing a better estimate.

Let $\mathbf{X}_r$ denote a section of rows, or a block of data, then (\ref{eq:brute-update-complete}) would become
\begin{equation} \label{eq:brute-adaptive-update-complete}
\mathbf{h}^{(n + 1)} = -(\mathbf{X}_r^\mathrm{H}\mathbf{X}_r)^{-1}
\Big(\alpha\mathbf{e}^{j\angle{\mathbf{h}}^{(n)}} + 
\eta\frac{-1 + [(\mathbf{X}_r^\mathrm{H}\mathbf{X}_r)^{-1}(\alpha\mathbf{e}^{j\angle{\mathbf{h}}^{(n)}})]^\mathrm{H}\mathrm{e}_d}
{\mathrm{e}_d^\mathrm{H}((\mathbf{X}_r^\mathrm{H}\mathbf{X}_r)^{-1})^\mathrm{H}\mathrm{e}_d}\mathrm{e}_d\Big).
\end{equation}
Depending on the size of the blocks of data, this can take very few steps. The larger the block of data, the fewer steps are needed, however, the matrix inverse is larger, which would still be a lengthy process.

Channel estimate results for the ideal simulation can be seen in Figures \ref{fig:ABF_ideal_30v3000} and \ref{fig:ABF_NITF_30v3000}. These results are not much better than the non-adaptive BF algorithm, but are improved. Impulse response set IR-1 appears to have all but the 200 MHz sampling rate producing good results. The 200 MHz sampling rate is quite random in comparison. Set IR-2 does not have as good of estimates as the first set, but there are some good peaks, if not in a very noisy channel estimate.
\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/20MHz_ABF_Ideal__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/100MHz_ABF_Ideal__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/150MHz_ABF_Ideal__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_ABF_Ideal__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the Adaptive BF algorithm with 250,000 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-1, seed 0.}
	\label{fig:ABF_ideal_30v3000}
\end{figure}
\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/20MHz_ABF_NITF__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/100MHz_ABF_NITF__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/150MHz_ABF_NITF__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_ABF_NITF__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the Adaptive BF algorithm with 250,000 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-2, seed 0.}
	\label{fig:ABF_NITF_30v3000}
      \end{figure}
      
As the adaptive results were a bit more promising, and not as computationally expensive, some nonideal conditions can be seen in Figures \ref{fig:ABF_narrow_30v3000}--\ref{fig:ABF_random_30v3000}. Like the previous algorithms, the narrowband simulation did not produce a good estimate, there is little resolution between channels, as shown in Figure \ref{fig:ABF_narrow_30v3000}. Fewer data samples shows similarities with the ideal case. The smaller data size results show that some of the peaks have split, and not yet resolved completely; see Figure \ref{fig:ABF_Nless_30v3000} for a comparison. Interestingly, Figure \ref{fig:ABF_noisy_30v3000} more noise ($SNR = 20$ dB) actually appears to yield noisy, but good channel estimates with distinct peaks. Not having the correct channel order does appear to affect this algorithm, as shown in Figure \ref{fig:ABF_wrongModel_30v3000}, involving more chaotic signals, that look random than not. What is interesting to note is that the normalized random start, as seen in Figure \ref{fig:ABF_random_30v3000} does not appear to have a great impact on the channel estimation, producing similar results to those shown in Figure \ref{fig:ABF_ideal_30v3000}.
\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/20MHz_ABF_narrowband__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/100MHz_ABF_narrowband__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/150MHz_ABF_narrowband__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_ABF_narrowband__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the Adaptive BF algorithm with 250,000 data samples, with SNR = 60 dB, at various sampling rates for narrowband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-1, seed 0.}
	\label{fig:ABF_narrow_30v3000}
\end{figure}

\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/20MHz_ABF_Nless__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/100MHz_ABF_Nless__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/150MHz_ABF_Nless__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_ABF_Nless__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the Adaptive BF algorithm with 2,500 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-1, seed 0.}
	\label{fig:ABF_Nless_30v3000}
\end{figure}
\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/20MHz_ABF_noisy__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/100MHz_ABF_noisy__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/150MHz_ABF_noisy__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_ABF_noisy__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the Adaptive BF algorithm with 250,000 data samples, with SNR = 20 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-1, seed 0.}
	\label{fig:ABF_noisy_30v3000}
\end{figure}
\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_ABF_undEst_2__30vs3000.tikz}}
		\caption{$L = 100$}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_ABF_undEst-10__30vs3000.tikz}}
		\caption{$L = 190$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_ABF_overEst_2__30vs3000.tikz}}
		\caption{$L = 400$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_ABF_overEst-10__30vs3000.tikz}}
		\caption{$L = 200$ MHz}
	\end{subfigure}
	\caption{Results of the Adaptive BF algorithm with 250,000 data samples, with SNR = 60 dB for wideband simulation at 200 MHz sampling rate for incorrect model order estimates (a) $L/2$, (b) $L-10$, (c) $2L$, and (d) $L + 10$, for impulse response set IR-1, seed 0.}
	\label{fig:ABF_wrongModel_30v3000}
      \end{figure}
\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/20MHz_ABF_rStart__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/100MHz_ABF_rStart__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/150MHz_ABF_rStart__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_ABF_rStart__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the Adaptive BF algorithm with 250,000 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-1 with a normalized random start.}
	\label{fig:ABF_random_30v3000}
\end{figure}

The complete derivation and code for this algorithm are located in Appendix \ref{app:BF}, and \ref{code:BF} respectively. It should be noted that this is the exact same algorithm as the non-adaptive BF algorithm, the only difference between the two algorithms is which data matrix is being used: the block data matrix, $\mathbf{X}_r$ (for the adaptive BF), or the full data matrix, $\mathbf{X}$ (for the non-adaptive) data matrix. These can be switched interchangeably depending on which algorithm is desired.

\subsection{Varied Adaptive Brute Force}
The first variation is minute, by merely changing the constraint to allow for a second variable, like the VAXIS algorithm shown in Chapter \ref{ch:5}. As this algorithm involves variation, it is referred to as the Varied ``Brute Force'' (VBF) algorithm. The corresponding objective function to the VBF algorithm is shown as
\begin{equation} \label{eq:brute-varied-obj}
    \begin{aligned}
        & \min_{{\mathbf{h}^{(n + 1)}}^*}&  &||\mathbf{X}_r\mathbf{h}^{(n + 1)}||_2^2 + \alpha||{\mathbf{h}^{(n + 1)}}^*||_1& \\
        & \text{subject to} & & {\mathbf{h}^{(n + 1)}}^\mathrm{H}\mathrm{e}_d = \mathcal{z}.
    \end{aligned}
\end{equation}
As explained in the previous chapter, the variable $\mathcal{z}$ is initialized as 1, but after the first iteration, $\mathcal{z} = \mathbf{h}^{(n + 1)}_d$.
This small alteration only changes the derivation slightly in that $\tilde{\lambda}$ has a $\mathcal{z}$ instead of a 1. The update equation becomes 
\begin{equation}
    \label{eq:brute-varied-adaptive-update-complete}
    \mathbf{h}^{(n + 1)} = -(\mathbf{X}_r^\mathrm{H}\mathbf{X}_r)^{-1}
    \Big(\alpha\mathbf{e}^{j\angle{\mathbf{h}}^{(n)}} + 
    \eta\frac{-\mathcal{z} + [(\mathbf{X}_r^\mathrm{H}\mathbf{X}_r)^{-1}(\alpha\mathbf{e}^{j\angle{\mathbf{h}}^{(n)}})]^\mathrm{H}\mathrm{e}_d}
    {\mathrm{e}_d^\mathrm{H}((\mathbf{X}_r^\mathrm{H}\mathbf{X}_r)^{-1})^\mathrm{H}\mathrm{e}_d}\mathrm{e}_d\Big).
  \end{equation}
The code and complete derivation can be seen in Appendices \ref{code:VBF}, and \ref{app:VBF}. 
  
This does not vary the results much, as can be see in Figure \ref{fig:VBF_ideal_30v3000} and Figure \ref{fig:VBF_NITF_30v3000}. The channel estimates are very similar to the estimates shown for the adaptive BF algorithm, and so no further VBF channel estimates will be shown for the other simulation situations. 

\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/20MHz_VBF_Ideal__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/100MHz_VBF_Ideal__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/150MHz_VBF_Ideal__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_VBF_Ideal__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the VBF algorithm with 250,000 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-1, seed 0.}
	\label{fig:VBF_ideal_30v3000}
\end{figure}
\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/20MHz_VBF_NITF__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/100MHz_VBF_NITF__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/150MHz_VBF_NITF__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_VBF_NITF__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the VBF algorithm with 250,000 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-2, seed 0.}
	\label{fig:VBF_NITF_30v3000}
\end{figure}
\subsection{Dual Fixed-Peak Brute Force}
Again, like the DFPAXIS algorithm in Chapter \ref{ch:5}, a secondary constraint was added to combat the asymmetric treatment of the transfer functions, due to the first constraint. This algorithm in turn is called the Dual Fixed-Peak BF (DFPBF) algorithm.
\begin{equation} \label{eq:brute-dfp-obj}
    \begin{aligned}
        & \min_{{\mathbf{h}^{(n + 1)}}^*}&  &||\mathbf{X}_r\mathbf{h}^{(n + 1)}||_2^2 + \alpha||{\mathbf{h}^{(n + 1)}}^*||_1& \\
        & \text{subject to} & &{\mathbf{h}^{(n + 1)}}^\mathrm{H}\mathrm{e}_d = \mathcal{z}& \\
        & & &{\mathbf{h}^{(n + 1)}}^\mathrm{H}\mathrm{e}_c = \mathcal{y}.
    \end{aligned}
  \end{equation}
  As with the DFPAXIS algorithm, this secondary constraint enforces a peak for the second impulse response, alleviate the focus on the first impulse response.
  
  The derivation for this algorithm starts, like the others, by first implementing the Lagrangian,
\begin{equation} \label{eq:brute-dfp-lagrangian}
    \mathcal{L} = {\mathbf{h}^{(n + 1)}}^\mathrm{H}\mathbf{X}_r^\mathrm{H}\mathbf{X}_r{\mathbf{h}^{(n + 1)}} + \alpha||{\mathbf{h}^{(n + 1)}}^\mathrm{*}||_1 + \lambda({\mathbf{h}^{(n + 1)}}^\mathrm{H}\mathrm{e}_d - \mathcal{z}) + \mu({\mathbf{h}^{(n + 1)}}^\mathrm{H}\mathrm{e}_c - \mathcal{y}).
\end{equation}
Then, the derivative is taken with respect to the conjugate of the transfer function estimates, 
\begin{equation} \label{eq:brute-dfp-derivative-lagrangian}
    \frac{d}{d{\mathbf{h}^{(n + 1)}}^*}\mathcal{L} = \mathbf{X}_r^\mathrm{H}\mathbf{X}_r\mathbf{h}^{(n +1)} + \alpha\mathbf{e}^{j\angle{\mathbf{h}}^{(n + 1)}} + \lambda\mathrm{e}_d + \mu\mathrm{e}_c.
\end{equation}
Setting (\ref{eq:brute-dfp-derivative-lagrangian}) to 0 and solving for $\mathbf{h}^{(n + 1)}$ produces the update,
\begin{equation} \label{eq:brute-dfp-update-lambda-mu}
    \mathbf{h}^{(n +1)} = -(\mathbf{X}_r^\mathrm{H}\mathbf{X}_r)^{-1} (\alpha\mathbf{e}^{j\angle{\mathbf{h}}^{(n)}} + \lambda\mathrm{e}_d + \mu\mathrm{e}_c).
\end{equation}
Replacing the update equation, (\ref{eq:brute-dfp-update-lambda-mu}), in place of $\mathbf{h}^{(n + 1)}$ in the constraints listed in (\ref{eq:brute-dfp-obj}) and solving produces a system of equations,
\begin{equation} 
    \begin{aligned}
        &-\mathcal{z} - \big((\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}
        \alpha\mathbf{e}^{{j}\angle\mathbf{h}^{(n)}}\big)^\mathrm{H}\mathrm{e}_{d}& 
        &=& 
        &\tilde{\lambda}\mathrm{e}_d^\mathrm{H}
        \big((\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\big)^\mathrm{H}\mathrm{e}_d+
        \tilde{\mu}\mathrm{e}_c^\mathrm{H}
        \big((\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\big)^\mathrm{H}\mathrm{e}_d & \\
        &-\mathcal{y} - \big((\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}
        \alpha\mathbf{e}^{{j}\angle\mathbf{h}^{(n)}}\big)^\mathrm{H}\mathrm{e}_{c}& 
        &=& 
        &\tilde{\lambda}\mathrm{e}_d^\mathrm{H}
        \big((\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\big)^\mathrm{H}\mathrm{e}_c+
        \tilde{\mu}\mathrm{e}_c^\mathrm{H}
        \big((\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\big)^\mathrm{H}\mathrm{e}_c &  
    \end{aligned}
\end{equation}
\begin{equation}
    \begin{bmatrix}
        \mathrm{e}_d^\mathrm{H}
        (\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_d & 
        \mathrm{e}_c^\mathrm{H}
        (\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_d \\
        \mathrm{e}_d^\mathrm{H}
        (\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_c &
        \mathrm{e}_c^\mathrm{H}
        (\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_c
    \end{bmatrix}
    \begin{bmatrix}
        \tilde{\lambda} \\
        \tilde{\mu}
    \end{bmatrix}
    =
    \begin{bmatrix}
        -\mathcal{z} - \big((\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}
        \alpha\mathbf{e}^{{j}\angle\mathbf{h}^{(n)}}\big)^\mathrm{H}\mathrm{e}_{d} \\
        -\mathcal{y} - \big((\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}
        \alpha\mathbf{e}^{{j}\angle\mathbf{h}^{(n)}}\big)^\mathrm{H}\mathrm{e}_{c}
    \end{bmatrix}.
\end{equation}
The variables  $\tilde{\lambda}$ and $\tilde{\mu}$ are defined as 
\begin{equation} \label{eq:brute-dfp-lambda-mu}
    \tilde{\lambda} = \frac{(\mathcal{y} +{\alpha\mathbf{e}^{{j}\angle\mathbf{h}^{(n)}}}^\mathrm{H} (\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_{c})
    \mathrm{e}_c^{\mathrm{H}}(\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_d
    - (\mathcal{z} +{\alpha\mathbf{e}^{{j}\angle\mathbf{h}^{(n)}}}^\mathrm{H} (\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_{d})
    \mathrm{e}_c^{\mathrm{H}}(\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_c
    }
    {\mathrm{e}_d^\mathrm{H}
    (\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_d
    \mathrm{e}_c^\mathrm{H}
    (\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_c -
     \mathrm{e}_d^\mathrm{H}
    (\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_c 
    \mathrm{e}_c^\mathrm{H}
     \mathrm{e}_c^\mathrm{H}
    (\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_d
    + \epsilon} 
\end{equation}
\begin{equation} \notag
    \tilde{\mu} = \frac{\mathrm{e}_d^{\mathrm{H}}(\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_c(\mathcal{z} +{\alpha\mathbf{e}^{{j}\angle\mathbf{h}^{(n)}}}^\mathrm{H} (\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_{d})
    -\mathrm{e}_d^{\mathrm{H}}(\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_d(\mathcal{y} +{\alpha\mathbf{e}^{{j}\angle\mathbf{h}^{(n)}}}^\mathrm{H} (\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_{c})
    }
    {\mathrm{e}_d^\mathrm{H}
    (\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_d
    \mathrm{e}_c^\mathrm{H}
    (\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_c -
     \mathrm{e}_d^\mathrm{H}
    (\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_c 
    \mathrm{e}_c^\mathrm{H}
     \mathrm{e}_c^\mathrm{H}
    (\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_d
    + \epsilon}
\end{equation}
Replacing $\lambda$ and $\mu$ with the conjugates of (\ref{eq:brute-dfp-lambda-mu}) in (\ref{eq:brute-dfp-update-lambda-mu}), becomes
\begin{multline} \label{eq:brute-dfp-update-complete}
    \mathbf{h}^{(n +1)} = -(\mathbf{X}_r^\mathrm{H}\mathbf{X}_r)^{-1}\Bigg(\alpha\mathbf{e}^{j\angle{\mathbf{h}}^{(n)}} + \eta\Big(\\
    \Big(\frac{(\mathcal{y} +{\alpha\mathbf{e}^{{j}\angle\mathbf{h}^{(n)}}}^\mathrm{H} (\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_{c})
    \mathrm{e}_c^{\mathrm{H}}(\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_d
    - (\mathcal{z} +{\alpha\mathbf{e}^{{j}\angle\mathbf{h}^{(n)}}}^\mathrm{H} (\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_{d})
    \mathrm{e}_c^{\mathrm{H}}(\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_c
    }
    {\mathrm{e}_d^\mathrm{H}
    (\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_d
    \mathrm{e}_c^\mathrm{H}
    (\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_c -
     \mathrm{e}_d^\mathrm{H}
    (\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_c 
    \mathrm{e}_c^\mathrm{H}
     \mathrm{e}_c^\mathrm{H}
    (\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_d
    + \epsilon}\Big)^*\mathrm{e}_d + \\ 
    \Big(\frac{\mathrm{e}_d^{\mathrm{H}}(\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_c(\mathcal{z} +{\alpha\mathbf{e}^{{j}\angle\mathbf{h}^{(n)}}}^\mathrm{H} (\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_{d})
    -\mathrm{e}_d^{\mathrm{H}}(\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_d(\mathcal{y} +{\alpha\mathbf{e}^{{j}\angle\mathbf{h}^{(n)}}}^\mathrm{H} (\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_{c})
    }
    {\mathrm{e}_d^\mathrm{H}
    (\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_d
    \mathrm{e}_c^\mathrm{H}
    (\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_c -
     \mathrm{e}_d^\mathrm{H}
    (\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_c 
    \mathrm{e}_c^\mathrm{H}
     \mathrm{e}_c^\mathrm{H}
    (\mathbf{X}_{r}^\mathrm{H}\mathbf{X}_{r})^{-1}\mathrm{e}_d
    + \epsilon}\Big)^*\mathrm{e}_c\Big)
    \Bigg).
  \end{multline}
\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/20MHz_DFPBF_Ideal__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/100MHz_DFPBF_Ideal__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/150MHz_DFPBF_Ideal__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_DFPBF_Ideal__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the DFPBF algorithm with 250,000 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-1, seed 0.}
	\label{fig:DFPBF_ideal_30v3000}
\end{figure}
\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/20MHz_DFPBF_NITF__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/100MHz_DFPBF_NITF__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/150MHz_DFPBF_NITF__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_DFPBF_NITF__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the DFPBF algorithm with 250,000 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-2, seed 0.}
	\label{fig:DFPBF_NITF_30v3000}
\end{figure}

\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/20MHz_DFPBF_narrowband__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/100MHz_DFPBF_narrowband__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/150MHz_DFPBF_narrowband__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_DFPBF_narrowband__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the DFPBF algorithm with 250,000 data samples, with SNR = 60 dB, at various sampling rates for narrowband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-1, seed 0.}
	\label{fig:DFPBF_narrow_30v3000}
\end{figure}

\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/20MHz_DFPBF_Nless__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/100MHz_DFPBF_Nless__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/150MHz_DFPBF_Nless__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_DFPBF_Nless__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the DFPBF algorithm with 2,500 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-1, seed 0.}
	\label{fig:DFPBF_nless_30v3000}
\end{figure}
\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/20MHz_DFPBF_noisy__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/100MHz_DFPBF_noisy__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/150MHz_DFPBF_noisy__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_DFPBF_noisy__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the DFPBF algorithm with 250,000 data samples, with SNR = 20 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-1, seed 0.}
	\label{fig:DFPBF_noisy_30v3000}
\end{figure}
\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_DFPBF_undEst_2__30vs3000.tikz}}
		\caption{$L = 100$}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_DFPBF_undEst-10__30vs3000.tikz}}
		\caption{$L = 190$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_DFPBF_overEst_2__30vs3000.tikz}}
		\caption{$L = 400$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_DFPBF_overEst-10__30vs3000.tikz}}
		\caption{$L = 200$ MHz}
	\end{subfigure}
	\caption{Results of the DFPBF algorithm with 250,000 data samples, with SNR = 60 dB for wideband simulation at 200 MHz sampling rate for incorrect model order estimates (a) $L/2$, (b) $L-10$, (c) $2L$, and (d) $L + 10$, for impulse response set IR-1, seed 0.}
	\label{fig:DFPBF_wrongModel_30v3000}
      \end{figure}
      \begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/20MHz_DFPBF_rStart__30vs3000.tikz}}
		\caption{$F_s = 20$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/100MHz_DFPBF_rStart__30vs3000.tikz}}
		\caption{$F_s = 100$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/150MHz_DFPBF_rStart__30vs3000.tikz}}
		\caption{$F_s = 150$ MHz}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Chapters/Chapter_6/Figures/200MHz_DFPBF_rStart__30vs3000.tikz}}
		\caption{$F_s = 200$ MHz}
	\end{subfigure}
	\caption{Results of the DFPBF algorithm with 250,000 data samples, with SNR = 60 dB, at various sampling rates for wideband simulation, (a) 20 MHz, (b) 100 MHz, (c) 150 MHz, and (d) 200 MHz, for impulse response set IR-1, seed 0 with a normalized random start.}
	\label{fig:DFPBF_random_30v3000}
      \end{figure}
  
  As explained in Chapter \ref{ch:5}, the DFPBF algorithm is a secondary algorithm to try to eliminate the asymmetrical treatment of the transfer functions. And, as with the other DFP algorithms, $\mathcal{z}$ is initialized to 1. The $c^{th}$ element, $\mathcal{y}$ cannot get its value until the relationship between the impulse responses has been established.  Thus, for the first iteration or two of the algorith, one of the other algorithms must be used to initalize the channel estimates. Then, once the delay relationship has been established,  $\mathcal{y}$ can take its value from the maximum peak of the second impulse response.

  The impulse response estimates for the DFPBF algorithm can be seen in Figures \ref{fig:DFPBF_ideal_30v3000}--\ref{fig:DFPBF_random_30v3000}. There are some similarities with the adaptive BF results, but there are some distinction, and so all of the simulation situation variations are shown here.

  Figure \ref{fig:DFPBF_ideal_30v3000} and Figure \ref{fig:DFPBF_NITF_30v3000} show the ideal channel estimation results. Like with the adaptive BF results, the 200 MHz sampling rate estimates are not sparse. It is interespting to note that the channel estimates for the 100 MHz and 150 MHz sampling rates are also quite good for the second impulse response set, IR-2, unlike the adaptive BF results.

  There is little to no resolution in the narrowband simultion, as is typical for all of these algorithms explored in this thesis, see Figure \ref{fig:DFPBF_narrow_30v3000}. With respect to the size of the data, the fewer samples that were used to estimate the channel estimates in Figure \ref{fig:DFPBF_nless_30v3000}, actually show some improvement with respect to the BF algorithm. There appears to be some reduction in the duplicate peaks seen in Figure \ref{fig:ABF_Nless_30v3000}. In comparison with the adaptive BF algorithm, it is interesting to note that for the noisy case (20 dB SNR), the peaks have shifted from their placement in the adaptive BF algorithm, dislayed Figure \ref{fig:DFPBF_noisy_30v3000}. Figure \ref{fig:DFPBF_wrongModel_30v3000} shows the effects of the incorrect model order. Underestimating by one half appears to actually produce good results, but the other incorrect model order estimates produce similar results to the ideal case.

      A complete derivation of the DFPBF algorithm can be found in Appendix \ref{app:DFPBF}. The code listing for the DFPBF is found in Appendix \ref{code:DFPBF}.