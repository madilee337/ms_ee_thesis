%% Overhead - Real
% This version incorporates multiple functions for ease of processing for
% real observed data collected using the SDRs 
clear all; 
close all; 
clc;
% Add all files needed to working directory
dirs{1} = '/home/madi/Research_2018-2019/tdoa-bcid/Algorithms_MATLAB_Take2/Real/algorithms/';
dirs{2} = '/home/madi/Research_2018-2019/tdoa-bcid/Algorithms_MATLAB_Take2/Real/data/';
% Cycle through and add paths
for fold = 1:length(dirs)
    % Add folders
    addpath(dirs{fold});
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialization and Pre-Processing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initializations and pre-processing for algorithm comparison
    % Sampling Rate (FIX) 
    Fs = 16e6;
    % Convert sampling rate to MHz
    Fs_MHz = Fs/1e6;
    % Label sampling rate for output
    sampleRate = [num2str(Fs_MHz), ' MHz'];
    % Compute L 
    L = Fs_MHz;
    % Speed of light
    c = 3e8;
    % Distance conversion
    dist = (c/Fs).';  
    % Determine which data to read from and label data set for output
    dateDat = '002_2020-02-26_14-51'; dataSet = 'WalkieTalkie_Inside';
%     dateDat = '002_2020-02-26_14-52'; dataSet = 'WalkieTalkie_Hallway';
%     dateDat = '002_2020-02-26_14-54'; dataSet = 'WalkieTalkie_Under';
    % Initialize file to output results to
    fileName = ['Method_Comparisons_Real_' date '_v1.csv'];
     % Check if file already exits to prevent overwrite
    while exist(fileName, 'file')
        % Determine version
        if fileName(end - 5) == 'v'
            % If single digits
            ver = str2double(fileName(end-4));
        elseif fileName(end - 6) == 'v'
            % If double digits
            ver = str2double(fileName(end-5:end-4));
        elseif fileName(end - 7) == 'v'
            % If triple digits
            str2double(fileName(end-6:end-4));
        else
            % If more
            msgl1 = 'HELP! More than triple digits method comparison\n';
            msgl2 = 'files, add more to if statement';
            msg = [msgl1 msgl2];
            error(msg);
        end
        % Update version
        new_ver = ver + 1;
        % Determine new filename
        fileName = ['Method_Comparisons_Real_' date '_v' ...
                                                  num2str(new_ver) '.csv'];
    end
    % Set first row of data table array
    header = {'Data_Set', '&', 'Channels', '&', 'Algorithm',  '&', ...
                                   'Sample_Rate', '&', 'Estimate_m', '\\'};
    % Initialize number of columns for output table;
    dataTabCol = 5;
    % Set algorithms (1:19 selects which algorithm to run, NOTE multiple
    % algorithms can be selected)
    alg_list = {'CC', 'SVD', ... % [1 2]
            'AED', 'ModAED', 'ModAEDS', ... %[3 4 5]
            'AXIS', 'VAXIS', 'DFPAXIS', 'NSAXIS',... %[6 7 8 9]
            'NBF', 'ABF', 'VBF', 'DFPBF', 'NSBF',... %[10 11 12 13 14]
            'SC', 'VSC', 'DFPSC', 'NSSC'}; %[15 16 17 18]
    % Select the adaptive algorithms and the CC algorithm for processing
    % real data
    algs = alg_list([1, 3, 4, 5, 6, 7, 8, 9 15, 16, 17, 18]);
    % Determine number of algorithms
    numAlgs = length(algs);
    % Create dataTable
    dataTable =  cell(numAlgs, 2*dataTabCol);
    % Read in data 
    data = readDataFile(dirs{2}, dateDat);
    % Modify to processable size
%     N = 2500;
    N = length(data);
    data = data(:, 1:N);
    % Determine which channels to run through processing (1 2 3 4)
    channels = [1 3];
    % Process all data through algorithms
    for a = 1:numAlgs
        % Process algorithms
        delayEst = algSelect(algs{a}, data, L, channels);
        % Compute distance estimate
        distEst = delayEst*dist;
        % Add to table
        dataTable(a, :) = {dataSet, '&', [num2str(channels(1)) '\&' ...
                        num2str(channels(2))], '&', algs{a}, '&', ...
                                           sampleRate, '&', distEst, '\\'};
    end
    % Output to file
    T = cell2table(vertcat(header, dataTable));
    writetable(T, fileName, 'Delimiter', ' ');         
    
    
    
    
    
