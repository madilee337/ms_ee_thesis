%% Read In GNU Radio Data
% GNU Radio data is collected in subfolder dat files, this code cycles
% through the subfolders and reads in the dat files
%   Input:
%       - directory: This is the location of the data files
%       - dateDat: This denotes which folders to cycle through via the date
%   Output:
%       - data: This holds all of the channels data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function data = readDataFile(directory, dateDat)
    % Determine length of date
    lenD = length(dateDat);
    % Get files in directory
    files = dir(directory);
    directoryNames = {files([files.isdir]).name};
     % Cycle through directory to add data subfolders
    for fold = 1:length(directoryNames)
        % Check if folder matches date 
        if length(directoryNames{fold}) > lenD
            if strcmp(directoryNames{fold}(1:lenD), dateDat)
                % Save directory
                subDir = directoryNames{fold};
                % break out of loop
                break
            end
        elseif fold == length(directoryNames)
            % Add Warning and Break
            disp('Directory does not exist in specified folder, pausing.')
            pause();
        end
    end
    % Add subDir to path
    subDirPath = [directory subDir];
    addpath(subDirPath);
    % Grab dat files in folder
    dataFiles = dir([subDirPath '/*.dat']);
    dataFilesName = strings(length(dataFiles), 1);
    for datFile = 1:length(dataFiles)
        dataFilesName(datFile, 1) = dataFiles(datFile, 1).name;
    end
%     data = zeros(length(dataFiles), 1);
    % Load in data files
    for d = 1:length(dataFilesName)
        % Load in data files
        fid = fopen(dataFilesName(d));
        dat = fread(fid, 'int16');
        fclose(fid);
        % Format
        dat = reshape(dat, [length(dat)/2 2]);
        dat = dat(:, 1) + 1j*dat(:, 2);
        % Put in data array
        data(d,:) = dat;
    end
end