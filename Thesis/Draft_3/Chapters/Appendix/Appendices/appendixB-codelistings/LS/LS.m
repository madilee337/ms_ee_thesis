% Least Squares Singular Value Decomposition Algorithm
%   - The channel estimation algorithm that utilizes the SVD
%   Inputs:
%       - x: Toeplitz data matrix size: (N - L)xM(L + 1)
%       - L: the length of a transfer function estimate
%       - M: the number of channels
%   Outputs:
%       - EstChannels: vector containing the new updated estimate for
%       transfer functions
function EstChannels = LS(x, L, M)
%% Pre-Processing
%% Processing
    % Call the SVD
    [~, ~, hnew] = svd(x);
%% Output
    % Format output
    EstChannels = reshape(hnew(:, end), L + 1, M);
end