\section{Comparison of Algorithms}
All of the algorithms proposed in this thesis will be compared and contrasted here. Algorithm complexity, simulation results and observed results all contribute to the viability of the algorithms usage in a real world environment. 
\subsection{Algorithm Complexity}
Computational complexity is a rough estimate of how efficient an algorithm is. It cannot describe exactly how much more work goes into processing the data, as it only represents the additions and multiplies, as explained in \cite{book:matComp}. However, it is still a common way to express efficiency of an algorithm, so computational complexity is shown below for some of the algorithms discussed in this thesis\footnote{The BF group of algorithms is not discussed further in this thesis as the simulation results were not very good.}. Table \ref{tab:compAlgComplexity} shows the computational complexity of the algorithms using both the adaptive line-by-line approach, and the modified block approach explained in more detail in Appendix \ref{code:mods}.

As can be seen in the table, the SVD algorithm is by far the most computationally expensive algorithm, and cannot be made any simpler. Looking at only the line-by-line complexity in the second column, the DFPSC has high complexity compared with the other algorothms, but it is of the same magnitude as the other SC algorithms. For the non-single constraint and SVD algorithms, the DFPAXIS has the most computational complexity. The ModAED algorithm actually has the smallest complexity, only needing $6M(N - L)(L + 1)$ additions and multiplies. 

\begin{landscape}
\begin{table}
  \begin{center}
    \begin{tabular}{||c c c||}
      \hline
      Algorithm & Complexity (Line) & Complexity (Block) \\ [0.5ex]
      \hline\hline
      SVD & $4\big((N - L)^2M(L + 1)\big) + 8\Big((N - L)\big(M(L + 1)\big)^2\Big) + 9\big(M(L + 1)\big)^3$  & N/A\\
      \hline
      AED & $(N-L)(9M(L+1) - 1)$ & $\frac{(N - L)}{b}(M(L+1)(4b + 7) - (b + 2) + 1$ \\ 
      \hline
      ModAED & $(N - L)(6M(L + 1))$ & $\frac{(N - L)}{b}(M(L+1)(6b + 2) -2b)$ \\
      \hline
      ModAEDS & $(N - L)(8M(L + 1))$ &$\frac{(N - L)}{b}(M(L+1)(6b + 4) - 2b)$  \\
      \hline
      AXIS & $(N - L)(30M(L + 1) + 2)$ & $\frac{(N - L)}{b}(M(L+1)(24b + 9) - 2)$ \\
      \hline
      VAXIS & $(N - L)(30M(L + 1) + 4)$ & $\frac{(N - L)}{b}(M(L + 1)(9 + 24b) + 2(b - 1))$ \\
      \hline
      DFPAXIS & $(N - L)(52M(L + 1) + 11)$ & $\frac{(N - L)}{b}(M(L + 1)(10 + 46b) +5b)$ \\
      \hline
      NSAXIS & $(N - L)(28M(L + 1) + 2)$ & $\frac{(N - L)}{b}(M(L + 1)(24b + 7) -2)$\\
      \hline
      SC &  $(N - L)(9M^2(L + 1)^2 + 8M(L + 1))$ & $\frac{(N - L)}{b}(M(L + 1)(9 + 7b)-2b + 1)$ \\
      \hline
      VSC &$(N - L)(9M^2(L + 1)^2 + 8M(L + 1) + 1)$ & $\frac{(N - L)}{b}(M(L + 1)(9 + 7b)-2b + 2)$ \\
      \hline
      DFPSC & $(N - L)(12M^2(L + 1)^2 + 10M(L + 1) + 1)$ & $\frac{(N - L)}{b}(M(L + 1)(11 + 7b) - 2b + 4)$\\
      \hline
      NSSC & $(N - L)(9M^2(L + 1)^2 + 6M(L + 1))$ & $\frac{(N - L)}{b}(7M(L + 1)(b + 1) - 2b + 1)$\\
      \hline
    \end{tabular}
  \end{center}
  \caption{Comparison of the computational complexity of the algorithms.}
  \label{tab:compAlgComplexity}
\end{table}
\end{landscape}
The third column of the table shows the computational complexity for the modified block approach developed to be more computationally efficient. It should be noted that the modified block implementation of the code was not implemented to be more computationally efficient, but to yield more accurate results, however it appears that in some cases, the algorithms might actually be more efficient, such as the SC variants of the AXIS algorithm. The DFPAXIS algorithm actually requires the most computations, and the AED algorithm the least. 
\subsection{Simulation Results}
All the algorithms were tested using simulation first. In simulation, all but the SVD algorithm derived by Xu et al. used the same simulation conditions.
\begin{table}
	\begin{center}
		\begin{tabular}{||c c||}
			\hline
			Variable & Value \\ [0.5ex]
			\hline\hline
			$\epsilon$ & 1e-10 \\
			\hline
			$\alpha$ & 1e-5 \\ 
			\hline
			$\gamma$ & 1e-5 \\
			\hline
			$\phi$ & 1e-5
			\\
			\hline
		\end{tabular}
	\end{center}
	\caption{Table of various variables used in simulation.}
	\label{tab:Sim_Variable_Values_Code}
      \end{table}
      \subsubsection{MATLAB Conditions}
      During simulation, two different data sizes, two different noise levels, two different bandwidths, two different initial estimates, and five different model orders at 11 different sample rates\footnote{See Section \ref{sec:sim_params} for an explanation of these variations.} were tested. However with all the variation that occured during testing, there was some consistency between parameter variations. MATLAB was used for all simulation testing on the same computer. Table \ref{tab:Sim_Variable_Values_code} show the constant variables used during simulation. These values were arbitrarily decided, prior to processing.     
      \subsubsection{Results}
      Numerical results for the algorithms discussed here can be found in Chapters \ref{ch:4} and \ref{ch:5}, although the full results can be found in Appendix \ref{app:results}.

 In the ideal case, all of the adaptive algorithms had comparable results as seen in Tables \ref{tab:preExist_Algs_Results_ideal}, \ref{tab:AXIS_ideal_results_IR_1} and \ref{tab:AXIS_ideal_results_IR_2}. Although the DFPAXIS algorithm typically has the best channel estimate, all the distance estimates are typically within 1 meter from each other. As observed previously, impulse response set IR-2 typically has better results than set IR-1. For set IR-1, the 20 MHz SVD has arguably the best estimate due to the range of the estimate being zero. However with that argument, the SVD also has the worst estimate for set IR-2 at 200 MHz as it has a very large range, although the error is quite small. It is interesting to note that although mathematically, the AXiS algorithms are more similar to the ModAED and ModAEDS algorithms, the produce results more similar to the AED algorithm. Another interesting observation is that when the SVD suffers, the adaptive algorithms tend to do much better.


\paragraph{\textbf{\textit{Effects of Various Parameters:}}}
      There were various prameters that were adjusted during simulation to test the robustness of the algorithms. This included testing the robustness to signal bandwidth, noise, sample rate, data size, channel order, and  initial channel estimates. The effects of these parameters will be discussed below. 
      \subparagraph{\underline{Signal Bandwidth and Sample Rate:}} As was seen in all the figures in Chapters \ref{ch:4} and \ref{ch:5}, the narrowband simulations for all the algorithms resulted in very bad channel estimates that showed that the channels did not appear to resolve, but rather were duplicates of each other. Table \ref{tab:preExist_Algs_Results_narrow} and Tables  \ref{tab:AXIS_narrow_results_IR_1} and \ref{tab:AXIS_narrow_results_IR_2} display large estimation ranges. Impulse response set IR-1, show on average, very small distance estimates, while set IR-2 show larger estimates that are actulaly closer to the estimates typically used.

      The wideband simulations have shown to do much better. In these simulations, bandwidth corresponds with sample rate directly. It is  assumed that the higher the sample rate, the higher the bandwidth. It appears that higher bandwidth could correlate to better accuracy.  
      \subparagraph{\underline{Noise:}}
      The results for more noise can be seen in Table \ref{tab:preExist_Algs_Results_noisy}, Table \ref{tab:AXIS_noisy_results_IR_1} and Table \ref{tab:AXIS_noisy_results_IR_2}. The noisy results, excluding the SVD algorithm, are not that different from the less noisy case.

      The SVD algorithm suffers greatly with the added noise, The results in the table shows a wide range of estimates with high average values, that degenerate at the higher sampling rate. The plots in Chapter \ref{ch:4} show that the estimates degenerate overall. For the highest sample rate (200 MHz), the SVD algorithm is not able to resolve the channels at all. NaN is the average distance estimate for both impulse response sets at 200 MHz.
      
      Like the less noisy case, the DFPAXIS algorithm tends to do better than the other algorithms, although it is a slight margin over the other adaptive algorithms. There are even some instances where the noisy distance estimates are better than the ideal case. It should be noted that typically, however, the algorithms either do the same or slightly poorer. 
      \subparagraph{\underline{Sample Size:}}
      Tables \ref{tab:preExist_Algs_Results_lessSamples}, \ref{tab:AXIS_Nless_results_IR_1} and\ref{tab:AXIS_Nless_results_IR_2} show the numerical results for the simulations which used fewer samples. There is very little difference from the more samples case excluding the SVD approach. Some of the algorithm results show that they even perform better than with more samples, and some show the opposite. 

      The adaptive algorithms with fewer samples on average produce the same results. There is not one algorithm whose performance stands out significantly compared with the others. The SVD approach does fine for the smaller sample rates, but degenerates at the high sample rates. This could possibly be due to the model order being too close to the sample amount. It should be noted that the SVD algorithm wasn't able to use as many samples as the other algorithms due to memory issues, but the ratio between the different data sizes is the same.
      \subparagraph{\underline{Channel Order:}}
      The wrong model order results can be seen in Tables \ref{tab:preExist_Algs_Results_wrongModel_order_under1}--\ref{tab:preExist_Algs_Results_wrongModel_order_over2}, and Tables \ref{tab:AXIS_wrongModel_under1_results_IR_1}--\ref{tab:AXIS_wrongModel_over2_results_IR_2}. The model order results show that most of the algorithms are robust to model order error, and some are not. 

      Underestimating the channel order for both the SVD and the adaptive algorithms does quite terribly for the $20$ MHz sampling rate in the first impulse response set, IR-1, yielding only NaNs. These NaNs possibly occurred due to there is not enough room for the peaks to establish themselves within 10 samples. There does not appear to be much effect on the adaptive algorithms. The distance estimates are similar (within $\approx 1$ or $2$ m difference) to those shown in the correct model order cases. The SVD appears to suffer more with the underestimation of the channel order than the adaptive algorithms. The SVD has a much greater disparity between the correct model order and the underestimated model order.

      As can be seen in Tables \ref{tab:preExist_Algs_Results_wrongModel_order_over1} and \ref{tab:preExist_Algs_Results_wrongModel_order_over2} overestimating the channel order is much more detrimental to the SVD algorithm. Although estimated distance are not NaNs, the channel estimates shown in Chapter \ref{ch:4} show that even overestimating by 10 samples degenerates the estimate, although not as doubling the model order. Interestingly, the adaptive algorithms were not as affected by overestimating the model order. The estimates were only slightly different from the estimates with the actual order.
      \subparagraph{\underline{Initial Channel Estimates:}}
      In the ideal case, and for all the cases described above, the initial channel estimate for the adaptive algorithms was a unit vector, shown in (\ref{eq:ch_4_unit_vector}). A different initial channel estimate was also tested. As can be seen in Table \ref{tab:preExist_Algs_Results_randomStart}, Table \ref{tab:AXIS_random_results_IR_1}, and Table \ref{tab:AXIS_random_results_IR_2}, using a normalized random start does not produce good estimates. The distance estimates for all but the AXIS and NSAXIS are extremly large, with very large ranges of estimates. The SVD algorithm does not use an initial estimate, so it is not included in the tables or this section.  
\subsection{Results with Real Observed Data}
\subsubsection{Testing Conditions}
\subsubsection{Results}
