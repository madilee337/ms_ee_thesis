\documentclass[journal]{IEEEtran}
\usepackage{cite}
\input{symbolmac}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{url}
\usepackage{graphicx}
\usepackage{float}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{wrapfig}
\usepackage{epsfig}
\usepackage{color} 
\usepackage{euscript}
\usepackage{tikz}
\usetikzlibrary{decorations.pathreplacing}
\usetikzlibrary{patterns}
\usetikzlibrary{positioning}
\usepackage{etex}		
\usepackage{pgfplots}			
\pgfplotsset{compat=1.6}     
\usetikzlibrary{calc}
\usepackage{calc}

\newcounter{mytempeqncnt}

\begin{document}
\title{An Alternative Blind Channel Identification Method Using the Cross-Relation}
\author{Madison~L.~Rose,
        Joseph~Ipson,
        Todd~K.~Moon,~\IEEEmembership{Senior Member,~IEEE,}
        and~Jacob~H.~Gunther~\IEEEmembership{Senior Member,~IEEE}% <-this % stops a space
\thanks{The authors are  with the Department of Electrical and
  Computer Engineering, Utah State University, Logan, UT  84322 USA (email: {madi.mickelsen@aggiemail.usu.edu,  jipson@aggiemail.usu.edu, todd.moon@usu.edu, jake.gunther@usu.edu)}}}
\markboth{IEEE Signal Processing Letters, April~2020}%
{Rose \MakeLowercase{\textit{et al.}}: An Alternative Blind Channel Identification Method Using the Cross-Relation}
\maketitle
\begin{abstract}
Blind channel identification (BCI) is useful for source localization in multipath environments as  when identifying the channel, a time delay between received signals can be established.  This time delay can then be extrapolated to be applied to time difference of arrival (TDOA).  This letter introduces an alternative approach which uses the cross-relation (CR), referred to as the Adaptive Cross-Channel Identification with Sparse Shift-Suppression (AXIS) algorithm.
\end{abstract}

% Note that keywords are not normally used for peerreview papers.
\begin{IEEEkeywords}
channel identification, cross-relation, iterative algorithm, multipath, time delay, source localization, time difference of arrival, 
\end{IEEEkeywords}


\section{Introduction}
\IEEEPARstart{S}{ource} localization has become increasingly relevant
as technology advances.  Time difference of arrival (TDOA) source
localization approaches are of great interest, as TDOA can be used in
blind situations, and in the presence of severe multipath, as long as
direct paths are detectable.   Many time delay estimation (TDE)
techniques rely on identifying the largest peak in the
cross-correlation or related function \cite{bib:thesis}.  However,
these algorithms have been shown to deteriorate in the presence of
multipath.  It should also be noted that in indoor environments or
other multipath conditions, the strongest path may not correspond to
the shortest path (the direct path), and thus the largest peak can
misidentify time delays.

An alternative approach to identifying time delays involves using
channel estimates of the impulse responses
\cite{bib:thesis,bib:kailath,bib:benesty}.  When the source signal is
unknown, blind channel identification (BCI) is performed using the
received signals, and the relevant time delays are extracted from
peaks in those estimates.  While only the relative time delay between a
set of receivers is strictly required for time-delay estimation, there
are advantages to estimating full channel responses where possible.
First, if detectable, the earliest path represented in an impulse
response can be chosen correctly, even if not the strongest available
path.   Also, the estimated channel can give time delay and amplitude
estimates for secondary paths, which may be useful information in some
position estimation techniques, such as those found in
\cite{bib:TDOA_ML}.   Also, the set of delay spreads may provide
information about local geographic complexity, such as the quantity of
local reflectors.

The purpose of this letter is to enhance previous work found in \cite{bib:kailath,bib:benesty} on blind channel identification, by proposing a method utilizing both sparsity, and anchor constrains in an iterative normalized least mean squares (NLMS) type algorithm.  
\section{Complex Cross Relations Methods}
The formulation used in this research largely follow that given in
\cite{bib:thesis,bib:kailath,bib:benesty}.  Consider a source at an
unknown location transmitting an unknown signal in a multipath
environment that is detected by $M \geq 1$ receivers.  This system is
modelled as
\begin{equation}
\label{eq:system_conj}
\begin{cases}
x_{1}(n)=s(n)*h^*_{1}(n)+w_{1}(n) \\ 
\ \vdots \\ 
x_{M}(n)=s(n)*h^*_{M}(n)+w_{M}(n), 
\end{cases}
\end{equation}
where $*$ denotes convolution, $h^*_{i}(n)$ denotes the complex
conjugate of each individual impulse response (channel) between the
source to the $i^\mathrm{th}$ receiver, $x_{i}(n)$ the value received
from the transmitted signal at the $i^{th}$ receiver, and $w_{i}(n)$.
e $n^\mathrm{th}$ sample. This model also assumes a finite impulse
response.

Neglecting noise, the signal model, (\ref{eq:system_conj}), can be expressed individually as $x_i(n) = s(n)*h^*_i(n)$, where $i = 1, \cdots, M$.  Convolving $x_i(n)$ with $h_j^*(n)$, where $i \neq j$ produces the complex cross-relation (CR) for a pair of receivers,
\begin{equation}
\label{eq:cr-derivation_conj}
h^*_j(n)*x_i(n) - h^*_i(n)*x_j(n) = 0.
\end{equation}
The CR can be expressed in terms of receiver measurements as the matrix equation \begin{equation} \label{eq:matCrossRelation}
        \begin{bmatrix} \mathrm{X}_i(L) & \vdots & -\mathrm{X}_j(L)
        \end{bmatrix}
        \begin{bmatrix} \hbf_j^* \\ \hbf_i^* \end{bmatrix} = 0,
\end{equation}
where $\mathrm{X}_m(L)$ is a Toeplitz matrix of the form
\begin{equation} \notag
    \mathrm{X}_m(L) = \begin{bmatrix}
                        x_m(L) & x_m(L - 1) & \cdots & x_m(0) \\
                        x_m(L + 1) & x_m(L) & \cdots & x_m(1) \\
                        \vdots & \ddots & \ddots & \vdots \\
                        x_m(N) & x_m(N - 1) & \cdots & x(N - L - 1)
                      \end{bmatrix}
\end{equation}
and $\hbf_m$ is the impulse response vector 
\begin{equation} \notag
        \hbf_m^* \triangleq [h_m(0), \cdots, h_m(L)]^{H}
    \end{equation}
For brevity, we will rewrite (\ref{eq:matCrossRelation}) as
\begin{equation} \label{eq:CR_brief}
    \Xbf\hbf^* = \mathbf{0}.
\end{equation}
Equation (\ref{eq:CR_brief}) is the key to the methods of channel identification discussed in this letter.  In practice however, (\ref{eq:CR_brief}) will not be strictly solvable due to unknown receiver noise, but estimates can be found.  
\subsection{Pre-Existing CR Algorithms}
In \cite{bib:kailath, bib:benesty}, two similar CR approaches are presented.  These two algorithms use (\ref{eq:CR_brief}) as the basis for their optimization equations.
\subsubsection{The SVD Approach}
Xu \emph{et al.}  in \cite{bib:kailath} present a solution to solving 
\begin{equation} \label{eq:SVD}
 \begin{aligned}
    &  \underset{\hbf}{\text{minimize}} & ||\Xbf\hbf^*||_2^2 &\\
    & \text{subject to} & ||\hbf||_2^2 = 1.  & 
 \end{aligned}
\end{equation}
In the absence of noise, and with the correct model order $L$ known
precisely, then the channel impulse response can be solved for up to a
multiplicative constant by finding a nonzero vector in the nullspace
of $\Xbf$.  In the presence of noise, a least-squares approximation to
a vector in the nullspace is sought in (\ref{eq:SVD}), which is found
using the right singular vector of the singular value
decomposition (SVD) of $\Xbf$.

If $L$ is underestimated, the channel cannot be fully identified.  If
the model order is overestimated, the null space of $\Xbf$ will
include time shifted variations of the channel vector, $\hbf$.  This
would not be a concern except that attempting to solve (\ref{eq:SVD})
in this case may yield arbitrary linear combinations of time-shifted
versions of the desired channel, which themselves are very poor
channel estimates.   

\subsubsection{The AED Approach}
When the amount of data is large, or estimation is desired real-time
during data collection, adaptive algorithms are often desirable.
Huang \emph{et al.} present a second adaptive approach in \cite{bib:benesty},
which looks at solving the expected value of the CR,
\begin{equation} \label{eq:AED}
 \begin{aligned}
    &  \underset{\hbf^{(n)}}{\text{min}} & \mathrm{E}(||\Xbf\hbf^{(n)}||_2^2) &\\
    & \text{subject to} & ||\hbf^{(n)}||_2^2 = 1 & 
 \end{aligned}
 \end{equation}
where $\hbf^{(n)}$  is the current iteration of the impulse response estimate.  

The approximate solution to (\ref{eq:AED}) is found using gradient descent, with the adaptive constrained LMS algorithm, found in \cite{bib:LMS}.  The update equation is
\begin{equation} \label{eq:AED_update}
	\begin{split}
          \tilde{\hbf}^{(n + 1)} &= \hbf^{(n)}  - \eta\frac{{\hbf^{(n)}}^{H}\xbf_r}
		{||\hbf^{(n)}||_2^2}\xbf_r \\
		\hbf^{(n + 1)}&= \frac{\tilde{\hbf}^{(n + 1)}}{||\tilde{\hbf}^{(n + 1)}||_2^2},	
	\end{split}
\end{equation}
a normalized update where $\eta$ denotes an adaptation step size,
${\hbf}^{(n + 1)}$ is the next iteration of the impulse response
estimate, ${\hbf}^{(n)}$, and $\xbf_{r}$ is the $r$th row of the data matrix
$\Xbf$, but represented as a column vector.  Equations (\ref{eq:AED}) and (\ref{eq:AED_update}) are referred to as the Adaptive Eigenvalue Decomposition (AED) algorithm.   Note that \cite{bib:benesty} assume that the signal is acoustic, and thus not adjusted for the complex RF signals, (\ref{eq:AED_update}) makes the necessary adjustments.

The AED algorithm is computationally less complex than the SVD
approach proposed by Xu \textrm{et al.}  This adaptive algorithm also
differs from the SVD approach in that rather than estimate the
channels exactly, the focus of this algorithm is to estimate the delay
between channels.  As a result, the channel estimates focus on the
major peaks of the impulse response.

\section{Alternative Approaches}
To combat some of the faults of the two exisiting CR algorithms, three
alternatives are introduced. The AED and SVD algorithms fail to utilize
the fact that typical mulitpath channel responses are sparse. The first
alternative approach addresses this. As both the pre-existing algorithms,
and the first alternative have the possibility of time-shifted versions of
the channel impulse response appearing in the estimated response, which
can hinder estimation, another alternative approach is proposed. The third
alternative looks at the possibility of preventing the linear combinations
of the impulse responses, but doesn't enforce the sparsity constraint. 
\subsection{The Modified Adaptive Eigenvalue Decomposition with
  Sparsity (ModAEDS)}
The first alternative approach introduces a different objective function format. This
new objective function begins with a cost function similar to the
normalized adaptive filter, seeking a minimal update change to the
filter which satisfies the constraint in (\ref{eq:CR_brief}) and other
operational constraints. The objective function also contains a
$L_1$ term weighted by a parameter $\alpha$ which encourages sparsity
of the solution. The combination of these yields the optimization problem
\begin{equation} \label{eq:ModAEDS_objective}
        \begin{aligned}
            & \min_{{\hbf^{(n+1)}}} & & ||{\hbf^{(n+1)} - \hbf^{(n)}}||_2^2 + \alpha||{\hbf^{(n + 1)}}||_1 & \\
            & \text{subject to} & & {\hbf^{(n + 1)}}^H\xbf_{r} = 0, &
        \end{aligned}
      \end{equation}
      where $\alpha$ denotes a tunable penalty weight. The sparsity constraint
      adds a penalty to prevent spurious peaks which provides a less noisy channel
      estimate.

      As with the AED algorithm, this solution to the adaptive algorithm is found using
      gradient descent. Instead of using the constrained LMS algorithm however, the
      update is derived using standard constrained optimization techniques, resulting in
      \begin{equation} \label{eq:ModAEDS_update}
        \hbf^{(n + 1)} = \hbf^{(n)} - \alpha\mathbf{e}^{{j}\angle\hbf^{(n)}} - \eta
        \frac{\xbf_{r}^H(\hbf^{(n)} - \alpha\mathbf{e}^{{j}\angle\hbf^{(n)}})}{||\xbf_{r}||_2^2}\xbf_{r}
      \end{equation}
      This algorithm is referred to as the Modified AED with Sparisty or ModAEDS algorithm because
      it looks similar to the AED algorithm, but uses a different derivation approach.
      
      The sparsity constraint manifests itself in (\ref{eq:ModAEDS_update}) as $\alpha\mathbf{e}^{j\angle\hbf^{(n)}}$.  This is done by first using a subgradient to represent the derivative of the $L_1$ norm,
    \begin{equation}
      \frac{d}{d{\hbf^{(n+1)}}^{*}}(||{\hbf^{(n + 1)}}||_1) \approx \mathbf{e}^{j\angle\hbf^{(n+1)}}.
    \end{equation}
    Since we are minimizing the difference between updates, we make the assumption that the difference
    between the sign of the impulse response should not be that much different from one iteration to
    the next, thus 
    \begin{equation}
    \mathbf{e}^{j\angle\hbf^{(n+1)}} \approx \mathbf{e}^{j\angle\hbf^{(n)}}
    \end{equation}
    is assumed.
    \subsection{The Adaptive Cross-Channel Identification with Sparse
      Shift-Suppression (AXIS)}
    As explained above, all three of the previous algorithms still have the
    possibility of time-shifted versions of the impulse responses appearing.
    To combat this, an additional constraint has been added. This extra constraint
    is an anchor constraint to suppress those time-shifted versions of the channel
    impulse responses from appearing, which is helpful when $L$ is longer than
    the actual channel.  This produces a different optimization problem,
\begin{equation} \label{eq:AXIS_objective}
        \begin{aligned}
            & \min_{{\hbf^{(n+1)}}} & & ||{\hbf^{(n+1)} - \hbf^{(n)}}||_2^2 + \alpha||{\hbf^{(n + 1)}}||_1 \\
            & \text{subject to} & & {\hbf^{(n + 1)}}^H\xbf_{r} = 0 \\
            & & & {\hbf^{(n + 1)}}^H\ebf_{d} = 1,
        \end{aligned}
\end{equation}
where the term $\ebf_d$ is a unit vector with $\ebf_d(d) = 1$.

Initially, to meet the restrictions the anchor constraint sets, the
initial channel estimate is equal to the unit vector $\ebf_d$. This
forces a peak at the $d^{\mathrm{th}}$ index, which consequently locks
in the peak as the dominant peak for one of the impulse responses
in place. To maintain the mathematical relationship, between
channels, the peak estimates for the other impulse response vary
until they reach an accurate delay estimate. 

Using typical methods of constrained optimization produces the update
equation is shown below in (\ref{eq:AXIS-complete-update})
% \begin{multline} \label{eq:AXIS-complete-update}
%     \hbf^{(n + 1)} = \hbf^{(n)} - \alpha\mathbf{e}^{{j}\angle\hbf^{(n)}} -\eta\Bigg( \\
%     \Big(\frac{((\hbf^{(n)} - \alpha\mathbf{e}^{{j}\angle\hbf^{(n)}})^\hbf - ((\hbf^{(n)} -
%         \alpha\mathbf{e}^{{j}\angle\hbf^{(n)}})^\hbf\ebf_{d}\ebf_{d}^\hbf -
%         \ebf_{d}^\hbf))\xbf_{r}}{\xbf_{r}^\hbf\xbf_{r} - \xbf_{r}^\hbf\ebf_{d}\ebf_{d}^\hbf\mathbf{x}_{r} + \epsilon}\Big)^{*}\xbf_{r} - \\
%    \Big(\frac{
%         \xbf_{r}^\hbf(\xbf_{r}((\hbf^{(n)} - \alpha\mathbf{e}^{{j}\angle\hbf^{(n)}})^\hbf\ebf_{d} - I) - \ebf_{d}(\hbf^{(n)} - \alpha\mathbf{e}^{{j}\angle\hbf^{(n)}})^\hbf\xbf_{r})}{
%         \xbf_{r}^\hbf\xbf_{r} - \xbf_{r}^\hbf\ebf_{d}\ebf_{d}^\hbf\xbf_{r} + \epsilon}\Big)^{*}\ebf_{d}\Bigg),
%     \end{multline}
where $I$ is the identity matrix.
\begin{figure*}[!t]
\setcounter{mytempeqncnt}{\value{equation}}
\setcounter{equation}{12}
\hrulefill
\begin{equation}
\begin{split}
\hbf^{(n + 1)} &= \hbf^{(n)} - \alpha\mathbf{e}^{{j}\angle\hbf^{(n)}} -\eta\Bigg( 
    \Big(\frac{((\hbf^{(n)} - \alpha\mathbf{e}^{{j}\angle\hbf^{(n)}})^H - ((\hbf^{(n)} -
        \alpha\mathbf{e}^{{j}\angle\hbf^{(n)}})^H\ebf_{d}\ebf_{d}^H -
        \ebf_{d}^H))\xbf_{r}}{\xbf_{r}^H\xbf_{r} -
        \xbf_{r}^H\ebf_{d}\ebf_{d}^H\mathbf{x}_{r} +
        \epsilon}\Big)^{*}\xbf_{r} - \\
& \qquad
   \Big(\frac{
        \xbf_{r}^H(\xbf_{r}((\hbf^{(n)} - \alpha\mathbf{e}^{{j}\angle\hbf^{(n)}})^H\ebf_{d} - I) - \ebf_{d}(\hbf^{(n)} - \alpha\mathbf{e}^{{j}\angle\hbf^{(n)}})^H\xbf_{r})}{
        \xbf_{r}^H\xbf_{r} -
        \xbf_{r}^H\ebf_{d}\ebf_{d}^H\xbf_{r} +
        \epsilon}\Big)^{*}\ebf_{d}\Bigg)
    \end{split}
\label{eq:AXIS-complete-update}
  \end{equation}
  \setcounter{equation}{\value{mytempeqncnt}+1}
  \hrulefill
  \vspace*{4pt}
\end{figure*}

This algorithm is referred to as the Adaptive Cross-Channel Identification wtih Sparse Shift-Suppression (AXIS).
\subsection{Non-Sparse Adaptive Cross-Channel Identification with Shift-Suppression (NSAXIS)}
The last alternative looks at only including the shift-suppression constraint. The objective function simplifies slightly, to
\begin{equation}\label{eq:NSAXIS_objective}
        \begin{aligned}
            & \min_{{\hbf^{(n+1)}}} & & ||{\hbf^{(n+1)} - \hbf^{(n)}}||_2^2& \\
            & \text{subject to} & & {\hbf^{(n + 1)}}^H\xbf_{r} = 0 \\
            & & & {\hbf^{(n + 1)}}^H\ebf_{d} = 1,
        \end{aligned}
  \end{equation}

  As before, the update is derived by typical optimization techniques which result in
  \begin{multline} \label{eq:NSAXIS-complete-update}
\hbf^{(n + 1)} = \hbf^{(n)} - \eta\Bigg( \\
\Big(\frac{{\hbf^{(n)}}^{H}\xbf_{r} - ({\hbf^{(n)}}^{H}\ebf_{d}\ebf_{d}^{H}\xbf_{r} -
	\ebf_{d}^{H}\xbf_{r})}{\xbf_{r}^{H}\xbf_{r} - \xbf_{r}^{H}\ebf_{d}\ebf_{d}^{H}\xbf_{r} + \epsilon}\Big)^{*}\xbf_{r} - \\
\Big(\frac{
	\xbf_{r}^{H}\xbf_{r}{\hbf^{(n)}}^{H}\ebf_{d} - \xbf_{r}^{H}\xbf_{r} - \xbf_{r}^{H}\ebf_{d}{\hbf^{(n)}}^{H}\xbf_{r}}{
	\xbf_{r}^{H}\xbf_{r} - \xbf_{r}^{H}\ebf_{d}\ebf_{d}^{H}\xbf_{r} + \epsilon}\Big)^{*}\ebf_{d}\Bigg).
\end{multline}
  %\begin{equation}
 %   \hbf^{(n+1)} = \hbf^{(n)} - \eta()
%    \end{equation}
  
    \begin{table*}
      \begin{center}
        \begin{tabular}{||c c c c c c c c||}
          \hline
          SNR (dB) & Algorithm & Sim Distance (m) & Est.  Dist.  (m) & Err.  (m)& Min.  Est (m) & Med.  Est (m) & Max.  Est (m) \\
          \hline
          \hline
          20 & SVD & 21.772 & 38.214 & 16.442& 0 & 30 & 105 \\
                   & AED & & 20 & 1.772& 7.5 & 22.5 & 37.5\\
                   & ModAEDS & & 18.571 & 3.201 &7.5 & 22.5 & 30 \\
                   & AXIS & & 20 & 1.772 & 7.5 & 22.5 & 37.5\\
                   & NSAXIS & & 20 & 1.772 & 7.5 & 22.5 & 37.5 \\
          \hline
          60 & SVD & 21.772 & 21.429 & 0.344 & 0 & 22.5 & 37.5 \\
                   & AED & & 20 & 1.772 & 7.5 & 22.5 & 37.5 \\
                   & ModAEDS & & 18.571 & 3.201 & 7.5 & 22.5 & 37.5 \\
                   & AXIS & & 20 & 1.772 & 7.5 & 22.5 & 37.5 \\
                   & NSAXIS & & 20 & 1.772 & 7.5 & 22.5 & 37.5 \\
          \hline
        \end{tabular}
        \caption{Averaged simulation results over 21 variations.}
        \label{tab:results}
        \end{center}
      \end{table*}
     \begin{figure*}[t]
      \centering
      \begin{subfigure}[b]{0.3\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{ \input{Figures/40MHz_actChan_IR-2_seed_2.tikz}}
		\caption{Actual Channels}
	\end{subfigure}
	\begin{subfigure}[b]{0.3\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Figures/40MHz_estChan_IR-2_seed_2_SVD.tikz}}
		\caption{SVD: Estimated Channels}
	\end{subfigure}
	\begin{subfigure}[b]{0.3\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Figures/40MHz_estChan_IR-2_seed_2_AED.tikz}}
		\caption{AED: Estimated Channels}
              \end{subfigure}
              \begin{subfigure}[b]{0.3\linewidth}
                \centering
		\resizebox{\linewidth}{!}
		{\input{Figures/40MHz_estChan_IR-2_seed_2_ModAEDS.tikz}}
		\caption{ModAEDS: Estimated Channels}
                \end{subfigure}
                \begin{subfigure}[b]{0.3\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Figures/40MHz_estChan_IR-2_seed_2_AXIS.tikz}}
		\caption{AXIS: Estimated Channels}
              \end{subfigure}
              \begin{subfigure}[b]{0.3\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Figures/40MHz_estChan_IR-2_seed_2_NSAXIS.tikz}}
		\caption{NSAXIS: Estimated Channels}
	\end{subfigure}     
	\caption{Results of the different algorithms for 30,000 data samples, with SNR = 60 dB, at 40 MHz sampling rate for wideband simulation.}
        \label{fig:results}
      \end{figure*}
\section{Simulation Results and Discussion}
Table \ref{tab:results} and Figure \ref{fig:results} show the
simulated comparison of these five blind channel estimation
techniques for two different SNRs. For 21 repetitions, a 30,000
sample Gaussian distributed white-noise signal was generated and fed
through two 40 MHz impulse responses representing an indoor
environment (each repetition, the impulse responses varied slighlty).
Noise was added according to the desired SNR.  Table \ref{tab:results}
shows the both the average estimated distance and average error over
those 21 variants. The range of estimates and the median estimate
over the same 21 variants can also be seen. Figure \ref{fig:results}
show the results for the third variant at the higher SNR.

As can be seen in the figure, the SVD algorithm did an excellent job
of estimating the two channels with the higher SNR, for the variant
shown.  The adaptive algorithms did not do as well at estimating the
channels exactly, but still maintain the correct delay with the
largest peaks.  As shown in Table \ref{tab:results}, however, the
adaptive algorithms are consistent over 21 variant estimates, whereas
the SVD approach suffers with the lower SNR. It is also interesting
to note that the although the SVD algorithm shows that at the higher
SNR it has a better averaged estimate, the adaptive algorithms
estimated values closer to the true distance more often.

There is not much difference between the adaptive algorithms,
except that with only the sparcity constraint, the ModAEDS algorithm
has much smaller extraneous peaks. It also on average had worse performance
than the other algorithms. The AED, AXIS, and NSAXIS algorithms
look approximately the same, with a little less noise on both the AXIS
and NSAXIS. Interestingly, it was observed
that the adaptive algorithms show that by fixing the peak of one
channel, the other channel tends to adopt some of the fixed channel's
characteristics.
    \section{Conclusion}
    The ModAEDS, AXIS, and NSAXIS algorithm has been introduced as alternatives
    to the existing blind channel estimation techniques for multipath environments.
    As adaptive algorithms, they are ideal for large amounts of data, and have
    shown in simulation that they are robust to noisy data, and provide consistent
    results.
    
    The AXIS algorithm has the added advantage in that it was derived to combat the
    spurious peaks that can occur either by noisiness or by possible linear combinations.
    As shown in simulation, it can do just as well or better than the
    other algorithms at estimating the time delay between channels.

    The ModAEDS and NSAXIS algorithms were each derived with only one of the two
    causes for spurious peaks. As a result the ModAEDS algorithm really enforced
    sparseness, which caused the averaged estimate to be a little lower. The
    NSAXIS focused only on preventing time-shifted variations from occuring. But
    this algorithm did not show much difference from the AXIS algorithm. And like
    the AXIS algorithm has shown in simulation that it can do just as well or
    better than the other algorithms at estimating delay between channels.
\ifCLASSOPTIONcaptionsoff
  \newpage
\fi
\begin{thebibliography}{1}
\bibitem{bib:thesis}
M.  L.  Rose, "Indoor Source localization of Radio Frequency Transmitters Using Blind Channel Identification Techniques," Master's thesis, Utah State University, Old Main Hill, Logan, UT, 2020.
\bibitem{bib:kailath}
G.  Xu, H.  Liu, L.  Tong, and T.  Kailath, “A least-squares approach to
blind channel identification,” \emph{IEEE Transactions on Signal Processing},
vol.  43, no.  12, pp.  2982–-2993, 1995.
\bibitem{bib:benesty}
Y.  Huang, J.  Bensty and G.  Elko, "Adaptive eigenvalue decomposition
algorithm for real time acoustic source localization system," \emph{Proc.  of
IEEE ICASSP99}, Vol.2, pp.  937--940, March 1999.  
\bibitem{bib:TDOA_ML} M.  N.  de Sousa and R.  S.  Thomä, “Enhancement of Localization Systems in NLOS Urban Scenario with Multipath Ray Tracing Fingerprints and Machine Learning,” \emph{Sensors}, vol.  18, no.  11, p.  4073, Nov.  2018.
\bibitem{bib:LMS}
O.  L.  Frost "An algorithm for linearly constrained adaptive array processing," \emph{Proceedings of the IEEE}, vol.  60, no.  8, pp.  926-935, Aug.  1972.  
\end{thebibliography}
\end{document}


