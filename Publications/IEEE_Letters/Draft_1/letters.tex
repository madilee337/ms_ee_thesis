\documentclass[journal]{IEEEtran}
\usepackage{cite}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{url}
\usepackage{graphicx}
\usepackage{float}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{wrapfig}
\usepackage{epsfig}
\usepackage{color} 
\usepackage{euscript}
\usepackage{tikz}
\usetikzlibrary{decorations.pathreplacing}
\usetikzlibrary{patterns}
\usetikzlibrary{positioning}
\usepackage{etex}		
\usepackage{pgfplots}			
\pgfplotsset{compat=1.6}     
\usetikzlibrary{calc}
\hyphenation{op-tical net-works semi-conduc-tor}


\begin{document}
\title{An Alternative Blind Channel Identification Method Using the Cross-Relation}
\author{Madison~L.~Rose,
        Joseph~Ipson,
        Todd~K.~Moon,~\IEEEmembership{Senior Member,~IEEE,}
        and~Jacob~H.~Gunther~\IEEEmembership{Senior Member,~IEEE}% <-this % stops a space
\thanks{The authors are  with the Department of Electrical and Computer Engineering, Utah State University, Logan, UT. 84322 USA (email: {madi.mickelsen@aggiemail.usu.edu,  jipson@aggiemail.usu.edu, todd.moon@usu.edu, jake.gunther@usu.edu)}}}
\markboth{IEEE Signal Processing Letters, April~2020}%
{Rose \MakeLowercase{\textit{et al.}}: An Alternative Blind Channel Identification Method Using the Cross-Relation}
\maketitle
\begin{abstract}
Blind channel identification (BCI) is useful for source localization in multipath environments as  when identifying the channel, a time delay between received signals can be established. This time delay can then be extrapolated to be applied to time difference of arrival (TDOA). This letter introduces an alternative approach which uses the cross-relation (CR), referred to as the Adaptive Cross-Channel Identification with Sparse Shift-Suppression (AXIS) algorithm.
\end{abstract}

% Note that keywords are not normally used for peerreview papers.
\begin{IEEEkeywords}
channel identification, cross-relation, iterative algorithm, multipath, time delay, source localization, time difference of arrival, 
\end{IEEEkeywords}

\IEEEpeerreviewmaketitle

\section{Introduction}
\IEEEPARstart{S}{ource} localization has become increasingly relevant as technology advances. Time difference of arrival (TDOA) source localization approaches are of great interest, as TDOA can be used in blind situations, and in the presence of severe multipath, as long as direct paths are detectable. 

Many time delay estimation (TDE) techniques rely on identifying the largest peak in the cross-correlation or related function \cite{bib:thesis}. However, these algorithms have been shown to deteriorate in the presence of multipath. It should also be noted that in indoor environments or other multipath conditions, the strongest path may not correspond to the shortest path (the direct path), and thus the largest peak can misidentify time delays. 

An alternative approach to identifying time delays involves using channel estimates of the impulse responses \cite{bib:thesis,bib:kailath,bib:benesty}. When the source signal is unknown, blind channel identification (BCI) is performed using the received signals, and the relevant time delays are extracted from peaks in those estimates. While only the relative time delay between a set of receivers is strictly required for time-delay estimation, there are advantages to estimating full channel responses where possible:
\begin{enumerate}
    \item If detectable, the earliest path represented in an impulse response can be chosen correctly, even if not the strongest available path.
    
    \item The estimated channel can give time delay and amplitude estimates for secondary paths, which may be useful information in some position estimation techniques, such as those found in \cite{bib:TDOA_ML}.
    
    \item The set of delay spreads may provide information about local geographic complexity, such as the quantity of local reflectors. 
\end{enumerate}

The purpose of this letter is to enhance previous work found in \cite{bib:kailath,bib:benesty} on blind channel identification, by proposing a method utilizing both sparsity, and anchor constrains in an iterative normalized least mean squares (NLMS) type algorithm. 
\section{Complex Cross Relations Methods}
The formulation used in this research largely follow that given in \cite{bib:thesis,bib:kailath,bib:benesty}. Consider a source, at an unknown location, transmitting an unknown signal in a multipath environment that is detected by multiple receivers. This system is modelled as
\begin{equation}
\label{eq:system_conj}
\begin{cases}
x_{1}(n)=s(n)*h^*_{1}(n)+w_{1}(n) \\ 
\ \vdots \\ 
x_{M}(n)=s(n)*h^*_{M}(n)+w_{M}(n), 
\end{cases}
\end{equation}
where $*$ denotes convolution, $h^*_{i}(n)$ denotes the complex conjugate of each individual channel mapping from the source to the $i^\mathrm{th}$ receiver, $x_{i}(n)$ the value received from the transmitted signal at the $i^{th}$ receiver, and $\mathrm{w}(n) = [w_{1}(n)\hspace{0.1cm}w_{2}(n)...w_{M}(n)]$ is additive white noise, and $n$ denotes the $n^\mathrm{th}$ sample. $M$ denotes the number of receivers. This model also assumes a finite impulse response, and is a SIMO system.

Neglecting noise, the signal model, (\ref{eq:system_conj}), can be expressed individually as $x_i(n) = s(n)*h^*_i(n)$, where $i = 1, \cdots, M$. Convolving $x_i(n)$ with $h_j^*(n)$, where $i \neq j$ produces the complex cross-relation (CR) for a pair of receivers,
\begin{equation}
\label{eq:cr-derivation_conj}
h^*_j(n)*x_i(n) - h^*_i(n)*x_j(n) = 0.
\end{equation}
The CR can be expressed in terms of receiver measurements as the matrix equation \begin{equation} \label{eq:matCrossRelation}
        \begin{bmatrix} \mathrm{X}_i(L) & \vdots & -\mathrm{X}_j(L)
        \end{bmatrix}
        \begin{bmatrix} \mathrm{h}_j \\ \mathrm{h}_i \end{bmatrix} = 0,
\end{equation}
where $\mathrm{X}_m(L)$ is a Toeplitz matrix of the form
\begin{equation} \notag
    \mathrm{X}_m(L) = \begin{bmatrix}
                        x_m(L) & x_m(L - 1) & \cdots & x_m(0) \\
                        x_m(L + 1) & x_m(L) & \cdots & x_m(1) \\
                        \vdots & \ddots & \ddots & \vdots \\
                        x_m(N) & x_m(N - 1) & \cdots & x(N - L - 1)
                      \end{bmatrix}
\end{equation}
and $\mathrm{h}_m$ is the impulse response vector 
\begin{equation} \notag
        \mathrm{h}_m^* \triangleq [h_m(0), \cdots, h_m(L)]^{\mathrm{H}}.
    \end{equation}
For brevity, we will rewrite (\ref{eq:matCrossRelation}) as
\begin{equation} \label{eq:CR_brief}
    \mathbf{X}\mathbf{h}^* = \mathbf{0}.
\end{equation}
Equation (\ref{eq:CR_brief}) is the key to the methods of channel identification discussed in this letter. In practice however, (\ref{eq:CR_brief}) will not be strictly solvable due to unknown receiver noise, but estimates can be found. 
\subsection{Pre-Existing CR Algorithms}
In \cite{bib:kailath, bib:benesty}, two similar CR approaches are presented. These two algorithms use (\ref{eq:CR_brief}) as the basis for their optimization equations.
\subsubsection{The SVD Approach}
Xu et al. in \cite{bib:kailath} present a solution to solving 
\begin{equation} \label{eq:SVD}
 \begin{aligned}
    &  \underset{\mathbf{h}}{\text{minimize}} & ||\mathbf{X}\mathbf{h}||_2^2 &\\
    & \text{subject to} & ||\mathbf{h}||_2^2 = 1. & 
 \end{aligned}
 \end{equation}
In the absence of  noise, and with the correct model order, $L$, known precisely, then the channel impulse response can be solved for up to a multiplicative constant by finding a nonzero vector in the null space of $\mathbf{X}$. This is done by using the right singular vector of  the singular value decomposition (SVD) of $\mathbf{X}$. 

It is important to note that if  $L$ is underestimated, the channel cannot be fully identified. If the model order is overestimated, the null space of $\mathbf{X}$ will include time shifted variations of the channel vector, $\mathbf{h}$. This would not be a concern except that attempting to solve (\ref{eq:SVD}) in this case may yield arbitrary linear combinations of time-shifted versions of the desired channel, which themselves are very poor channel estimates.  Also, as explained above, noise is not absent. Thus the right singular vector provides a least squares approximation to the null vectors. 
\subsubsection{The AED Approach}
When the amount of data is large, or estimation is desired real-time during data collection, adaptive algorithms are often desirable. Huang et. al present a second adaptive approach in \cite{bib:benesty}, which looks at solving the expected value of the CR,
\begin{equation} \label{eq:AED}
 \begin{aligned}
    &  \underset{\mathbf{h}^{(n)}}{\text{min}} & \mathrm{E}(||\mathbf{X}\mathbf{h}^{(n)}||_2^2) &\\
    & \text{subject to} & ||\mathbf{h}^{(n)}||_2^2 = 1 & 
 \end{aligned}
 \end{equation}
where $\mathbf{h}^{(n)}$  is the current iteration of the impulse response estimate. 

The approximate solution to (\ref{eq:AED}) is found using gradient descent, with the adaptive constrained LMS algorithm, found in \cite{bib:LMS}. The update equation is
\begin{equation} \label{eq:AED_update}
	\begin{split}
          \tilde{\mathbf{h}}^{(n + 1)} &= \mathbf{h}^{(n)}  - \eta\frac{{\mathbf{h}^{(n)}}^{H}\mathrm{x_r}}
		{||\mathbf{h}^{(n)}||_2^2}\mathrm{x}_r \\
		\mathbf{h}^{(n + 1)}&= \frac{\tilde{\mathbf{h}}^{(n + 1)}}{||\tilde{\mathbf{h}}^{(n + 1)}||_2^2},	
	\end{split}
\end{equation}
a normalized update where $\eta$ denotes an adaptation step size, ${\mathbf{h}}^{(n + 1)}$ is the next iteration of the impulse response estimate, ${\mathbf{h}}^{(n)}$, and $\mathrm{x}_{r}$ is a row of the data matrix $\mathbf{X}$, transposed as all vectors are assumed to be column formatted. Equations (\ref{eq:AED}) and (\ref{eq:AED_update}) are referred to as the Adaptive Eigenvalue Decomposition (AED) algorithm.  Note that \cite{bib:benesty} assume that the signal is acoustic, and thus not adjusted for the complex RF signals, (\ref{eq:AED_update}) makes the necessary adjustments.

The AED algorithm is computationally less complex than the SVD approach proposed by Xu et al. This adaptive algorithm also differs from the SVD approach in that rather than estimate the channels exactly,  the focus of this algorithm is to estimate the delay between channels. As a result, the channel estimates focus on the major peaks of the impulse response.
\section{Adaptive Cross-Channel Identification with Sparse Shift-Suppression}
The AED and SVD algorithms fail to utilize the fact that typical mulitpath channel responses are sparse. Both algorithms also have the possibility of time-shifted versions of the channel impulse response appearing in the estimated response, which can hinder estimation. To combat those two potential problems, an alternative approach is proposed.

This new objective function focuses on minimizing the differnce between estimate iterations, and uses (\ref{eq:CR_brief}) as the constraint instead. Two other constraints are introduced. The first constraint is a scaled sparsity constraint, added to the objective function. The second, an anchor constraint to suppress the time-shifted versions of the channel impulse responses from appearing. This combination yields,
\begin{equation} \label{eq:AXIS_objective}
        \begin{aligned}
            & \min_{{\mathbf{h}^{(n+1)}}^\mathrm{H}} & & ||{\mathbf{h}^{(n+1)} - \mathbf{h}^{(n)}}||_2^2 + \alpha||{\mathbf{h}^{(n + 1)}}^\mathrm{*}||_1^{} \\
            & \text{subject to} & & {\mathbf{h}^{(n + 1)}}^\mathrm{H}\mathrm{x}_{r} = 0 \\
            & & & {\mathbf{h}^{(n + 1)}}^\mathrm{H}\mathrm{e}_{d} = 1.
        \end{aligned}
\end{equation}
The term $\mathrm{e}_d$ is a unit vector with $\mathrm{e}_d(d) = 1$, and $\alpha$ denotes a tunable penalty weight. The added anchor constraint locks the dominant peak of one of the impulse functions in place. The peak estimates for the other impulse response are variable until they reach an accurate delay estimate. The sparsity constraint adds a penalty to prevent spurious peaks to provide a cleaner less noisy channel estimate.

Using typical methods of constrained optimization produces the update equation,
\begin{multline} \label{eq:AXIS-complete-update}
    \mathbf{h}^{(n + 1)} = \mathbf{h}^{(n)} - \alpha\mathbf{e}^{{j}\angle\mathbf{h}^{(n)}} -\eta\Bigg( \\
    \Big(\frac{((\mathbf{h}^{(n)} - \alpha\mathbf{e}^{{j}\angle\mathbf{h}^{(n)}})^\mathrm{H} - ((\mathbf{h}^{(n)} -
        \alpha\mathbf{e}^{{j}\angle\mathbf{h}^{(n)}})^\mathrm{H}\mathrm{e}_{d}\mathrm{e}_{d}^\mathrm{H} -
        \mathrm{e}_{d}^\mathrm{H}))\mathrm{x}_{r}}{\mathrm{x}_{r}^\mathrm{H}\mathrm{x}_{r} - \mathrm{x}_{r}^\mathrm{H}\mathrm{e}_{d}\mathrm{e}_{d}^\mathrm{H}\mathbf{x}_{r} + \epsilon}\Big)^{*}\mathrm{x}_{r} - \\
   \Big(\frac{
        \mathrm{x}_{r}^\mathrm{H}(\mathrm{x}_{r}((\mathbf{h}^{(n)} - \alpha\mathbf{e}^{{j}\angle\mathbf{h}^{(n)}})^\mathrm{H}\mathrm{e}_{d} - \mathrm{I}) - \mathrm{e}_{d}(\mathbf{h}^{(n)} - \alpha\mathbf{e}^{{j}\angle\mathbf{h}^{(n)}})^\mathrm{H}\mathrm{x}_{r})}{
        \mathrm{x}_{r}^\mathrm{H}\mathrm{x}_{r} - \mathrm{x}_{r}^\mathrm{H}\mathrm{e}_{d}\mathrm{e}_{d}^\mathrm{H}\mathrm{x}_{r} + \epsilon}\Big)^{*}\mathrm{e}_{d}\Bigg),
    \end{multline}
    where $\mathrm{I}$ is the identity matrix. The sparsity constraint manifests itself in (\ref{eq:AXIS-complete-update}) as $\alpha\mathbf{e}^{j\angle\mathbf{h}^{(n)}}$. This is done using a subgradient, and the fact that as we are minimizing the difference between updates, thus
    \begin{equation}
      \frac{d}{d{\mathbf{h}^{(n+1)}}^{*}}(||{\mathbf{h}^{(n + 1)}}^\mathrm{*}||_1) \approx \mathbf{e}^{j\angle\mathbf{h}^{(n+1)}} \approx\mathbf{e}^{j\angle\mathbf{h}^{(n)}}
    \end{equation}
    can be assumed.
    
    \begin{table*}
      \begin{center}
        \begin{tabular}{||c c c c c c c c||}
          \hline
          SNR (dB) & Algorithm & Sim Distance (m) & Est. Dist. (m) & Err. (m)& Min. Est (m) & Med. Est (m) & Max. Est (m) \\
          \hline
          \hline
          20 & SVD & 21.772 & 38.214 & 16.442& 0 & 30 & 105 \\
                   & AED & & 20 & 1.772& 7.5 & 22.5 & 37.5\\
                   & AXIS & & 20 & 1.772 & 7.5 & 22.5 & 37.5\\
          \hline
          60 & SVD & 21.772 & 21.429 & 0.344 & 0 & 22.5 & 37.5 \\
                   & AED & & 20 & 1.772 & 7.5 & 22.5 & 37.5\\
                   & AXIS & & 20 & 1.772 & 7.5 & 22.5 & 37.5\\
          \hline
        \end{tabular}
        \caption{Averaged simulation results over 21 variations.}
        \label{tab:results}
        \end{center}
      \end{table*}
     \begin{figure*}[t]
      \centering
      \begin{subfigure}[b]{0.35\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Figures/40MHz_actChan_IR-2_seed_2.tikz}}
		\caption{Actual Channels}
	\end{subfigure}
	\begin{subfigure}[b]{0.35\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Figures/40MHz_estChan_IR-2_seed_2_SVD.tikz}}
		\caption{Estimated Channels, SVD Approach}
	\end{subfigure}
	\begin{subfigure}[b]{0.35\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Figures/40MHz_estChan_IR-2_seed_2_AED.tikz}}
		\caption{Estimated Channels, AED Approach}
	\end{subfigure}
	\begin{subfigure}[b]{0.35\linewidth}
		\centering
		\resizebox{\linewidth}{!}
		{\input{Figures/40MHz_estChan_IR-2_seed_2_AXIS.tikz}}
		\caption{Estimated Channels, AXIS Approach}
	\end{subfigure}
	\caption{Results of the different algorithms for 30,000 data samples, with SNR = 60 dB, at 40 MHz sampling rate for wideband simulation.}
        \label{fig:results}
      \end{figure*}
      \section{Simulation Results and Discussion}
      Table \ref{tab:results} and Figure \ref{fig:results} show the simulated comparison of these three blind channel estimation techniques for two different SNRs. A 30,000 sample Gaussian distributed white-noise signal was generated and fed through two 40 MHz impulse responses representing an indoor environment 21 times (each time, the impulse responses varied slighlty). Noise was added according to the desired SNR. Table \ref{tab:results} shows the both the average estimated distance and average error over those 21 variants. The range of estimates and the median estimate over the same 21 variants can also be seen. Figure \ref{fig:results} show the results for the third variant at the higher SNR.  

      As can be seen in the figure, the SVD algorithm did an excellent job of estimating the two channels with the higher SNR, for the variant shown. The adaptive algorithms did not do as well at estimating the channels exactly, but still maintain the correct delay with the largest peaks. As shown in Table \ref{tab:results}, however, the adaptive algorithms are consistent over 21 variant estimates, whereas the SVD approach suffers with the lower SNR. It is also interesting to note that the although the SVD algorithm shows that at the higher SNR it has a better averaged estimate, the adaptive algorithms estimated values closer to the true distance more often.

      There is not much difference between the two adaptive algorithms, except that with the sparcity constraint, the AXIS algorithm has slightly smaller extraneous peaks. Interestingly, it was observed that the adaptive algorithms show that by fixing the peak of one channel, the other channel tends to adopt some of the fixed channel's characteristics.  
    \section{Conclusion}
    The AXIS algorithm has been introduced as an alternative to the existing blind channel estimation techniques for multipath environments. This algorithm has the added advantage in that it was derived to combat the spurious peaks that can occur either by noisiness or by possible linear combinations. As shown in simulation, it can do just as well or better than the other algorithms at estimating the time delay between channels. 

\ifCLASSOPTIONcaptionsoff
  \newpage
\fi
\begin{thebibliography}{1}
\bibitem{bib:thesis}
M. L. Rose, "Indoor Source localization of Radio Frequency Transmitters Using Blind Channel Identification Techniques," Master's thesis, Utah State University, Old Main Hill, Logan, UT, 2020.
\bibitem{bib:kailath}
G. Xu, H. Liu, L. Tong, and T. Kailath, “A least-squares approach to
blind channel identification,” \emph{IEEE Transactions on Signal Processing},
vol. 43, no. 12, pp. 2982–2993, 1995.
\bibitem{bib:benesty}
Y. Huang, J. Bensty and G. Elko, "Adaptive eigenvalue decomposition
algorithm for real time acoustic source localization system," \emph{Proc. of
IEEE ICASSP99}, Vol.2, pp.937-940, March 1999. 
\bibitem{bib:TDOA_ML} M. N. de Sousa and R. S. Thomä, “Enhancement of Localization Systems in NLOS Urban Scenario with Multipath Ray Tracing Fingerprints and Machine Learning,” \emph{Sensors}, vol. 18, no. 11, p. 4073, Nov. 2018.
\bibitem{bib:LMS}
O. L. Frost "An algorithm for linearly constrained adaptive array processing," \emph{Proceedings of the IEEE}, vol. 60, no. 8, pp. 926-935, Aug. 1972. 
\end{thebibliography}
\end{document}


