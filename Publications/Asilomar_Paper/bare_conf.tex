



\documentclass[conference]{IEEEtran}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{algorithm2e}
\usepackage{textcomp}
\usepackage{graphicx}

\hyphenation{op-tical net-works semi-conduc-tor}


\begin{document}

\title{AXIS: Adaptive Cross Channel Identification with Sparse Shift-Suppression for TDOA in Multipath Environments}
% \title{Blind Channel Identification for Sparse Matrices}

\author{\IEEEauthorblockN{Madison L. Rose}

\and
\IEEEauthorblockN{Joseph L. Ipson}

\and
\IEEEauthorblockN{Todd K. Moon}

\and
\IEEEauthorblockN{Jacob H. Gunther}
}

\maketitle

\begin{abstract}
Time-difference-of-arrival methods are an important approach to source localization. When there are significant multipath effects, estimating channel impulse responses is an advantageous approach in determining time-delays to perform localization. Blind channel estimation in this scenario is commonly done using the cross-relation (CR). This paper discusses a sparse cross-relation blind channel estimation method denoted as the Adaptive Cross Channel Identification with Sparse Shift-Suppression (AXIS) algorithm. This method minimizes the CR while enforcing sparsity and an additional constraint to improve channel estimates in a way that is robust to over-estimated channel order.

\end{abstract}

\IEEEpeerreviewmaketitle

\section{Introduction}
% no \IEEEPARstart
% !!! Should we emphasize blind channel ID more, or source localization?
%Identifying the transfer characteristics of propogation channels is an important aspect of many communication and other signal processing problems. This is particularly true in environments which exhibit substantial convolutive effects, such as under indoor multipath conditons. In many cases, the received signal is not known a priori, leading to what is known as the Blind Channel Estimation problem. 
% !!! Should we capitalize things like Blind Channel estimation?
%Much research has been done in Blind channel estimation generally, and also towards particular applications. Blind channel estimation is important to a variety of communication techniques [source?]. It can also be used as part of a blind deconvolution approach [source?]. (Are these too off topic?).
%One significant use of Blind Channel Identification is its use in time-delay estimation for Time-difference of Arrival (TDOA) source localization. While only the relative time delay between a set of receivers is strictly required for time-delay estimation, there are several advantages to estimating full channel responses where possible:   
Time-difference-of-arrival (TDOA) approaches for source localization are of great interest to many practical geolocation applications. TDOA is usable in the presence of severe multipath, as long as direct paths remain detectable. Many time-delay estimation techniques rely on identifying the largest peak in a cross-correlation or related function \cite{bib:benesty},  \cite{bib:MIMO_Book}, \cite{bib:MAP_TDE}. However in indoor or other multipath conditions, the strongest path may not correspond to the shortest path (the direct path), and thus the largest peak can misidentify time delays.
% include BSN? \cite{bib:BSN_CID}

Another approach for identifying time delays for TDOA localization is by using estimates of the channel impulse responses \cite{bib:benesty}, \cite{bib:BSN_CID}. When the source signal is unknown, blind channel identification is performed using the received signals, and the relevant time delays are extracted from peaks in these estimates. While only the relative time delay between a set of receivers is strictly required for time-delay estimation, there are advantages to estimating full channel responses where possible:     
  
\begin{enumerate}
    \item If detectable, the earliest path represented in a transfer function can be chosen correctly, even if not the strongest available path.
    
    \item The estimated transfer function can give time delay and amplitude estimates for secondary paths, which may be useful information in some position estimation techniques, such as those found in \cite{bib:TDOA_ML}.
    
    \item The set of delay spreads may provide information about local geographic complexity, such as the quantity of local reflectors. 

\end{enumerate}

Blind channel identification can be improved by utilizing expected multipath channel characteristics, such as sparsity \cite{bib:SIMO_Sparse}. The goal of this research is to enhance previous work on blind channel estimation, by proposing a method utilizing sparsity and anchor constraints in a normalized least mean squares (NLMS) type algorithm.
%This research proposes a blind channel identification algorithm utilizing sparsity and anchor constraints in a normalized LMS type algorithm.

\section{Cross Relation Methods}
Our formulation largely follows that given  in \cite{bib:kailath}. Consider a source transmitting an unknown signal in a multipath environment that is detected by multiple receivers. This system is modelled as
\begin{equation}
    \label{eq:system}
    \begin{cases}
        x_{1}(n)=s(n)*h_{1}(n)+w_{1}(n) \\ 
        \ \vdots \\ 
        x_{M}(n)=s(n)*h_{M}(n)+w_{M}(n), 
    \end{cases}
\end{equation}
where $*$ denotes linear convolution, $s(n)$ the signal transmitted, $h_{i}(n)$ denotes the individual channel mapping from the source to the $i^{th}$ receiver, $x_{i}(n)$ the value received from the transmitted signal at the $i^{th}$ receiver, and $\mathrm{w}(n) = [w_{1}(n)\hspace{0.1cm}w_{2}(n)...w_{M}(n)]$ is additive white noise, and $n$ denotes the $n^\mathrm{th}$ sample. $M$ denotes the number of channels. This model assumes a finite impulse response, and is a single input, multiple output (SIMO) system. 

Neglecting noise, the previously described signal model for a pair of signals can be expressed individually as $x_i(n) = s(n)*h_i(n)$. Convolving $x_i(n)$ with $h_j(n)$, and $x_j(n)$ with $h_i(n)$, where  $i \neq j$ gives
\begin{equation} \label{eq:CR}
    h_j(n)*x_i(n) - h_i(n)*x_j(n) = 0.
\end{equation}
The result in (\ref{eq:CR}) is known as the cross-relation (CR) and is key to these methods of channel estimation. The CR can be expressed in terms of receiver measurements as the matrix equation
\begin{equation} \label{eq:5}
        \begin{bmatrix} \mathrm{X}_i(L) & \vdots & -\mathrm{X}_j(L)
        \end{bmatrix}
        \begin{bmatrix} \mathbf{h}_j \\ \mathbf{h}_i \end{bmatrix} = 0,
\end{equation}
where $\mathrm{X}_m(L)$ is a Toeplitz matrix of the form
\begin{equation} \notag
    \mathrm{X}_m(L) = \begin{bmatrix}
                        x_m(L) & x_m(L - 1) & \cdots & x_m(0) \\
                        x_m(L + 1) & x_m(L) & \cdots & x_m(1) \\
                        \vdots & \ddots & \ddots & \vdots \\
                        x_m(N) & x_m(N - 1) & \cdots & x(N - L - 1)
                      \end{bmatrix}
\end{equation}
and $\mathrm{h}_m$ is the impulse response vector 
\begin{equation} \notag
        \mathbf{h}_m \triangleq [h_m(0), \cdots, h_m(L)]^{\mathrm{T}}.
    \end{equation}
For brevity, we will rewrite (\ref{eq:5}) as
\begin{equation} \label{eq:CR_brief}
    \mathbf{X}\mathbf{h} = \mathbf{0}.
\end{equation}
In the absence of noise, and if the correct model order L is known precisely, this can be solved directly for the channel impulse response up to a multiplicative constant by finding a nonzero vector in the null space of X. This could be performed using the right singular vector of the singular value decomposition of X. 

It is important to note that if the model order $L$ is under-estimated, the channel cannot be fully identified. If the model order is over-estimated, the null space of $\mathbf{X}$ will include time-shifted variations of the channel vector $\mathbf{h}$. This would not be a concern except that attempting to solve (\ref{eq:CR_brief}) in this case may yield arbitrary linear combinations of time-shifted versions of the desired channel, which themselves are very poor channel estimates. 
%Since final transfer function coefficients can be very small, some of these difficulties can occur even when the model order is estimated correctly.

In practice, (\ref{eq:CR_brief}) will not be strictly solvable due to receiver noise. A right singular vector provides a least squares approximation to the null vectors. When the amount of data is large, or estimation is desired real-time during data collection, adaptive algorithms are often desirable.

\section{Adaptive Cross Channel Estimation Algorithm}
Rather than use the entirety of the matrix $\mathbf{X}$, we can learn $\mathbf{h}$ in an adaptive method.
%If we let $\mathbf{h_{n + 1}}$, be the update for the transfer function, $\mathbf{h_{n}}$, and use the cross relation criterion as a constraint, then we can set up a convex optimization problem: 
Letting $\mathbf{h}_\mathrm{n + 1}$ be the next updated estimate of the transfer function, $\mathbf{h_{n}}$, we can use the cross relation criterion as a constraint, yielding the following optimization problem, which is related to a normalized LMS adaptive filter. 
\begin{equation} \label{eq:optimization}
    \begin{aligned}
        & \underset{\mathbf{h}_\mathrm{n+1}}{\text{minimize}} & & ||{\mathbf{h}_\mathrm{n+1} - \mathbf{h}_\mathrm{n}}|| \\
        & \text{subject to} & & \mathbf{h}_\mathrm{n + 1}^\mathrm{T}\mathbf{x}_\mathrm{n} = 0.
\end{aligned}
\end{equation}
% To solve this, we set up the Lagrangian:
% \begin{equation}
%     \mathcal{L} = \mathbf{h_{n + 1}^{T}}\mathbf{h_{n + 1}} - 
%                   2\mathbf{h_{n}^{T}}\mathbf{h_{n + 1}} +
%                   \mathbf{h_{n}^{T}}\mathbf{h_{n}} + 
%                   \lambda\mathbf{h_{n + 1}^{T}}\mathbf{x_n}
% \end{equation}
% Taking the derivative of the Lagrangian, setting equal to $0$ and then solving for $\mathbf{h_{n + 1}}$, produces the following:
% \begin{equation}
%     \frac{d}{d\mathbf{h_{n + 1}}}\mathcal{L} = 2\mathbf{h_{n + 1}} - 
%                                               2\mathbf{h_{n}} + 
%                                               \lambda\mathbf{x_n}
% \end{equation}
% \begin{equation} \label{eq:10}
%     \mathbf{h_{n+1}} = \mathbf{h_{n}} - \lambda\mathbf{x_n}
% \end{equation}
% Using (\ref{eq:10}), we can solve the constraint for the value of $\lambda$
% \begin{equation}
%     \begin{split}
%          \mathbf{h_{n + 1}^{T}}\mathbf{x_n} &= 0 \\
%          (\mathbf{h_{n}} - \lambda\mathbf{x_n})^{T}\mathbf{x_n} &= 0 \\
%          \mathbf{h_{n}^{T}}\mathbf{x_n} - \lambda\mathbf{x_n})^{T}\mathbf{x_n} &= 0 \\
%          \lambda = \frac{ \mathbf{h_{n}^{T}}\mathbf{x_n}}{||\mathbf{x_n}||}
%     \end{split}
% \end{equation}
% Replacing $\lambda$ in the original update equation, and adding a weight on the update gives us an adaptive update rule:
The update equation can be found using methods of constrained optimization as
\begin{equation}
    \mathbf{h}_\mathrm{n+1} = \mathbf{h}_\mathrm{n} - \mu\frac{ \mathbf{h}_\mathrm{n}^\mathrm{T}\mathbf{x}_\mathrm{n}}{||\mathbf{x}_\mathrm{n}||}\mathbf{x}_\mathrm{n},
\end{equation}
where $\mu$ denotes an adaption step size.

% Letting 
% \begin{equation}
%     e_n = \frac{ \mathbf{h_{n}^{T}}\mathbf{x_n}}{||\mathbf{x_n}||}
% \end{equation}
% allows the update equation to simplify to 
% \begin{equation}
%     \mathbf{h_{n+1}} = \mathbf{h_{n}} - \mu e_n\mathbf{x_n}
% \end{equation}
As suggested by \cite{bib:benesty}, the update equation can be normalized to enforce $||\mathbf{h}_\mathrm{n}||_2=1$. Some form of normalization is necessary to prevent the transfer function estimate from shrinking to zero. This method is functional, but fails to utilize the fact that typical multipath channel responses are sparse, making it inferior to methods such as those of  \cite{bib:SIMO_Sparse, bib:BSN_CID} and \cite{bib:ID_BDeconv}. The lower quality of estimate obtained by this method can lead to errors in the identified transfer function, and therefore errors in the delay estimate.
%As explained in \cite{bib:benesty}, normalizing the update equation provides optimal filter weights and prevents the transfer function estimate from shrinking to zero. This method often yields poor channel estimates as explained in \cite{bib:SIMO_Sparse, bib:BSN_CID} and \cite{bib:ID_BDeconv}, particularly when the channel transfer functions are sparse. These errors in the identified transfer function result in an inaccurate delay estimate. 

\section{Adaptive Cross Channel Identification Algorithm with Sparsity Constraint}
As the transfer functions being estimated are usually sparse by nature, a sparsity constraint is added to the optimization problem of (\ref{eq:optimization}). Sparsity constraints are typically represented as $L_{\mathrm{1}}$ norms \cite{bib:SIMO_Sparse}, \cite{bib:BSN_CID}, 
% Adding a sparsity constraint to the optimization problem produced a stable estimate that could accurately estimate the delay between the receivers. 
% A sparsity constraint is typically represented by the $L_{\mathrm{1}}$ norm.% Put Reference here
Incorporating the 1-norm into the objective function produces
\begin{equation} \label{eq:optimization_sparse}
        \begin{aligned}
            & \min_{\mathbf{h}_\mathrm{n+1}} & & ||{\mathbf{h}_\mathrm{n+1} - \mathbf{h}_\mathrm{n}}||_2^2 - \alpha||\mathbf{h}_\mathrm{n + 1}||_1 \\
            & \text{subject to} & & \mathbf{h}_\mathrm{{n + 1}}\mathrm{^{T}}\mathbf{x}_\mathrm{n} = 0, \\
        \end{aligned}
    \end{equation}
where $\alpha$ denotes a tunable penalty weight.
As before, setting up the Lagrangian, and then taking the derivative with respect to $\mathbf{h}_\mathrm{n + 1}$, produces the update equation
%     \begin{equation} \label{eq:AED_1}
%         \frac{d}{d\mathbf{h_{n + 1}}}\mathcal{L} = 2\mathbf{h_{n + 1}} - 
%                                               2\mathbf{h_{n}} + 
%                                               \lambda\mathbf{x_n} -\alpha \text{sgn}(\mathbf{h_{n+1}})
%     \end{equation}
% %The derivative of the $L_{\mathrm{1}}$ norm does not exist everywhere due to the discontinuity at 0. A sub-gradient can be used at these points instead, providing an accurate approximation as is shown in (\ref{eq:AED_1}). REFERENCES
% While the proper gradient of the $L_{\mathrm{1}}$ norm is discontinuous at 0, using a sub-gradient is used at these points does not negatively impact performance. REFERENCE? %(\ref{eq:AED_1})
% % The derivative of the $L_{\mathrm{1}}$ norm is not possible to calculate due to the discontinuity at 0. To remedy this, a sub-derivative can be used to accurately approximate the derivative of the $L_{\mathrm{1}}$ norm, as is shown in (\ref{eq:AED_1}). REFERENCES

% Solving for $\mathbf{h_{n + 1}}$, we get the following:
% \begin{equation}\label{eq:AED_2}
%     \mathbf{h_{n+1}} + \alpha\text{sgn}(\mathbf{h_{n + 1}})= \mathbf{h_{n}} - \lambda\mathbf{x_n};% + \alpha\text{sgn}(\mathrm{h_{n + 1}})
% \end{equation}
% %Which is not exactly what we want. This equation can be modified slightly by assuming that $\text{sgn}(\mathbf{h_{n + 1}}) \approx \text{sgn}(\mathbf{h_{n}})$. Thus we can remedy (\ref{eq:AED_2}), as follows:
% Solving this directly is difficult due to the $\text{sgn}(\mathrm{h_{n + 1}})$ term. If we assume $\text{sgn}(\mathbf{h_{n + 1}}) \approx \text{sgn}(\mathbf{h_{n}})$, we can then remedy (\ref{eq:AED_2}), as follows:
% \begin{equation}\label{eq:AED_3}
%             \mathbf{h_{n+1}} = \mathbf{h_{n}} - \lambda\mathbf{x_n} - \alpha\text{sgn}(\mathbf{h_n})
%         \end{equation}
%     Plugging (\ref{eq:AED_3}) into the constraint equation and solving for $\lambda$ produces:
%     \begin{equation}
%         \lambda = \frac{(\mathbf{h_{n}} + \alpha\text{sgn}(\mathbf{h_n}))^{T}\mathbf{x_n}}{||\mathbf{x_n}||^2}
%     \end{equation}
\begin{equation}\label{eq:AED_4}
    \mathbf{h}_\mathrm{n+1} = \mathbf{h}_\mathrm{n} - \mu\frac{(\mathbf{h}_\mathrm{n} + \text{sgn}(\mathbf{h}_\mathrm{n}))^\mathrm{T}\mathbf{x}_\mathrm{n}}{||\mathbf{x}_\mathrm{n}||^2}\mathbf{x}_\mathrm{n} + \alpha\text{sgn}(\mathbf{h}_\mathrm{n}).
\end{equation}
As before, this result is then normalized to have unit energy after every update.

This method produces a more reliable estimate that could often accurately estimate the delay between the receivers dependably, even when the model order L was not exactly correct. However, the estimates produced by this method often still contained some spurious peaks, particularly with a poor model order estimate. These concerns motivate the shift-suppression constraint in the next section.

\section{Adaptive Cross Channel Identification with Sparse Shift-Suppression (AXIS) Algorithm}
When using the previous algorithm, time-shifted versions of the channel impulse response appearing in the estimated response can hinder estimation. To suppress these shifted versions, an anchor constraint is added, similar to that of \cite{bib:BSN_CID}. We require $\mathbf{h}_\mathrm{n + 1}^\mathrm{T}\mathbf{e}_\mathrm{d} = 1$, where $\mathbf{e}_\mathrm{d}$ is a unit vector with $\mathbf{e}_\mathrm{d}(d) = 1$. Coupled with the sparsity constraint, various linear combinations of shifts are penalized, while the estimate with a peak at at a chosen delay $d$ is protected. The sparsity constraint is the same as described above, in (\ref{eq:optimization_sparse}). We formulate the problem similar to before as
\begin{equation}
        \begin{aligned}
            & \min_{\mathbf{h}_\mathrm{{n+1}}} & & ||{\mathbf{h}_\mathrm{n+1} - \mathbf{h}_\mathrm{n}}||_2^2 - \alpha||\mathbf{h}_\mathrm{n + 1}||_1^{} \\
            & \text{subject to} & & \mathbf{h}_\mathrm{n + 1}^\mathrm{T}\mathbf{x}_\mathrm{n} = 0 \\
            & & & \mathbf{h}_\mathrm{n + 1}^\mathrm{T}\mathbf{e}_\mathrm{d} = 1.
        \end{aligned}
    \end{equation}
The added anchor constraint locks the dominant peak of one of the transfer functions in place, while the other peak estimates for the transfer functions are variable until they reach accurate delay estimates. 
 
As before, calculating the Lagrangian, and then taking the derivative with respect to $\mathbf{h}_\mathrm{n + 1}$, and solving,
% gets us (operating under the same assumptions as before):
% \begin{equation} \notag
%     \frac{d}{d\mathbf{h}_\mathrm{{n + 1}}}\mathcal{L} = 2\mathbf{h}_\mathrm{{n + 1}} - 
%             2\mathbf{h}_\mathrm{n} +  \lambda\mathbf{x}_\mathrm{n} + \eta\mathbf{e}_\mathrm{d} - 
%             \alpha \text{sgn}(\mathbf{h}_\mathrm{n+1})
% \end{equation}
\begin{equation} \label{eq:AED_5}
    \mathbf{h}_\mathrm{n+1} = \mathbf{h}_\mathrm{{n}} - \lambda\mathbf{x}_\mathrm{n} - \eta\mathbf{e}_\mathrm{d} - \alpha\text{sgn}(\mathbf{h}_\mathrm{n}),
\end{equation}
where $\lambda$ and $\eta$ are Lagrange multipliers. Using the two constraint equations we can set up a system of equations to solve for $\eta$ and $\lambda$ simultaneously
    % \begin{equation}
    %     \begin{aligned}
    %         & (\mathbf{h}_\mathrm{n}\mathrm{^{T}} + \alpha\text{sgn}(\mathbf{h}_\mathrm{n}^\mathrm{{T}}))\mathbf{x}_\mathrm{n}&  &=&  &\lambda\mathbf{x}_\mathrm{n}^\mathrm{{T}}\mathbf{x}_\mathrm{n} + \eta\mathbf{e}_\mathrm{d}^\mathrm{T}\mathbf{x}_\mathrm{n}& \\
    %         &(\mathbf{h}_\mathrm{n}^\mathrm{T} + \alpha\text{sgn}(\mathbf{h}_\mathrm{n}^\mathrm{T}))\mathbf{e}_\mathrm{d} - 1& &=& &\lambda\mathbf{x}_\mathrm{n}^\mathrm{T}\mathbf{e}_\mathrm{d} + \eta&
    %     \end{aligned}
    % \end{equation}
    % Stacking everything up into matrices
    % \begin{equation}
    %     \begin{bmatrix}
    %         \mathbf{x}_\mathrm{n}^\mathrm{T}\mathbf{x}_\mathrm{n} & \mathbf{e}_\mathrm{d}^\mathrm{T}\mathbf{x}_\mathrm{n} \\
    %         \mathbf{x}_\mathrm{n}^\mathrm{T}\mathbf{e}_\mathrm{d} & 1
    %     \end{bmatrix}
    %     \begin{bmatrix}
    %         \lambda \\
    %         \eta
    %     \end{bmatrix}
    %     = 
    %     \begin{bmatrix}
    %         \mathbf{h}_\mathrm{n}^\mathrm{T}\mathbf{x}_\mathrm{n} + \alpha\text{sgn}(\mathbf{h}_\mathrm{n}^\mathrm{T})\mathbf{x}_\mathrm{n}  \\
    %         \mathbf{h}_\mathrm{n}^\mathrm{T}\mathbf{e}_\mathrm{d} + \alpha\text{sgn}(\mathbf{h}_\mathrm{n}^\mathrm{T})\mathbf{e}_\mathrm{d} - 1
    %     \end{bmatrix}
    % \end{equation}
%   The resulting values of $\lambda$ and $\eta$ are
    \begin{equation} \label{eq:constrainst_0}
        \lambda = \frac{\mathbf{h}_\mathrm{n}^\mathrm{T}(\mathbf{x}_\mathrm{n} - \mathbf{e}_\mathrm{d}) + 
                    \alpha(\text{sgn}(\mathbf{h}_\mathrm{n}^\mathrm{T})\mathbf{x}_\mathrm{n} - \text{sgn}(\mathbf{h}_\mathrm{n}^\mathrm{T})\mathbf{e}_\mathrm{d}) -  \mathbf{x}_\mathrm{n}^\mathrm{T}\mathbf{e}_\mathrm{d}}
                    {\mathbf{x}_\mathrm{n}^\mathrm{T}\mathbf{x}_\mathrm{n} - \mathbf{e}_\mathrm{d}^\mathrm{T}\mathbf{x}_\mathrm{n}\mathbf{x}_\mathrm{n}^\mathrm{T}\mathbf{e}_\mathrm{d} + \beta},
    \end{equation}
    \begin{multline} \label{eq:constraint_1}
        \eta = \frac{\mathbf{x}_\mathrm{n}^\mathrm{T}\mathbf{x}_\mathrm{n}\mathbf{h}_\mathrm{n}^\mathrm{T}\mathbf{e}_\mathrm{d} - \mathbf{x}_\mathrm{n}^\mathrm{T}\mathbf{e}_\mathrm{d}\mathbf{h}_\mathrm{n}^\mathrm{T}\mathbf{x}_\mathrm{n}             + \alpha(\text{sgn}(\mathbf{h}_\mathrm{n}^\mathrm{T})\mathbf{e}_\mathrm{d}\mathbf{x}_\mathrm{n}^\mathrm{T}\mathbf{x}_\mathrm{n})}
        {\mathbf{x}_\mathrm{n}^\mathrm{T}\mathbf{x}_\mathrm{n} - \mathbf{e}_\mathrm{d}^\mathrm{T}\mathbf{x}_\mathrm{n}\mathbf{x}_\mathrm{n}^\mathrm{T}\mathbf{e}_\mathrm{d} + \beta} \\
        -                                      \frac{\alpha(\text{sgn}(\mathbf{h}_\mathrm{n}^\mathrm{T})\mathbf{x}_\mathrm{n}\mathbf{x}_\mathrm{n}^\mathrm{T}\mathbf{e}_\mathrm{d})-  \mathbf{x}_\mathrm{n}^\mathrm{T}\mathbf{x}_\mathrm{n}}
                    {\mathbf{x}_\mathrm{n}^\mathrm{T}\mathbf{x}_\mathrm{n} - \mathbf{e}_\mathrm{d}^\mathrm{T}\mathbf{x}_\mathrm{n}\mathbf{x}_\mathrm{n}^\mathrm{T}\mathbf{e}_\mathrm{d} + \beta}.
    \end{multline}
$\beta$ is an offset to prevent the denominator from becoming 0.  For this algorithm, the Lagrange multipliers, $\lambda$, and $\eta$ are recognized as the {\it{error}} values, such that $\lambda = \mathrm{err_1}$, and  $\mu = \mathrm{err_2}$. Plugging the actual values of $\lambda$ and $\eta$ into our update equation
\begin{multline} \label{eq:AED_6}
    \mathbf{h}_\mathrm{n+1}= \mathbf{h}_\mathrm{n} \\ - 
                \mu(
                \frac{
                  \mathbf{h}_\mathrm{n}^\mathrm{T} (\mathbf{x}_\mathrm{n} - \mathbf{e}_\mathrm{d}) 
                + \alpha \text{sgn}(\mathbf{h}_\mathrm{n}^\mathrm{T}) (\mathbf{x}_\mathrm{n} - \mathbf{e}_\mathrm{d})
                - \mathbf{x}_\mathrm{n}^\mathrm{T} \mathbf{e}_\mathrm{d}}
                  {\mathbf{x}_\mathrm{n}^\mathrm{T}\mathbf{x}_\mathrm{n} 
                - \mathbf{e}_\mathrm{d}^\mathrm{T} \mathbf{x}_\mathrm{n} \mathbf{x}_\mathrm{n}^\mathrm{T} \mathbf{e}_\mathrm{d} + \beta}\mathbf{x}_\mathrm{n} \\
            +   \frac{
                 (\mathbf{x}_\mathrm{n}^\mathrm{T} \mathbf{x}_\mathrm{n} \mathbf{h}_\mathrm{n}^\mathrm{T} \mathbf{e}_\mathrm{d}
                - \mathbf{x}_\mathrm{n}^\mathrm{T} \mathbf{e}_\mathrm{d} \mathbf{h}_\mathrm{n}^\mathrm{T}) \mathbf{x}_\mathrm{n}
                + \alpha (\text{sgn}(\mathbf{h}_\mathrm{n}^\mathrm{T}) \mathbf{e}_\mathrm{d} \mathbf{x}_\mathrm{n}^\mathrm{T} \mathbf{x}_\mathrm{n})}
                {\mathbf{x}_\mathrm{n}^\mathrm{T} \mathbf{x}_\mathrm{n} 
                - \mathbf{e}_\mathrm{d}^\mathrm{T} \mathbf{x}_\mathrm{n} \mathbf{x}_\mathrm{n}^\mathrm{T} \mathbf{e}_\mathrm{d} + \beta}\mathbf{e}_\mathrm{d} \\
            -   \frac{
                  \alpha (\text{sgn}(\mathbf{h}_\mathrm{n}^\mathrm{T}) \mathbf{x}_\mathrm{n} \mathbf{x}_\mathrm{n}^\mathrm{T}  \mathbf{e}_\mathrm{d})
                -  \mathbf{x}_\mathrm{n}^\mathrm{T} \mathbf{x}_\mathrm{n}} {\mathbf{x}_\mathrm{n}^\mathrm{T}      \mathbf{x}_\mathrm{n} 
            -\mathbf{e}_\mathrm{d}^\mathrm{T} \mathbf{x}_\mathrm{n} \mathbf{x}_\mathrm{n}^\mathrm{T} \mathbf{e}_\mathrm{d} + \beta}\mathbf{e}_\mathrm{d})  
                    - \alpha\text{sgn}(\mathbf{h}_\mathrm{n}).
\end{multline}
% In (\ref{eq:AED_6}), $\mu$ is a step size. 
We can simplify (\ref{eq:AED_6}) by denoting the Lagrange multipliers as their respective error values
\begin{equation} \label{eq:AED_7}
    \mathbf{h}_\mathrm{{n+1}} = \mathbf{h}_\mathrm{n} - \mu({\mathrm{err_1}\mathbf{x}_\mathrm{n} + \mathrm{err_2}\mathbf{e}_\mathrm{d}}) - \alpha\text{sgn}(\mathbf{h}_\mathrm{n}).
\end{equation}

This update equation defines the adaptive cross channel identification with sparse shift suppression or AXIS algorithm. This leads us to the steps: 

\vspace{0.1cm}
    %noindent{\bf{Result:}}{Transfer Function Estimates} \\
    \noindent\hspace*{0.2cm}{\bf{For}} {\it{each row $\mathbf{x}_n$ of }}$\mathbf{X}$: \\
    %\hspace*{0.3cm} Obtain next data row: $\mathbf{x_n}$; \\
    \hspace*{0.3cm} Compute Error Vectors according to (\ref{eq:constrainst_0}) and (\ref{eq:constraint_1}) above: \\
    \hspace*{0.65cm} $\mathrm{err_1}=\lambda$ and $\mathrm{err_2}=\eta$; \\
    \hspace*{0.3cm} Update Filter Coefficients: \\
    \hspace*{0.65cm} $\mathbf{h}_\mathrm{{n+1}} = \mathbf{h}_\mathrm{{n}} - \mu({\mathrm{err_1}\mathbf{x}_\mathrm{n} + \mathrm{err_2}\mathbf{e}_\mathrm{d}}) - \alpha\text{sgn}(\mathbf{h}_\mathrm{n})$; \\

As this algorithm is adaptive, the estimate is made as it passes through the data. When there is only limited data available, the estimate can be improved through several passes through the data. 
\noindent Note that due to the constraint forcing a nonzero value in the transfer function, it can no longer be driven to zero, so normalizing the transfer function estimate at each step is no longer necessary.

The $L_{\mathrm{1}}$ regularization significantly reduces spurious peaks and inaccuracies from the previous algorithm, resulting in a better transfer function estimate. This in turn, lends itself to improved time delay estimation. 



The AXIS algorithm is similar to what was done in \cite{bib:SIMO_Sparse}, \cite{bib:BSN_CID}, \cite{bib:ETDOA_L1}. In \cite{bib:SIMO_Sparse}, they do implement sparseness in the same way that the AXIS algorithm does, but the shift-suppression constraint is not implemented. \cite{bib:BSN_CID} and \cite{bib:ETDOA_L1} discuss similar constraints. This algorithm differs in that it is built around minimizing estimate change, and does not make the non-negativity assumption often used in the acoustic domain.
% , and \cite{bib:BCI_reverb}
\section{Results and Discussion}
% As mentioned previously, the AXIS algorithm provides an improved adaptive solution to blind channel estimation of sparse channels, eliminating the problematic spurious peaks. 


The proposed adaptive cross channel identification with sparse shift-suppression (AXIS) algorithm has been verified by simulation. Transfer functions for four receivers, and one transmitter were generated for an indoor environment using a ray-tracing algorithm in MATLAB\textsuperscript \textregistered. Gaussian white noise was then generated and filtered through the simulated transfer functions, and noise was added to produce the test received signals. Figure \ref{fig:AXIS_Noise} depicts the transfer function estimates for $\text{SNR} = 10 \text{dB}$, with $\mathbf{h}_\mathrm{{0}}=0$, $\mu = 5\times10^{-4}$, and $\alpha = 10^{-4}$. In contrast, directly finding the right singular vector under these conditions gives an invalid estimate.

%!!! Discuss shift-suppression constraint here: How it isn't useful without the sparsity constraint? 
%The step size for the sparsity constraint 
A key advantage of the proposed AXIS algorithm (over other algorithms) is its robustness to over-estimation of the channel order. Our simulations found that over-estimating the channel order even by a factor of 2 still yielded good channel estimates.  For substantial ($\text{SNR} \geq 0\text{dB}$) added white noise, the AXIS algorithm still provides an accurate estimate of the delay.
%The AXIS algorithm does not estimate the transfer function exactly as is proposed in \cite{bib:kailath}, but rather estimates the delay between the the two transfer functions. One key advantage of the proposed AXIS algorithm (over most other algorithms) is its robustness to over-estimation of the channel order. Our simulations found that over-estimating the channel order by a factor of 2 still yielded good channel estimates. The AXIS algorithm is also more robust when it comes to noise than the algorithm described in \cite{bib:kailath}. For substantial ($\text{SNR} \geq 0\text{dB}$) added white noise, the AXIS algorithm still provides an accurate estimate of the delay.

It is worth noting that the initial value for $\mathbf{h}_\mathrm{{n}}$ will effect its convergence. Starting with $\mathbf{h}_\mathrm{{0}}=0$ seems to perform acceptably, though the performance may be improved by starting with a vector which is closer to the actual channel impulse response.

Some alternative programming modifications can make the proposed algorithm faster and/or more accurate. Instead of updating the transfer function estimates by an individual row of the $\mathbf{X}$ matrix at a time, the program can be modified such that blocks of rows of data are used. Another modification is upsampling the received data to compensate for the limited sample rate. Upsampling provides a fractional bin delay estimate instead of a whole bin delay estimate. This allows for more accurate delay estimate. The estimated transfer functions do need to be appropriately modified to compensate for this upsampling as well. 

%This algorithm has only been verified by simulation. Transfer functions for four receivers, and one transmitter were generated for an indoor environment using a ray-tracing algorithm in MATLAB\textsuperscript \textregistered. Gaussian white noise was then generated and filtered through the simulated transfer functions, and noise was added for more accurate simulations. Figure \ref{fig:AXIS_Noise} depicts the transfer function estimates for $\text{SNR} = 10 \text{dB}$, with $\mathbf{h_{0}}=0$, $\mu = 5e-4$, and $\alpha = 1e-4$.

% Figure \ref{fig:AXIS_error} shows the change in error over iterations. The depicted change in error is only for  $\mathrm{err_1}$. As is observed, the error converging to 0 does not necessarily mean that the transfer function is more accurate. 

For the final paper, we plan to test this algorithm on data gathered  in  a  real  indoor  environment.

We also expect to have implemented more than two pairs of sensors for our $\mathrm{X}$ data matrix such that we can simultaneously solve for more delay estimates.
\begin{figure}
    \centering
    \includegraphics[width = 10cm]{AXIS_Fig2.png}
    \caption{A comparison of (a) the simulated transfer functions,  with (b) the transfer function estimates obtained using the AXIS algorithm for two receivers.}
    \label{fig:AXIS_Noise}
\end{figure}

 
% all approach the problem in an acoustic sense, and so some of the assumptions made, are not valid assumptions for our RF system identification.  
\section{Conclusion}
As is shown, the AXIS algorithm provides a solution to blind channel estimation of sparse channels utilizing a gradient descent method. The algorithm provides a stable, consistent, and robust solution; as well as preventing the spurious effects and inaccuracies of some other adaptive methods.

Although the AXIS algorithm works well, there are some drawbacks. If there are too many passes through the same data, the estimated transfer function may eventually begin to degenerate. The algorithm also treats the transfer functions asymmetrically. Further work needs to be done to see if these issues can be resolved.

% conference papers do not normally have an appendix
% use section* for acknowledgment
% \section*{Acknowledgment}
% *OMIT*
% The authors would like to thank the Laboratory for Telecommunication Sciences for providing funding, support, and expertise.






% trigger a \newpage just before the given reference
% number - used to balance the columns on the last page
% adjust value as needed - may need to be readjusted if
% the document is modified later
%\IEEEtriggeratref{8}
% The "triggered" command can be changed if desired:
%\IEEEtriggercmd{\enlargethispage{-5in}}

% references section

% can use a bibliography generated by BibTeX as a .bbl file
% BibTeX documentation can be easily obtained at:
% http://mirror.ctan.org/biblio/bibtex/contrib/doc/
% The IEEEtran BibTeX style support page is at:
% http://www.michaelshell.org/tex/ieeetran/bibtex/
%\bibliographystyle{IEEEtran}
% argument is your BibTeX string definitions and bibliography database(s)
%\bibliography{IEEEabrv,../bib/paper}
%
% <OR> manually copy in the resultant .bbl file
% set second argument of \begin to the number of references
% (used to reserve space for the reference number labels box)
\begin{thebibliography}{1}

\bibitem{bib:kailath}
G. Xu, H. Liu, L. Tong, and T. Kailath, “A least-squares approach to
blind channel identification,” \emph{IEEE Transactions on Signal Processing},
vol. 43, no. 12, pp. 2982–2993, 1995.

\bibitem{bib:benesty}
Y. Huang, J. Bensty and G. Elko, "Adaptive eigenvalue decomposition
algorithm for real time acoustic source localization system," \emph{Proc. of
IEEE ICASSP99}, Vol.2, pp.937-940, March 1999. 

\bibitem{bib:SIMO_Sparse}
A. Aïssa-El-Bey, K. Abed-Meraim and C. Laot, "Adaptive blind estimation of sparse SIMO channels," \emph{International Workshop on Systems, Signal Processing and their Applications, WOSSPA}, Tipaza, 2011, pp. 348-351.

\bibitem{bib:BSN_CID}
Y. Lin, J. Chen, Y. Kim and D. D. Lee, "Blind Sparse-Nonnegative (BSN) Channel Identification for Acoustic Time-Difference-of-Arrival Estimation," \emph{2007 IEEE Workshop on Applications of Signal Processing to Audio and Acoustics}, New Paltz, NY, USA, 2007, pp. 106-109.

\bibitem{bib:MIMO_Book}
Y. Huang, J.Benesty and J.Chen "Time delay estimation and acoustic source localization" in \emph{Acoustic MIMO Signal Processing}, 1st ed. Springer-Verlag Berlin Heidelberg, 2006, pp. 215-259

\bibitem{bib:ID_BDeconv}
Y. Li, K. Lee and Y. Bresler, "Identifiability in Blind Deconvolution With Subspace or Sparsity Constraints," in \emph{IEEE Transactions on Information Theory}, vol. 62, no. 7, pp. 4266-4275, July 2016.

\bibitem{bib:subgradient}
Y. Chen, "Subgradient Methods", \emph{Princeton University ELE522: Large-Scale Optimization for Data Science} [Online]. Available: http://www.princeton.edu/~yc5/ele522\_optimization/lectures/subgradient\_\\methods.pdf. [Accessed: April. 17, 2019].

\bibitem{bib:ETDOA_L1}
M. Crocco and A. Del Bue, "Estimation of TDOA for room reflections by iterative weighted l1 constraint,"\emph{2016 IEEE International Conference on Acoustics, Speech and Signal Processing (ICASSP)}, Shanghai, 2016, pp. 3201-3205.

\bibitem{bib:BCI_reverb}
Y. Lin, J. Chen, Y. Kim, D. D. Lee, "Blind channel identification for speech dereverberation using lI-norm sparse learning", \emph{Advances in Neural Information Processing Systems}, pp. 921-928, 2007

\bibitem{bib:MAP_TDE}
B. Lee and T. Kalker, "Maximum A Posteriori Estimation of Time Delay," \emph{2007 2nd IEEE International Workshop on Computational Advances in Multi-Sensor Adaptive Processing}, St. Thomas, VI, 2007, pp. 285-288.

\bibitem{bib:MCBI_Overview}
Lang Tong and S. Perreau, "Multichannel blind identification: from subspace to maximum likelihood methods," in \emph{Proceedings of the IEEE}, vol. 86, no. 10, pp. 1951-1968, Oct. 1998.

\bibitem{bib:BSI_Overview}
K. Abed-Meraim, Wanzhi Qiu and Yingbo Hua, "Blind system identification," in \emph{Proceedings of the IEEE}, vol. 85, no. 8, pp. 1310-1322, Aug. 1997.

\bibitem{bib:TDOA_ML} M. N. de Sousa and R. S. Thomä, “Enhancement of Localization Systems in NLOS Urban Scenario with Multipath Ray Tracing Fingerprints and Machine Learning,” \emph{Sensors}, vol. 18, no. 11, p. 4073, Nov. 2018.
% \bibitem{IEEEhowto:kopka}
% H.~Kopka and P.~W. Daly, \emph{A Guide to \LaTeX}, 3rd~ed.\hskip 1em plus
%   0.5em minus 0.4em\relax Harlow, England: Addison-Wesley, 1999.

\end{thebibliography}




% that's all folks
\end{document}


